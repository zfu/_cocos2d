//
//  handBookLayer.h
//  notfound
//
//  Created by 付卓 on 13-11-10.
//
//

#ifndef __notfound__handBookLayer__
#define __notfound__handBookLayer__

#include <iostream>
#include <cocos2d.h>
USING_NS_CC;

class handBookLayer : public Layer{
public:
    static Scene *scene();
    CREATE_FUNC(handBookLayer);
    bool init();
    
    virtual void onEnterTransitionDidFinish();
    virtual void onExit();
    virtual bool ccTouchBegan(Touch *touch, Event *event);
    virtual void ccTouchMoved(Touch *touch, Event *event);
    virtual void ccTouchEnded(Touch *touch, Event *event);
    
    void buttonCallback(Object* pSender);
    ~handBookLayer();
private:
    Sprite *bg;
    Sprite *bg2;
    Sprite *modeLogo;
    Sprite *setupModeSprite;
    Sprite *storyModeSprite;
    Sprite *eternalModeSprite;
    Sprite *achieveModeSprite;
    Sprite *shopModeSprite;
    Array *modeSprites;
    
    Sprite *setupMaskSprite;
    Sprite *storyMaskSprite;
    Sprite *eternalMaskSprite;
    Sprite *achieveMaskSprite;
    Sprite *shopMaskSprite;
    Array *maskSprites;
    
    Menu *menu;
    void initModeSprite();
    int current;
    Layer *rotateLayer;
    bool inRotate;
    void rotateOver();
    
    //for item show
    void initItems();
    Layer *itemLayer;
    void updateItems();
    
    Sprite *modeRotateSprite;
    MenuItemSprite *prevItemSprite;
    MenuItemSprite *nextItemSprite;
    MenuItemSprite *guideItemSprite;
    
    //for show item select and item detal
    typedef enum{kSelectStage, kSelectItem, kItemDetal} State;
    Sprite *itemSelectSprite;
    State mState;
    void changeToStage(State);
    void initItemSprite();
    
    //label
    LabelTTF *percentLabel;
    char percentString[64];
    unsigned int stage1UnlockNum;
    unsigned int stage2UnlockNum;
    unsigned int stage3UnlockNum;
    unsigned int stage4UnlockNum;
    unsigned int totoalUnlockNum;
    
    //detail
    Sprite *itemDetalSprite;
    Sprite *itemSprite;
    LabelTTF *nameLabel;
    LabelTTF *descLabel;
    LabelTTF *lockLabel;
    LabelTTF *itemPercentLabel;
    char itemPercentString[1024];
    
    Point beforePoint;
    
    //for guide
    bool isGuideShow;
    Sprite *guidSprite;
};

#endif /* defined(__notfound__handBookLayer__) */
