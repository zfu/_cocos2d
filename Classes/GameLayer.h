//
//  GameLayer.h
//  notfound
//
//  Created by 付卓 on 13-11-7.
//
//

#ifndef __notfound__GameLayer__
#define __notfound__GameLayer__

#include <cocos2d.h>
#include <vector>
USING_NS_CC;

class GameLayer : public Layer
{
public:
    CREATE_FUNC(GameLayer);
    bool init();
    static Scene *scene();
    ~GameLayer();
    
    virtual void onEnterTransitionDidFinish();
    virtual void onExit();
    virtual bool ccTouchBegan(Touch *touch, Event *event);
    virtual void ccTouchMoved(Touch *touch, Event *event);
    virtual void ccTouchEnded(Touch *touch, Event *event);
    
private:
    typedef enum{kGameModeBuffet, kGameModeToyCastle, kGameModeTvShow, kGameModeBazaar, kGameModeEternal, kGameModeInvalid}GameMode;
    GameMode gameMode;
    
    Sprite *bg;//back ground
    Sprite *itembarSprite;
    Sprite *topbarSprite;
    Sprite *rotateSprite;
    Sprite *ingameGoldFrameSprite;
    Sprite *ingameMaskBallSprite;
    Sprite *animateTimeSprite;
    MenuItemSprite *pauseMenuItemSprite;
    MenuItemSprite *returnMenuItemSprite;
    MenuItemSprite *ingameClockSprite;//clock tool
    MenuItemSprite *ingameGlassSprite;//glass tool
    MenuItemSprite *ingameRotateSprite;//rotate tool
    MenuItemSprite *ingameCleanSprite;//clean tool
    void buttonCallback(Object* pSender);
    bool rotateToolRunning;
    void rotateToolRunOver();
    void glassToolRunOver();
    Sprite *glassFindSprite;
    String *glassFindName;
    Sprite *glassActionSprite;
    Point glassFindPos;
    bool glassToolRunning;
    Point beforeRotatePoint;
    float beforeRotateAng;
    float beforeRotateLayerAng;
    bool rotated;
    float lastBadRotateRunTime;
    
    Sprite *currentClickedSprite;
    //ttf label for tool nums
    LabelTTF *clockNumLabel;
    LabelTTF *glassNumLabel;
    LabelTTF *rotateNumLabel;
    LabelTTF *cleanNumLabel;
    void initToolNumSprites();
    void updateToolNum();
    typedef enum{kBadToolWater=5, kBadToolBlock, kBadToolPollute, kBadToolForbid, kBadToolAntiRotate, kBadToolSlowRotation,kBadToolInvalid}BadToolType;
    BadToolType badTool;
    BadToolType currentRunBadTool;
    void getToolFinish(Sprite *targetSprite, Sprite *actionSprite);
    void getBadTool(Sprite *targetSprite, Sprite *actionSprite, int type);
    void badToolActionOver(Sprite *actionSprite, int type);
    bool blockStartClean;
    void clearBlock();
    void blockOver();
    void playBlockEffect();
    bool badToolRunning;
    bool inCleaning;
    void resetClean();
    //bad tool sprites
    Sprite *waterSprite;
    Sprite *blockLeftSprite,*blockMidSprite,*blockRightSprite,*blockSideSprite;
    Sprite *blockLeftMirrorSprite,*blockMidMirrorSprite,*blockRightMirrorSprite,*blockSideMirrorSprite;
    Sprite *polluteToLeftSprite, *polluteToRightSprite;
    Sprite *polluteToLeftSprite2, *polluteToRightSprite2;
    Sprite *forbidUpSprite;
    Sprite *forbidDownSprite;
    Sprite *currentBadActionSprite;
    unsigned int effectIndex;
    void playPolluteEffect();
    
    Layer *rotateLayer;
    int negtive;
    void initRotateSprites();
    Array *rotateSpriteArray;
    Array *rotateSpriteArray0;
    Array *rotateSpriteArray1;
    Array *rotateSpriteArray2;
    Array *rotateSpriteArray3;
    Array *rotateSpriteNameArray;
    Array *rotateSpriteNameArray0;
    Array *rotateSpriteNameArray1;
    Array *rotateSpriteNameArray2;
    Array *rotateSpriteNameArray3;
    int r0,r1,r2,r3;
    bool onAction;
    void touchCallBack();
    
    int timeValue;
    int timeTotal;
    int lastTime;
    char timeString[10];
    void updateTime();
    void updateTimeEnd();
    void showTimeIncDecAction(bool inc);
    LabelTTF *timeLabel;
    void initTime();
    void timeOut(float);
    bool showNormal;
    void playTimeOutEffect();
    bool gameOver;
    Point clickedSpritePosition;
    int clickedLine, clickedIndex;
    void actionClikedOver(Sprite *clickedSprite, Sprite *sprite, int index);
    void stopEffectById(unsigned int index);
    
private:
    void resetGameLayer();
    Array *targets;
    void initItems();
    void initEternalItems();
    std::string getOneEternalItem();
    void updateTargets();
    void clickTarget(Sprite *targetSprite, const char *targetName);
    unsigned int rightTargetNum;
    unsigned int rightTargetTypeNum;
    Sprite *target0;
    Sprite *target1;
    Sprite *target2;
    LabelTTF *targetNum0;
    LabelTTF *targetNum1;
    LabelTTF *targetNum2;
    Array *currentTargets;//different target num
    Array *currentTargetNum;//num of every type target
    int totalTargetsNum;//total num of target
    int curTargetsNum;//current target nums
    int errorClickTimes;//record error click times
    //std::vector<int> currentTargetNum;
    //for debug
    void printAllTarget();
    void checkTargetNum();
    
    typedef enum{kGameRunning, kGamePaused, kGameOver} GameState;
    GameState mGameState;
    
    //for game pause only
    void initPauseGUI();
    Sprite *bgPauseSprite;
    MenuItemSprite *menuSelectButton;
    MenuItemSprite *menuAgainButton;
    MenuItemSprite *menuResumeButton;
    Menu *menuIngame;
    Menu *menuPause;
    bool canRemoveResource;
	Sprite *pauseGlassSprite;
	Sprite *pauseCleanSprite;
	Sprite *pauseClockSprite;
	Sprite *pauseRotateSprite;
	Sprite *pauseGlassSpriteNum;
	Sprite *pauseCleanSpriteNum;
	Sprite *pauseClockSpriteNum;
	Sprite *pauseRotateSpriteNum;
	MenuItemSprite *buyGlass;
	MenuItemSprite *buyClock;
	MenuItemSprite *buyRotate;
	MenuItemSprite *buyClean;
	LabelTTF *buyGlassNumLabel;
    LabelTTF *buyClockNumLabel;
    LabelTTF *buyRotateNumLabel;
    LabelTTF *buyCleanNumLabel;
	LabelTTF *pauseClockNumLabel;
    LabelTTF *pauseGlassNumLabel;
    LabelTTF *pauseRotateNumLabel;
    LabelTTF *pauseCleanNumLabel;
    Sprite *dialogSprite;
    bool isShowDialog;
    Sprite *stageSprite;
    LabelBMFont *stageInfoLabel;

    //for gameEnd
    void doGameWin();
    void doGameFailed();
    Menu *gameScoreMenu;
    MenuItemSprite *gameScoreSelectButton;
    MenuItemSprite *gameScoreAgainButton;
    MenuItemSprite *gameScoreNextButton;
    void gameOverButtonCallBack();
    void starEnd();
    
    LayerColor *fadeLayer;
    //for gameOver
    Menu *gameOverMenu;
    Sprite *gameOverFrame;
    MenuItemSprite *gameOverSelectButton;
    MenuItemSprite *gameOverAgainButton;
    MenuItemSprite *gameOverNextButton;
    
    //for coin shows
    LabelBMFont *coinBMFont;
    void initCoinBMFont();
    void updateCoin();
    unsigned int currentStageGetCoin;
    
    //for guide
    bool isGuideShow;
    unsigned int currentGuid;
    Sprite *guidSprite;
};

#endif /* defined(__notfound__GameLayer__) */
