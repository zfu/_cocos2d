//
//  ShopSelectLayer.h
//  notfound
//
//  Created by 付卓 on 13-11-11.
//
//

#ifndef __notfound__ShopSelectLayer__
#define __notfound__ShopSelectLayer__

#include <iostream>
#include <cocos2d.h>
USING_NS_CC;

class ShopSelectLayer : public Layer{
public:
    static Scene *scene();
    CREATE_FUNC(ShopSelectLayer);
    bool init();
    
    virtual void onEnterTransitionDidFinish();
    virtual void onExit();
    virtual bool ccTouchBegan(Touch *touch, Event *event);
    virtual void ccTouchMoved(Touch *touch, Event *event);
    virtual void ccTouchEnded(Touch *touch, Event *event);
    
    void buttonCallBack(Object* pSender);
    ~ShopSelectLayer();
private:
    Sprite *bg;
    Sprite *shopUpdateSprite;
    Sprite *shopBuyToolSprite;
    Sprite *shopBuyCoinSprite;
    Sprite *maskSprite0;
    Sprite *maskSprite1;
    Sprite *maskSprite2;
    Array *maskSprites;
    
    void initModeSprite();
    int current;
    Array *modeSprites;
    Layer *rotateLayer;
    bool inRotate;
    bool loadPlistFile;
    void rotateOver();
    
    Sprite *modeRotateSprite;
    MenuItemSprite *prevItemSprite;
    MenuItemSprite *nextItemSprite;
    MenuItemSprite *guideItemSprite;
    MenuItemSprite *returnSprite;
    
    void initModeRotateSprite();
    Array *modeRotateSprites;
    Layer *modeRotateLayer;
    Point beforePoint;
};

#endif /* defined(__notfound__ShopSelectLayer__) */
