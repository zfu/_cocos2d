//
//  ShopLayer.h
//  notfound
//
//  Created by 付卓 on 13-11-11.
//
//

#ifndef __notfound__ShopLayer__
#define __notfound__ShopLayer__

#include <cocos2d.h>
USING_NS_CC;

class ShopLayer : public Layer
{
public:
    typedef enum{kUpdateTool, kBuyTool, kBuyCoin, kDialog} State;
    CREATE_FUNC(ShopLayer);
    static Scene* scene(State gameState);
    bool init1();
    
    virtual void onEnterTransitionDidFinish();
    virtual void onExit();
    virtual bool ccTouchBegan(Touch *touch, Event *event);
    virtual void ccTouchMoved(Touch *touch, Event *event);
    virtual void ccTouchEnded(Touch *touch, Event *event);
private:
    State mState;
    State preState;
    void setState(State state);
    Array *shopItems;
    MenuItemSprite *returnSprite;
    MenuItemSprite *buyButton;
    void buttonCallBack(Object* pSender);
    unsigned int index;
    unsigned int leftIndex;
    unsigned int rightIndex;
    Sprite *leftSprite,*midSprite,*rightSprite;
    SpriteFrame *frame0,*frame1,*frame2,*frame3,*frame4,*frame5;
    void updateState();
    
    LabelTTF *nameLabel;//name
    LabelTTF *commentLabel;//if buy
    LabelTTF *currentCommentLabel;//current
    Sprite *stateSprite;
    LabelBMFont *stateLabel;
    
    LabelBMFont *coinLabel;//show total money
    LabelBMFont *priceLabel;//show the price
    Sprite *priceSprite;
    unsigned int currentPrice;
    Sprite *dialogSprite;
    Sprite *buyCoinSprite;
};

#endif /* defined(__notfound__ShopLayer__) */
