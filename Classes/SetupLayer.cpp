//
//  StageSelectLayer.cpp
//  notfound
//
//  Created by 付卓 on 13-11-5.
//
//

#include "SetupLayer.h"
#include "SimpleAudioEngine.h"
#include "GuidLayer.h"
#include "StartLayer.h"
#include "SimpleAudioEngine.h"
#include "StageSelectLayer.h"
#include "GameLayer.h"
#include "GameData.h"

Scene* SetupLayer::scene()
{
    Scene *scene = Scene::create();
    SetupLayer *layer = SetupLayer::create();
    scene->addChild(layer);
    return scene;
}

bool SetupLayer::init()
{
	flags = kNormal;
	//musicOn = true;
    musicOn = GameData::getInstance()->iSBackGroundMusicOn();
	//effectOn = true;
    effectOn = GameData::getInstance()->isEffectOn();
    
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("setup-hd.plist");
    
    bg = Sprite::createWithSpriteFrameName("mode_setup_bk.png");
    this->addChild(bg);
    Size size = Director::getInstance()->getVisibleSize();
    bg->setPosition(Point(size.width/2, size.height/2));
    Size bgSize = bg->getContentSize();
 //   float scale = (size.width/bgSize.width)>(size.height/bgSize.height)?(size.width/bgSize.width):(size.height/bgSize.height);
   float scale = 2.0;
	log("scale = %f", scale);
    bg->setScale(scale);

	if(musicOn){
        musicOnSprite = Sprite::createWithSpriteFrameName("mode_setup_button_music_selected.png");
        musicOffSprite = Sprite::createWithSpriteFrameName("mode_setup_button_music.png");
    }else{
        musicOffSprite = Sprite::createWithSpriteFrameName("mode_setup_button_music_selected.png");
        musicOnSprite = Sprite::createWithSpriteFrameName("mode_setup_button_music.png");
    }

	musicSprite = MenuItemSprite::create(musicOnSprite,musicOffSprite, CC_CALLBACK_1(SetupLayer::buttonMusic, this));
	//musicSprite->setScale(0.5);
	musicSprite->setPosition(Point(size.width/2 + 213, size.height*290/768 ));//is it need to modify.
    
    if(effectOn){
        effectOnSprite = Sprite::createWithSpriteFrameName("mode_setup_button_effect_selected.png");
        effectOffSprite = Sprite::createWithSpriteFrameName("mode_setup_button_effect.png");
    }else{
        effectOffSprite = Sprite::createWithSpriteFrameName("mode_setup_button_effect_selected.png");
        effectOnSprite = Sprite::createWithSpriteFrameName("mode_setup_button_effect.png");
    }
	effectSprite = MenuItemSprite::create(effectOnSprite, effectOffSprite, CC_CALLBACK_1(SetupLayer::buttonEffect, this));
	//effectSprite->setScale(0.5);
	effectSprite->setPosition(Point(size.width/2 -403, size.height*130/768 ));//is it need to modify.
    
	trashSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("mode_setup_reset.png"), Sprite::createWithSpriteFrameName("mode_setup_reset_celected.png"), CC_CALLBACK_1(SetupLayer::buttonResetData, this));
	//trashSprite->setScale(0.5);
	trashSprite->setPosition(Point(size.width/2 - 405 , size.height*520/768 ));//is it need to modify.
    
	producerSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("mode_setup_about.png"), Sprite::createWithSpriteFrameName("mode_setup_about.png"), CC_CALLBACK_1(SetupLayer::buttonShowInformation, this));
	//producerSprite->setScale(0.5);
	producerSprite->setPosition(Point(size.width/2 + 310, size.height*40/40  ));//is it need to modify.
	producerSprite->setAnchorPoint(Point(0.5, 1));
    initModeSprite();
    
	modeSetupEffectSprite = Sprite::createWithSpriteFrameName("mode_setup_effect.png");
	modeSetupEffectSprite->setPosition(Point(size.width/2 - 160, size.height*180/768 ));//is it need to modify.
    if(!effectOn) modeSetupEffectSprite->setVisible(false);
	//modeSetupEffectSprite->setScale(0.5);
	modeSetupMusicSprite = Sprite::createWithSpriteFrameName("mode_setup_music.png");
	modeSetupMusicSprite->setPosition(Point(size.width/2 + 90, size.height*560/768 ));//is it need to modify.
    if(!musicOn) modeSetupMusicSprite->setVisible(false);
	//modeSetupMusicSprite->setScale(0.5);
	this->addChild(modeSetupEffectSprite);
	this->addChild(modeSetupMusicSprite, 10);
    
    ScaleTo *scaleToUp1 = ScaleTo::create(0.5, 1.2);
    ScaleTo *scaleToDown1 = ScaleTo::create(0.5, 1);
    Sequence *seq1 = Sequence::create(scaleToUp1, scaleToDown1, NULL);
    RepeatForever *action1 = RepeatForever::create(seq1);
    modeSetupMusicSprite->runAction(action1);
    
    ScaleTo *scaleToUp2 = ScaleTo::create(0.5, 1.2);
    ScaleTo *scaleToDown2 = ScaleTo::create(0.5, 1);
    Sequence *seq2 = Sequence::create(scaleToUp2, scaleToDown2, NULL);
    RepeatForever *action2 = RepeatForever::create(seq2);
    modeSetupEffectSprite->runAction(action2);

	returnSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("being_return2.png"), Sprite::createWithSpriteFrameName("being_return2_selected.png"), CC_CALLBACK_1(SetupLayer::buttonReturn, this));
    Size spriteSize = returnSprite->getContentSize();
    //returnSprite->setScale(0.5);
    returnSprite->setPosition(size.width - spriteSize.width/2 - 5, size.height - spriteSize.height/2 - 5);
    
    menu = Menu::create(trashSprite, musicSprite, effectSprite, producerSprite, returnSprite/*, guideItemSprite*/, NULL);
    menu->setPosition(Point::ZERO);
    this->addChild(menu, 10);
    
    MoveBy *moveUp = MoveBy::create(0.5, Point(0, 20));
    MoveBy *moveDown = MoveBy::create(0.5, Point(0, -20));
    Sequence *moveUpDownSeq = Sequence::create(moveUp, moveDown, NULL);
    trashSprite->runAction(RepeatForever::create(moveUpDownSeq));
    
	RotateBy *rotateLeft = RotateBy::create(2, 5);
	RotateBy *rotateRight = RotateBy::create(2, -5);
	Sequence *rotateLeftRight = Sequence::create(rotateLeft, rotateRight, NULL);
	producerSprite->runAction(RepeatForever::create(rotateLeftRight));

    return true;
}

void SetupLayer::initModeSprite()
{
	Size size = Director::getInstance()->getVisibleSize();
    
	informationSprite  = Sprite::createWithSpriteFrameName("mode_setup_aboutMsg.png");
	//informationSprite->setAnchorPoint(Point(0.0f,0.0f));
	Size informationSize = informationSprite->getContentSize();
    float scale = 1.0;
    informationSprite->setScale(scale);
	//informationSprite->setScale(0.5);
	this->addChild(informationSprite);
	informationSprite->setPosition(Point(size.width/2, size.height/2));
	informationSprite->setVisible(false);
    
	resetDateLayer = LayerRGBA::create();
	resetDateLayer->setZOrder(1);
	resetDateLayer->setVisible(false);
	this->addChild(resetDateLayer, 20);
    
	resetbg = Sprite::createWithSpriteFrameName("setup_dialogbox_bk.png");
	//resetbg->setScale(0.5);
	resetbg->setPosition(Point(size.width/2, size.height/2));
	resetDateLayer->addChild(resetbg);
    
    yesButtonSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("setup_dialogbox_btn_confirm.png"), Sprite::createWithSpriteFrameName("setup_dialogbox_btn_confirm_selected.png"), CC_CALLBACK_1(SetupLayer::buttonYes, this));
	//yesButtonSprite->setScale(0.5);
	Size yesButtonSize = yesButtonSprite->getContentSize();
	yesButtonSprite->setPosition(Point(size.width/2 - yesButtonSize.width*7/16 + yesButtonSize.width*3/64 , size.height/4 ));//is it need to modify.
	noButtonSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("setup_dialogbox_btn_cancel.png"), Sprite::createWithSpriteFrameName("setup_dialogbox_btn_cancel_selected.png"), CC_CALLBACK_1(SetupLayer::buttonNo, this));
	//noButtonSprite->setScale(0.5);
	Size noButtonSize = noButtonSprite->getContentSize();
	noButtonSprite->setPosition(Point(size.width/2 + noButtonSize.width/4 + yesButtonSize.width*22/64, size.height/4 + noButtonSize.height*6/32 - noButtonSize.height/32));//is it need to modify.
	Menu* menu_reset = Menu::create(yesButtonSprite, noButtonSprite/*, guideItemSprite*/, NULL);
    menu_reset->setPosition(Point::ZERO);
    resetDateLayer->addChild(menu_reset);
}

void SetupLayer::onEnterTransitionDidFinish()
{
    Director::getInstance()->getTouchDispatcher()->addTargetedDelegate(this, 1, true);
    Layer::onEnterTransitionDidFinish();
}

void SetupLayer::onExit()
{
    Director::getInstance()->getTouchDispatcher()->removeDelegate(this);
    Layer::onExit();
}

bool SetupLayer::ccTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    return true;
}

void SetupLayer::ccTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
    //    log("touch Moved");
}

void SetupLayer::ccTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
    musicSprite->setVisible(true);
	effectSprite->setVisible(true);
	menu->setVisible(true);
	if(effectOn) modeSetupEffectSprite->setVisible(true);
	if(musicOn) modeSetupMusicSprite->setVisible(true);
	trashSprite->setVisible(true);
	producerSprite->setVisible(true);
	informationSprite->setVisible(false);
	if ( flags == kInformation) {
		flags = kNormal;
		if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
	}
	return;
}

SetupLayer::~SetupLayer()
{
	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("setup-hd.plist");
}

void SetupLayer::buttonReturn(Object* pSender)
{
	if ( flags != kNormal)
		return ;
	if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
	Scene *scene = StartLayer::scene();
    TransitionFade *fade = TransitionFade::create(0.3, scene, Color3B::BLACK);
    Director::getInstance()->replaceScene(fade);
}
void SetupLayer::buttonMusic(Object* pSender)
{
    log("button Music!!");
	SpriteFrame * musicOnTemp;
	SpriteFrame * musicOffTemp;
	musicOnTemp = SpriteFrameCache::getInstance()->getSpriteFrameByName("mode_setup_button_music_selected.png");
	musicOffTemp = SpriteFrameCache::getInstance()->getSpriteFrameByName("mode_setup_button_music.png");
    if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
	if (musicOn == true) {
        log("on-->off");
		musicOffSprite->setDisplayFrame(musicOnTemp);
		musicOnSprite->setDisplayFrame(musicOffTemp);
		musicOn = false;
        modeSetupMusicSprite->setVisible(false);
        GameData::getInstance()->setBackGroundMusicOn(false);
        CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	}
	else if (musicOn == false) {
        log("off-->on");
		musicOnSprite->setDisplayFrame(musicOnTemp);
		musicOffSprite->setDisplayFrame(musicOffTemp);
		musicOn = true;
        modeSetupMusicSprite->setVisible(true);
        GameData::getInstance()->setBackGroundMusicOn(true);
        if(GameData::getInstance()->iSBackGroundMusicOn())
            CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("mainMenu.mp3", true);
	}
}
void SetupLayer::buttonEffect(Object* pSender)
{
	SpriteFrame * effectOnTemp;
	SpriteFrame * effectOffTemp;
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
	effectOnTemp = SpriteFrameCache::getInstance()->getSpriteFrameByName("mode_setup_button_effect_selected.png");
	effectOffTemp = SpriteFrameCache::getInstance()->getSpriteFrameByName("mode_setup_button_effect.png");
    log("effect music");
	if (effectOn == true) {
        log("effect on--->off");
		effectOnSprite->setDisplayFrame(effectOffTemp);
		effectOffSprite->setDisplayFrame(effectOnTemp);
		effectOn = false;
        modeSetupEffectSprite->setVisible(false);
        GameData::getInstance()->setEffectOn(false);
	}
	else if (effectOn == false) {
        log("effect off--->on");
		effectOnSprite->setDisplayFrame(effectOnTemp);
		effectOffSprite->setDisplayFrame(effectOffTemp);
		effectOn = true;
        modeSetupEffectSprite->setVisible(true);
        GameData::getInstance()->setEffectOn(true);
	}
}

void SetupLayer::buttonShowInformation(Object* pSender)
{
	if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
	flags = kInformation;
	musicSprite->setVisible(false);
	effectSprite->setVisible(false);
	modeSetupEffectSprite->setVisible(false);
	modeSetupMusicSprite->setVisible(false);
	menu->setVisible(false);
	trashSprite->setVisible(false);
	producerSprite->setVisible(false);
	informationSprite->setVisible(true);
	return;
}


void SetupLayer::buttonResetData(Object* pSender)
{
	if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
	flags = kReset;
	resetDateLayer->setVisible(true);
	menu->setTouchEnabled(false);
	return;
}

void SetupLayer::buttonYes(Object* pSender)
{
	if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
	menu->setTouchEnabled(true);
	resetDateLayer->setVisible(false);
	flags = kNormal;
	log("yes");
    GameData::getInstance()->resetAllData();
	return;
}
void SetupLayer::buttonNo(Object* pSender)
{
	if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
	menu->setTouchEnabled(true);
	resetDateLayer->setVisible(false);
	flags = kNormal;
	log("no");
	return;
}

