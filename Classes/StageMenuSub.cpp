//
//  StageSelectLayer.cpp
//  notfound
//
//  Created by 付卓 on 13-11-5.
//
//

#include "StageMenuSub.h"
#include "SimpleAudioEngine.h"
#include "GuidLayer.h"
#include "StartLayer.h"
#include "SimpleAudioEngine.h"
#include "StageSelectLayer.h"
#include "GameLayer.h"
#include "GameData.h"

Scene* StageMenuSub::scene()
{
    Scene *scene = Scene::create();
    StageMenuSub *layer = StageMenuSub::create();
    scene->addChild(layer);
    return scene;
}

bool StageMenuSub::init()
{
    //  SpriteFrameCache::getInstance()->addSpriteFramesWithFile("beding-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("stageBack-hd.plist");
    //  SpriteFrameCache::getInstance()->addSpriteFramesWithFile("stageBg-hd.plist");
    //  SpriteFrameCache::getInstance()->addSpriteFramesWithFile("stageLogo-hd.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("stageMenuSub-hd.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bg_stage_sub-hd.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("font-hd.plist");
    
    Sprite *titleSprite = NULL;//Sprite::createWithSpriteFrameName("");
    if(GameData::getInstance()->tmpData->gameLayerType == TempData::kGameTypeBuffet){
        //bg = Sprite::createWithSpriteFrameName("stageBack_buffet.png");
        titleSprite = Sprite::createWithSpriteFrameName("font_buffet.png");
        //titleSprite->setScale(0.5);
    }else if(GameData::getInstance()->tmpData->gameLayerType == TempData::kGameTypeToyCastle){
        //bg = Sprite::createWithSpriteFrameName("stageBack_toyCastle.png");
        titleSprite = Sprite::createWithSpriteFrameName("font_toyCastle.png");
        //titleSprite->setScale(0.5);
    }else if(GameData::getInstance()->tmpData->gameLayerType == TempData::kGameTypeTvShow){
        //bg = Sprite::createWithSpriteFrameName("stageBack_tvShow.png");
        titleSprite = Sprite::createWithSpriteFrameName("font_tvShow.png");
        //titleSprite->setScale(0.5);
    }else if(GameData::getInstance()->tmpData->gameLayerType == TempData::kGameTypeBazaar){
        //bg = Sprite::createWithSpriteFrameName("stageBack_bazaar.png");
        titleSprite = Sprite::createWithSpriteFrameName("font_bazaar.png");
        //titleSprite->setScale(0.5);
    }
    bg = Sprite::createWithSpriteFrameName("bg_stage_sub-hd.png");
    this->addChild(bg);
    Size size = Director::getInstance()->getVisibleSize();
    bg->setPosition(Point(size.width/2, size.height/2));
    Size bgSize = bg->getContentSize();
    float scale = (size.width/bgSize.width)>(size.height/bgSize.height)?(size.width/bgSize.width):(size.height/bgSize.height);
    log("scale = %f", scale);
    bg->setScale(scale);
    
	headSprite = Sprite::createWithSpriteFrameName("stageMenuSub_title_bar.png");
	//headSprite->setScale(0.5);
	this->addChild(headSprite);
	headSprite->setPosition(Point(size.width/2, size.height*37/40 ));//is it need to modify.

    Size winSize = Director::getInstance()->getVisibleSize();
    Size itemSize = titleSprite->getContentSize();
    titleSprite->setPosition(Point(winSize.width/2, winSize.height - itemSize.height+20));
    this->addChild(titleSprite);
    initModeSprite();
  
    returnSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("being_return2.png"), Sprite::createWithSpriteFrameName("being_return2_selected.png"), CC_CALLBACK_1(StageMenuSub::buttonReturn, this));
    Size spriteSize = returnSprite->getContentSize();
    //returnSprite->setScale(0.5);
    returnSprite->setPosition(size.width - spriteSize.width/2 - 5, size.height - spriteSize.height/2 - 5);
    
     Menu* menu = Menu::create(returnSprite/*, guideItemSprite*/, NULL);
     menu->setPosition(Point::ZERO);
     this->addChild(menu);
    
    //play the music
    //CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    if(!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
        if(GameData::getInstance()->iSBackGroundMusicOn())
            CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("mainMenu.mp3", true);
    
    return true;
}

void StageMenuSub::initModeSprite()
{
 
    Size size = Director::getInstance()->getVisibleSize();
	customsPassSprites = Array::createWithCapacity(15);
	chooseLayer = LayerRGBA::create();
    
    char name[4];
    unsigned int totalStar = 0;
	for (int i = 0; i < 15; i++) {
		customsPassSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("stageMenuSub_frame.png"), Sprite::createWithSpriteFrameName("stageMenuSub_frame_selected.png"), CC_CALLBACK_1(StageMenuSub::buttonChoose, this));
		customsPassSprites->addObject(customsPassSprite);
		customsPassSprite->setPosition(Point(size.width*(i%5 + 1)/6, size.height*3/4 - 50 - size.height*(i/5)/4));
        Size s = customsPassSprite->getContentSize();
		//customsPassSprite->setScale(0.5);
        
        sprintf(name, "%d", i+1);
        //LabelTTF *label = LabelTTF::create(name, "STHeitiTC-Bold", 70);
        LabelBMFont *label = LabelBMFont::create(name, "number_stage_sub.fnt");
        Size itemSize = customsPassSprite->getContentSize();
        //label->setColor(Color3B::BLACK);
        label->setPosition(Point(itemSize.width/2, itemSize.height/2+32));
        customsPassSprite->addChild(label);
        
        int stage = GameData::getInstance()->currentStage;
        Sprite *lockSprite=NULL;
        if(GameData::getInstance()->isStageLocked(stage, i+1)){
            lockSprite = Sprite::createWithSpriteFrameName("stageMenuSub_lock.png");//for lock picture
            lockSprite->setPosition(Point(s.width/2, s.height/2));
            customsPassSprite->addChild(lockSprite);
        }

        int starNum = GameData::getInstance()->getStarNum(stage, i+1);
        
		if(starNum == 1){
			starsSprite = Sprite::createWithSpriteFrameName("stageMenuSub_one_star.png");
		}else if(starNum == 2){
			starsSprite = Sprite::createWithSpriteFrameName("stageMenuSub_two_star.png");
		}else if(starNum == 3) {
			starsSprite = Sprite::createWithSpriteFrameName("stageMenuSub_three_star.png");
		}
        /*
		if (i%3 == 0) {
			starsSprite = Sprite::createWithSpriteFrameName("stageMenuSub_one_star.png");
		}
		else if (i%3 == 1) {
			starsSprite = Sprite::createWithSpriteFrameName("stageMenuSub_two_star.png");
		}
		else if ( i % 3 == 2) {
			starsSprite = Sprite::createWithSpriteFrameName("stageMenuSub_three_star.png");
		}
         */
        if(starNum>0){
            //starsSprite->setAnchorPoint(Point(0.5f,1.0f));
            Size s2 = starsSprite->getContentSize();
            starsSprite->setPosition(Point(s.width/2, -s2.height/2));
            customsPassSprite->addChild(starsSprite);
        }
        totalStar+=starNum;
	}
	Menu* menu = Menu::createWithArray(customsPassSprites);
    menu->setPosition(Point::ZERO);
    this->addChild(menu);
    customsPassSprites->retain();
    
    char startNumText[64];
    sprintf(startNumText, "%d/%d", totalStar, 3*15);
    LabelBMFont *starNumLabel = LabelBMFont::create(startNumText, "number_dollar.fnt");
    this->addChild(starNumLabel);
    starNumLabel->setPosition(Point(size.width/2, 60));
}

void StageMenuSub::buttonChoose(Object* pSender)
{
    int i;
    for (i = 0; i < 15; i++) {
		if (((Sprite*)customsPassSprites->objectAtIndex(i)) == pSender) {
			log("in choose %d function\n", i);
			break;
		}
	}
    int stage = GameData::getInstance()->currentStage;
    if(GameData::getInstance()->isStageLocked(stage, i+1)) return;
    
    if(GameData::getInstance()->tmpData->gameLayerType == TempData::kGameTypeBuffet){
        GameData::getInstance()->setCurrentGame(1, i+1);
        log("+++++++++++ Game Staget[%d,%d]", 1, i+1);
    }else if(GameData::getInstance()->tmpData->gameLayerType == TempData::kGameTypeToyCastle){
        GameData::getInstance()->setCurrentGame(2, i+1);
        log("+++++++++++ Game Staget[%d,%d]", 2, i+1);
    }else if(GameData::getInstance()->tmpData->gameLayerType == TempData::kGameTypeTvShow){
        GameData::getInstance()->setCurrentGame(3, i+1);
        log("+++++++++++ Game Staget[%d,%d]", 3, i+1);
    }else if(GameData::getInstance()->tmpData->gameLayerType == TempData::kGameTypeBazaar){
        GameData::getInstance()->setCurrentGame(4, i+1);
        log("+++++++++++ Game Staget[%d,%d]", 4, i+1);
    }
	log("in choose function\n");
	if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
    Scene *scene = GameLayer::scene();
    TransitionFade *fade = TransitionFade::create(0.3, scene, Color3B::BLACK);
    Director::getInstance()->replaceScene(fade);
    return;
}

void StageMenuSub::onEnterTransitionDidFinish()
{
    //log("StageSelectLayer on Enter");
    Director::getInstance()->getTouchDispatcher()->addTargetedDelegate(this, 1, true);
    Layer::onEnterTransitionDidFinish();
}

void StageMenuSub::onExit()
{
    //log("StageSelectLayer on Exit");
    Director::getInstance()->getTouchDispatcher()->removeDelegate(this);
    Layer::onExit();
}

bool StageMenuSub::ccTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    return true;
}

void StageMenuSub::ccTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
}

void StageMenuSub::ccTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
}

StageMenuSub::~StageMenuSub()
{
	customsPassSprites->release();
    // SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("beding-hd.plist");
    // SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("stageBack-hd.plist");
    // SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("stageBg-hd.plist");
    // SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("stageLogo-hd.plist");
	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("stageMenuSub-hd.plist");
}

void StageMenuSub::buttonReturn(Object* pSender)
{
	if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
    Scene *scene = StageSelectLayer::scene();
    TransitionFade *fade = TransitionFade::create(0.3, scene, Color3B::BLACK);
    Director::getInstance()->replaceScene(fade);
}
