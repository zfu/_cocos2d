//
//  StageSlectLayer.h
//  notfound
//
//  Created by 付卓 on 13-11-5.
//
//

#ifndef __notfound__StageSelectLayer__
#define __notfound__StageSelectLayer__

#include <cocos2d.h>
USING_NS_CC;

class StageSelectLayer : public Layer{
public:
    static Scene *scene();
    CREATE_FUNC(StageSelectLayer);
    bool init();
    
    virtual void onEnterTransitionDidFinish();
    virtual void onExit();
    virtual bool ccTouchBegan(Touch *touch, Event *event);
    virtual void ccTouchMoved(Touch *touch, Event *event);
    virtual void ccTouchEnded(Touch *touch, Event *event);
    
    void buttonCallBack(Object* pSender);
    ~StageSelectLayer();
private:
    Sprite *bg;
    Sprite *bg2;
    Sprite *buffetSprite;
    Sprite *toyCastleSprite;
    Sprite *tvShowSprite;
    Sprite *bazaarSprite;
    Sprite *blankSprite;
    Sprite *maskSprite0;
    Sprite *maskSprite1;
    Sprite *maskSprite2;
    Sprite *maskSprite3;
    Sprite *maskSprite4;
    Array *maskSprites;
    void initModeSprite();
    int current;
    Array *modeSprites;
    Layer *rotateLayer;
    bool inRotate;
    bool loadPlistFile;
    void rotateOver();
    
    Sprite *modeRotateSprite;
    MenuItemSprite *prevItemSprite;
    MenuItemSprite *nextItemSprite;
    MenuItemSprite *guideItemSprite;
    MenuItemSprite *returnSprite;
    
    void initModeRotateSprite();
    Array *modeRotateSprites;
    Layer *modeRotateLayer;
    Point beforePoint;
};

#endif /* defined(__notfound__StageSelectLayer__) */
