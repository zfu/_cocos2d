//
//  GameData.h
//  notfound
//
//  Created by 付卓 on 13-11-12.
//
//

#ifndef __notfound__GameData__
#define __notfound__GameData__

#include <iostream>
#include <cocos2d.h>
#include "ConfigINI.h"

USING_NS_CC;
class TempData
{
public:
    TempData(){
        startLayerMode = KModeTypeInvalid;
        gameLayerType = kGameTypeInvalid;
        shopTypeMode = kShopTypeInvalid;
        handBookType = kHandBookTypeInvalid;
    }
    typedef enum{kModeHandBook, kModeSetup, kModeGame, kModeEternal, kModeShop, KModeTypeInvalid}ModeType;
    ModeType startLayerMode;
    typedef enum{kGameTypeBuffet, kGameTypeToyCastle, kGameTypeTvShow, kGameTypeBazaar, kGameEternal, kGameTypeInvalid}GameType;
    GameType gameLayerType;
    typedef enum{kModeUpdateTool, kModeBuyTool, kModeBuyCoin, kShopTypeInvalid}ShopType;
    ShopType shopTypeMode;
    typedef enum{kHandBookBuffet, kHandBookToyCastle, kHandBookTvShow, kHandBookBazaar, kHandBookTypeInvalid}HandBookType;
    HandBookType handBookType;
};

//class handBookEntry
//{
//public:
//    char name[1024];
//    char fileName[1024];
//    char desc[1024];
//    int timeNeed;
//    int TimeCur;
//};

class stageEntry
{
public:
    //unsigned int stage;
    bool locked[15];
    unsigned int starNum[15];//total 15 sub stages
    bool selfLocked;
};

class toolsEntry
{
public:
    char name[1024];
    unsigned int level;
    unsigned int num;
};

class GameData : public Object
{
public:
    CREATE_FUNC(GameData);
    bool init();
    static GameData *getInstance();
    ~GameData();
    TempData *tmpData;
    void resetAllData();
    
    //for hand book
public:
    string getHandBookDataFileName(unsigned int stage, unsigned int index);
    string getHandBookDataDesc(unsigned int stage, unsigned int index);
    string getHandBookDataName(unsigned int stage, unsigned int index);
    
    bool isHandBookLocked(unsigned int stage, unsigned int index);
    void updateHandBookItem(std::string fileName);//call in game
    unsigned int getTotalUnlockedNum();//all unlocked num
    unsigned int getStageUnlockedNum(unsigned int stage);//unlocked item num for current stage
    unsigned int getStageItemUnlockNum(unsigned int stage, unsigned int index);
private:
//    handBookEntry buffetHandBookData[20];
//    handBookEntry toyCastleHandBookData[20];
//    handBookEntry tvShowHandBookData[20];
//    handBookEntry bazaarHandBookData[20];
    ConfigINI *buffetINI;
    ConfigINI *toyCastleINI;
    ConfigINI *tvShowINI;
    ConfigINI *bazaarINI;
    void initHandBookData();
    
public:
    //for tools and coins
    typedef enum{kToolClock, kToolGlass, kToolRotate, kToolClean, kToolTotalNum} ToolType;
    
    //for skills
    ConfigINI *skillINI;
    void initSkillInfo();
    string getSkillName(unsigned int index);
    string getSkillComment(unsigned int index);
    string getSkillComment(unsigned int index, unsigned int level);
    string getToolName(unsigned int index);
    string getToolComment(unsigned int index);
    unsigned int getToolPrice(unsigned int index);
    unsigned int getSkillminValue(unsigned int index, unsigned int level);
    unsigned int getSkillmaxValue(unsigned int index, unsigned int level);
    unsigned int getSkillValue(unsigned int index, unsigned int level);
    unsigned int getSkillPrice(unsigned int index, unsigned int level);
    
    unsigned int getSkillLevel(unsigned int index);
    void addSkillLevel(unsigned int index);
    string getCoinName(unsigned int index);
    string getCoinComment(unsigned int index);
    unsigned int getCoinPrice(unsigned int index);
    
    //for menu sub stage
    stageEntry stageData[4];
    void initStageInfo();
    void resetStageInfo();
    void saveStageInfo();
    bool isStageLocked(int stage, int subStage);
    bool isLargeStageLocked(int stage);
    bool isNextStageLocked();
    bool updateStar(int stage, int subStage, int starNum);
    bool updateCurrentStageStar(int startNum);
    int getStarNum(int stage, int subStage);
    ConfigINI *stageINI;
    
    toolsEntry toolData[kToolTotalNum];//total 4 type tools
    unsigned int coinNums;
    unsigned int getCoinNums(){return coinNums;};
    void addCoinNum(unsigned int num){
        coinNums+=num;
        gameDataINI->setIntValue("coin", "num", coinNums);
        gameDataINI->writeConfigFile();
    }
    bool useCoinNum(unsigned int num){
        if(num <= coinNums){
            coinNums-=num;
            gameDataINI->setIntValue("coin", "num", coinNums);
            gameDataINI->writeConfigFile();
            return true;
        }else return false;
    }
    ConfigINI *gameDataINI;
    void loadGameData();
    bool isFirstTime(std::string str);
    
    //for tools
    unsigned int getToolNum(ToolType type){return toolData[type].num;}
    unsigned int getToolLevel(ToolType type){return toolData[type].level;}
    void initTools();
    void updateTools();
    void addToolNum(ToolType type, unsigned int num){
        toolData[type].num+=num;
        if(type == kToolClock) gameDataINI->setIntValue("clock", "num", toolData[type].num);
        else if(type == kToolGlass) gameDataINI->setIntValue("glass", "num", toolData[type].num);
        else if(type == kToolRotate) gameDataINI->setIntValue("rotate", "num", toolData[type].num);
        else if(type == kToolClean) gameDataINI->setIntValue("clean", "num", toolData[type].num);
        else CCASSERT(false, "shoud not go here");
        gameDataINI->writeConfigFile();
    }
    bool useTool(ToolType type){
        if(toolData[type].num >0){
            toolData[type].num--;
            if(type == kToolClock) gameDataINI->setIntValue("clock", "num", toolData[type].num);
            else if(type == kToolGlass) gameDataINI->setIntValue("glass", "num", toolData[type].num);
            else if(type == kToolRotate) gameDataINI->setIntValue("rotate", "num", toolData[type].num);
            else if(type == kToolClean) gameDataINI->setIntValue("clean", "num", toolData[type].num);
            else CCASSERT(false, "shoud not go here");
            gameDataINI->writeConfigFile();
            return true;
        }else return false;
    }
    
    //for story mode
    void getTarget(int *num, int **targets);
    void getEternalTarget(int *num, int **targets);
    unsigned int getEternalItem();
    void setCurrentGame(int stage, int subStage);//{currentStage = stage; currentSubStage=subStage;}
    int currentStage;
    int currentSubStage;
    
    //setup control
    bool iSBackGroundMusicOn(){return backGroundMusicOn;}
    bool isEffectOn(){return effectOn;}
    void setBackGroundMusicOn(bool value);//{backGroundMusicOn = value; saveMusicEffect();}
    void setEffectOn(bool value);//{effectOn = value;saveMusicEffect();}
    
private:
    void loadMusicEffect();
    void saveMusicEffect();
    ConfigINI *musicEffectIni;
    bool backGroundMusicOn;
    bool effectOn;
    void initAndroidData();
    void copyData(const char *fileName);
};
#endif /* defined(__notfound__GameData__) */
