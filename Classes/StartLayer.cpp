//
//  StartLayer.cpp
//  notfound
//
//  Created by 付卓 on 13-11-1.
//
//

#include "StartLayer.h"
#include "SimpleAudioEngine.h"
#include "GuidLayer.h"
#include "StageSelectLayer.h"
#include "handBookLayer.h"
#include "SetupLayer.h"
#include "ShopSelectLayer.h"
#include "GameLayer.h"
#include "GameData.h"

#define PI 3.14159265
#define ANG 45
#define TIME 0.4

Scene* StartLayer::scene()
{
    Scene *scene = Scene::create();
    StartLayer *layer = StartLayer::create();
    if(layer){
        scene->addChild(layer);
        return scene;
    }
    return NULL;
}

bool StartLayer::init()
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bg_logo-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("mode-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("modeRotate-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("modebg-hd.plist");
    asyncLoadAllResources();
    
    TempData *tmpData = GameData::getInstance()->tmpData;
    if(tmpData->startLayerMode == TempData::KModeTypeInvalid){
        //log("startLayerMode invalid set default GameMode");
        tmpData->startLayerMode = TempData::kModeGame;
    }
    
    if(GameData::getInstance()->tmpData->startLayerMode == TempData::kModeHandBook){
        bg = Sprite::createWithSpriteFrameName("bg_achieve.png");
        bg2 = Sprite::createWithSpriteFrameName("bg_achieve.png");
    }else if(GameData::getInstance()->tmpData->startLayerMode == TempData::kModeSetup){
        bg = Sprite::createWithSpriteFrameName("bg_setup.png");
        bg2 = Sprite::createWithSpriteFrameName("bg_setup.png");
    }else if(GameData::getInstance()->tmpData->startLayerMode == TempData::kModeGame){
        bg = Sprite::createWithSpriteFrameName("bg_story.png");
        bg2 = Sprite::createWithSpriteFrameName("bg_story.png");
    }else if(GameData::getInstance()->tmpData->startLayerMode == TempData::kModeEternal){
        bg = Sprite::createWithSpriteFrameName("bg_eternal.png");
        bg2 = Sprite::createWithSpriteFrameName("bg_eternal.png");
    }else if(GameData::getInstance()->tmpData->startLayerMode == TempData::kModeShop){
        bg = Sprite::createWithSpriteFrameName("bg_shop.png");
        bg2 = Sprite::createWithSpriteFrameName("bg_shop.png");
    }else{
        CCASSERT(true, "ERROR game mode");
    }
    
    this->addChild(bg2);
    this->addChild(bg);
    Size size = Director::getInstance()->getVisibleSize();
    bg->setPosition(Point(size.width/2, size.height/2));
    bg2->setPosition(Point(size.width/2, size.height/2));
    Size bgSize = bg->getContentSize();
    float scale = (size.width/bgSize.width)>(size.height/bgSize.height)?(size.width/bgSize.width):(size.height/bgSize.height);
    log("scale = %f", scale);
    bg->setScale(scale);
    bg2->setScale(scale);
    
    Sprite *modeLogo = Sprite::createWithSpriteFrameName("bg_logo-hd.png");
    Size logoSize = modeLogo->getContentSize();
    //modeLogo->setScale(0.5);
    modeLogo->setPosition(Point(size.width/2, size.height-logoSize.height/2));
    this->addChild(modeLogo);
    
    initModeSprite();//init sprite rotate with rotateLayer
    
    //modeRotateSprite = Sprite::createWithSpriteFrameName("rotate_story.png");
    //modeRotateSprite->setPosition(Point(size.width/2, -size.height/20));
    //modeRotateSprite->setScale(0.5);
    //this->addChild(modeRotateSprite);
    
    //for next and prev button
    nextItemSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("being_all.png"), Sprite::createWithSpriteFrameName("being_all_selected.png"), CC_CALLBACK_1(StartLayer::buttonCallback, this));
    //nextItemSprite->setScale(0.5);
	nextItemSprite->setPosition(size.width*5/6, size.height/6);
    nextItemSprite->setVisible(false);
    
    prevItemSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("being_all.png"), Sprite::createWithSpriteFrameName("being_all_selected.png"), CC_CALLBACK_1(StartLayer::buttonCallback, this));
    //prevItemSprite->setScale(0.5);
    prevItemSprite->setRotation(-180.0);
	prevItemSprite->setPosition(size.width*1/6, size.height/6);
    prevItemSprite->setVisible(false);
    
    guideItemSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("being_guid.png"), Sprite::createWithSpriteFrameName("being_guid_selected.png"), CC_CALLBACK_1(StartLayer::buttonCallback, this));
    //guideItemSprite->setScale(0.5);
	guideItemSprite->setPosition(size.width*9/10, size.height*8/9);
    
    Menu* menu = Menu::create(nextItemSprite, prevItemSprite, guideItemSprite, NULL);
    menu->setTag(123);
    menu->setPosition(Point::ZERO);
    this->addChild(menu);
    
    //play the music if no music and background music is ON
    if(!(CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())){
    //CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
        if(GameData::getInstance()->iSBackGroundMusicOn())
            CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("mainMenu.mp3", true);
    }
    
    isGuideShow = false;
    if(GameData::getInstance()->isFirstTime("game")){
        log("yes first time");
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("zoom.mp3");
        isGuideShow = true;
        guidSprite = Sprite::createWithSpriteFrameName("guid_0.png");
        guidSprite->setPosition(Point(size.width/2, size.height/2));
        Size guidSize = guidSprite->getContentSize();
        float scaleX = size.width/guidSize.width;
        float scaleY = size.height/guidSize.height;
        ScaleTo *scaleTo = ScaleTo::create(1, scaleX, scaleY);
        guidSprite->runAction(EaseBackOut::create(scaleTo));
        this->addChild(guidSprite);
    }
    return true;
}

void StartLayer::initModeSprite()
{
    TempData *tmpData = GameData::getInstance()->tmpData;
    if(tmpData->startLayerMode == TempData::KModeTypeInvalid){
        log("startLayerMode invalid[%d]", tmpData->startLayerMode);
        tmpData->startLayerMode = TempData::kModeGame;
    }
    
    current = (int)tmpData->startLayerMode;//story mode
    
    inRotate = false;
    Size size = Director::getInstance()->getVisibleSize();
    
    rotateLayer = LayerRGBA::create();
    
    Point o = Point(size.width/2,size.height/2);
    float rlen = size.height*2/3;
    
    //achieve
    achieveModeSprite = Sprite::createWithSpriteFrameName("mode_one_achieve.png");
    achieveModeSprite->setPosition(o + Point(rlen*cos(180*PI/180), rlen*sin(180*PI/180)));
    if(tmpData->startLayerMode == TempData::kModeHandBook) achieveModeSprite->setScale(2);
    achieveModeSprite->setRotation(-ANG*2);
    rotateLayer->addChild(achieveModeSprite);
    
    achieveMaskSprite = Sprite::createWithSpriteFrameName("mode_one_achieve.png");
    achieveMaskSprite->setColor(Color3B::BLACK);
    achieveMaskSprite->setScale(achieveModeSprite->getScale());
    achieveMaskSprite->setPosition(achieveModeSprite->getPosition());
    achieveMaskSprite->setRotation(achieveModeSprite->getRotation());
    if(tmpData->startLayerMode == TempData::kModeHandBook) achieveMaskSprite->setOpacity(0x0);
    else achieveMaskSprite->setOpacity(0x88);
    rotateLayer->addChild(achieveMaskSprite);
    
    //setup
    setupModeSprite = Sprite::createWithSpriteFrameName("mode_one_setup.png");
    setupModeSprite->setPosition(o + Point(rlen*cos(135*PI/180), rlen*sin(135*PI/180)));
    if(tmpData->startLayerMode == TempData::kModeSetup) setupModeSprite->setScale(2);
    setupModeSprite->setRotation(-ANG);
    rotateLayer->addChild(setupModeSprite);
    
    setupMaskSprite = Sprite::createWithSpriteFrameName("mode_one_setup.png");
    setupMaskSprite->setColor(Color3B::BLACK);
    setupMaskSprite->setScale(setupModeSprite->getScale());
    setupMaskSprite->setPosition(setupModeSprite->getPosition());
    setupMaskSprite->setRotation(setupModeSprite->getRotation());
    if(tmpData->startLayerMode == TempData::kModeSetup) setupMaskSprite->setOpacity(0x0);
    else setupMaskSprite->setOpacity(0x88);
    rotateLayer->addChild(setupMaskSprite);
    
    //story
    storyModeSprite = Sprite::createWithSpriteFrameName("mode_one_story.png");
    storyModeSprite->setPosition(o + Point(rlen*cos(90*PI/180), rlen*sin(90*PI/180)));
    if(tmpData->startLayerMode == TempData::kModeGame) storyModeSprite->setScale(2);
    rotateLayer->addChild(storyModeSprite);
    
    storyMaskSprite = Sprite::createWithSpriteFrameName("mode_one_story.png");
    storyMaskSprite->setColor(Color3B::BLACK);
    storyMaskSprite->setScale(storyModeSprite->getScale());
    storyMaskSprite->setPosition(storyModeSprite->getPosition());
    storyMaskSprite->setRotation(storyModeSprite->getRotation());
    if(tmpData->startLayerMode == TempData::kModeGame) storyMaskSprite->setOpacity(0x0);
    else storyMaskSprite->setOpacity(0x88);
    rotateLayer->addChild(storyMaskSprite);
    
    //eternal
    eternalModeSprite = Sprite::createWithSpriteFrameName("mode_one_eternal.png");
    eternalModeSprite->setPosition(o + Point(rlen*cos(45*PI/180), rlen*sin(45*PI/180)));
    if(tmpData->startLayerMode == TempData::kModeEternal) eternalModeSprite->setScale(2);
    eternalModeSprite->setRotation(ANG);
    rotateLayer->addChild(eternalModeSprite);
    bool locked = GameData::getInstance()->isLargeStageLocked(5);
    if(locked){
        Sprite *lockSprite = Sprite::createWithSpriteFrameName("mode_one_lock.png");
        Size itemSize = eternalModeSprite->getContentSize();
        lockSprite->setPosition(Point(itemSize.width/2, itemSize.height/2));
        eternalModeSprite->addChild(lockSprite);
        //show star num
        unsigned int totalStarNum = 0;
        for(int i=1;i<=4;i++){
            for(int j=1;j<=15;j++){
                unsigned int num = GameData::getInstance()->getStarNum(i, j);
                if(num==0) break;
                totalStarNum+=num;
            }
        }
        char starNumText[64];
        sprintf(starNumText, "%d/%d", totalStarNum, 90);
        LabelBMFont *starNumLabel = LabelBMFont::create(starNumText, "number_dollar.fnt");
        Size sizeLock = lockSprite->getContentSize();
        lockSprite->addChild(starNumLabel);
        starNumLabel->setPosition(Point(sizeLock.width/2, 0));
    }
    
    eternalMaskSprite = Sprite::createWithSpriteFrameName("mode_one_eternal.png");
    eternalMaskSprite->setColor(Color3B::BLACK);
    eternalMaskSprite->setScale(eternalModeSprite->getScale());
    eternalMaskSprite->setPosition(eternalModeSprite->getPosition());
    eternalMaskSprite->setRotation(eternalModeSprite->getRotation());
    if(tmpData->startLayerMode == TempData::kModeEternal) eternalMaskSprite->setOpacity(0x0);
    else eternalMaskSprite->setOpacity(0x88);
    rotateLayer->addChild(eternalMaskSprite);
    
    //shop
    shopModeSprite = Sprite::createWithSpriteFrameName("mode_one_shop.png");
    shopModeSprite->setPosition(o + Point(rlen*cos(0*PI/180), rlen*sin(0*PI/180)));
    if(tmpData->startLayerMode == TempData::kModeShop) shopModeSprite->setScale(2);
    shopModeSprite->setRotation(ANG*2);
    rotateLayer->addChild(shopModeSprite);
    
    shopMaskSprite = Sprite::createWithSpriteFrameName("mode_one_shop.png");
    shopMaskSprite->setColor(Color3B::BLACK);
    shopMaskSprite->setScale(shopModeSprite->getScale());
    shopMaskSprite->setPosition(shopModeSprite->getPosition());
    shopMaskSprite->setRotation(shopModeSprite->getRotation());
    if(tmpData->startLayerMode == TempData::kModeShop) shopMaskSprite->setOpacity(0x0);
    else shopMaskSprite->setOpacity(0x88);
    rotateLayer->addChild(shopMaskSprite);
    
    rotateLayer->setPosition(Point(0,-size.height*2/3));
    rotateLayer->setRotation((2-current)*ANG);
    this->addChild(rotateLayer);
    modeSprites = Array::create(achieveModeSprite,
                                setupModeSprite,
                                storyModeSprite,
                                eternalModeSprite,
                                shopModeSprite, NULL);
    maskSprites = Array::create(achieveMaskSprite,
                                setupMaskSprite,
                                storyMaskSprite,
                                eternalMaskSprite,
                                shopMaskSprite, NULL);
    maskSprites->retain();
    modeSprites->retain();
    initModeRotateSprite();
}

void StartLayer::initModeRotateSprite()
{
    modeRotateSprites = Array::create(Sprite::createWithSpriteFrameName("rotate_achieve.png"),
                                      Sprite::createWithSpriteFrameName("rotate_setup.png"),
                                      Sprite::createWithSpriteFrameName("rotate_story.png"),
                                      Sprite::createWithSpriteFrameName("rotate_eternal.png"),
                                      Sprite::createWithSpriteFrameName("rotate_shop.png"),
                                      NULL);
    modeRotateSprites->retain();
    modeRotateLayer = Layer::create();
    modeRotateLayer->setRotation(-90-72*current);
    //modeRotateLayer->setScale(0.2);
    this->addChild(modeRotateLayer);
    
    Size size = Director::getInstance()->getVisibleSize();
    modeRotateLayer->setPosition(Point(0, -size.height-270));
    Point o = Point(size.width/2, size.height/2);
    float radius = 620;
    for(int i=0; i<5; i++){
        Sprite *sprite = (Sprite*)modeRotateSprites->objectAtIndex(i);
        sprite->setRotation(90+i*360.0/5);
        modeRotateLayer->addChild(sprite);
        sprite->setPosition(o + Point(radius*cos(-i*360.0*PI/180/5), radius*sin(-i*360.0*PI/180/5)));
    }
}

void StartLayer::buttonCallback(Object* pSender)
{
    if(isGuideShow) return;
    if(pSender == nextItemSprite){//prev mode
        if(inRotate) return;
        //log("prev");
        if(current==0) return;
        inRotate = true;
        
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        RotateBy *rotateAction = RotateBy::create(TIME, ANG);
        //RotateBy *rotateAction2 = RotateBy::create(TIME, ANG);
        //modeRotateSprite->runAction(EaseBackOut::create(rotateAction2));
        //rotateLayer->runAction(rotateAction);
        Sequence *seq = Sequence::create(EaseBackOut::create(rotateAction),
                                         CallFunc::create(CC_CALLBACK_0(StartLayer::rotateOver, this)), NULL);
        rotateLayer->runAction(seq);
        RotateBy *rotateAction2 = RotateBy::create(TIME, 360/5);
        modeRotateLayer->runAction(EaseBackOut::create(rotateAction2));
        
        ScaleBy *scaleUpAction = ScaleBy::create(TIME, 0.5);
        ScaleBy *scaleDownAction = ScaleBy::create(TIME, 2);
        ScaleBy *scaleUpAction1 = ScaleBy::create(TIME, 0.5);
        ScaleBy *scaleDownAction1 = ScaleBy::create(TIME, 2);
        FadeTo *fadeIn = FadeTo::create(TIME, 0x0);
        FadeTo *fadeOut = FadeTo::create(TIME, 0x88);
        if(current-1 >=0){
            ((Sprite*)modeSprites->objectAtIndex(current-1))->runAction(EaseBackOut::create(scaleDownAction));
            ((Sprite*)maskSprites->objectAtIndex(current-1))->runAction(EaseBackOut::create(scaleDownAction1));
            ((Sprite*)maskSprites->objectAtIndex(current-1))->runAction(fadeIn);
        }
        ((Sprite*)modeSprites->objectAtIndex(current))->runAction(EaseBackOut::create(scaleUpAction));
        ((Sprite*)maskSprites->objectAtIndex(current))->runAction(EaseBackOut::create(scaleUpAction1));
        ((Sprite*)maskSprites->objectAtIndex(current))->runAction(fadeOut);
        
        current--;
        
        SpriteFrame *frame=NULL;
        unsigned int next=current;
        if(next==0) next=4;
        switch (next) {
            case 0://bazaar
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("bg_achieve.png");
                break;
            case 1:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("bg_setup.png");
                break;
            case 2:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("bg_story.png");
                break;
            case 3:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("bg_eternal.png");
                break;
            case 4:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("bg_shop.png");
                break;
                
            default:
                break;
        }
        bg2->setDisplayFrame(frame);
        FadeOut *fadeOutBg = FadeOut::create(TIME);
        bg->runAction(fadeOutBg);
    }else if(pSender == prevItemSprite){//next mode
        if(inRotate) return;
        if(current==modeSprites->count()-1) return;
        inRotate = true;
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        //log("next");
        RotateBy *rotateAction = RotateBy::create(TIME, -ANG);
        //RotateBy *rotateAction2 = RotateBy::create(TIME, -ANG);
        //modeRotateSprite->runAction(EaseBackOut::create(rotateAction2));
        Sequence *seq = Sequence::create(EaseBackOut::create(rotateAction),
                                         CallFunc::create(CC_CALLBACK_0(StartLayer::rotateOver, this)), NULL);
        rotateLayer->runAction(seq);
        RotateBy *rotateAction2 = RotateBy::create(TIME, -360/5);
        modeRotateLayer->runAction(EaseBackOut::create(rotateAction2));
        
        ScaleBy *scaleUpAction = ScaleBy::create(TIME, 0.5);
        ScaleBy *scaleDownAction = ScaleBy::create(TIME, 2);
        ScaleBy *scaleUpAction1 = ScaleBy::create(TIME, 0.5);
        ScaleBy *scaleDownAction1 = ScaleBy::create(TIME, 2);
        FadeTo *fadeIn = FadeTo::create(TIME, 0x0);
        FadeTo *fadeOut = FadeTo::create(TIME, 0x88);
        if(current+1 <= modeSprites->count()-1){
            ((Sprite*)modeSprites->objectAtIndex(current+1))->runAction(EaseBackOut::create(scaleDownAction));
            ((Sprite*)maskSprites->objectAtIndex(current+1))->runAction(EaseBackOut::create(scaleDownAction1));
            ((Sprite*)maskSprites->objectAtIndex(current+1))->runAction(fadeIn);
        }
        ((Sprite*)modeSprites->objectAtIndex(current))->runAction(EaseBackOut::create(scaleUpAction));
        ((Sprite*)maskSprites->objectAtIndex(current))->runAction(EaseBackOut::create(scaleUpAction1));
        ((Sprite*)maskSprites->objectAtIndex(current))->runAction(fadeOut);
        current++;
        
        SpriteFrame *frame=NULL;
        unsigned int next=current;
        if(next==4) next=0;
        switch (next) {
            case 0://bazaar
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("bg_achieve.png");
                break;
            case 1:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("bg_setup.png");
                break;
            case 2:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("bg_story.png");
                break;
            case 3:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("bg_eternal.png");
                break;
            case 4:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("bg_shop.png");
                break;
                
            default:
                break;
        }
        bg2->setDisplayFrame(frame);
        FadeOut *fadeOutBg = FadeOut::create(TIME);
        bg->runAction(fadeOutBg);
    }else{
        log("guide");
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("tips.mp3");
        Scene *scene = GuidLayer::scene();
        Director::getInstance()->pushScene(scene);
    }
}

void StartLayer::rotateOver()
{
    SpriteFrame *frame=NULL;
    switch (current) {
        case 0://bazaar
            frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("bg_achieve.png");
            break;
        case 1:
            frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("bg_setup.png");
            break;
        case 2:
            frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("bg_story.png");
            break;
        case 3:
            frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("bg_eternal.png");
            break;
        case 4:
            frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("bg_shop.png");
            break;
            
        default:
            break;
    }
    bg->setDisplayFrame(frame);
    bg->setOpacity(0xff);
    inRotate = false;
}

void StartLayer::onEnterTransitionDidFinish()
{
    log("StartLayer on Enter");
    Director::getInstance()->getTouchDispatcher()->addTargetedDelegate(this, 1, true);
    Director::getInstance()->getKeypadDispatcher()->addDelegate(this);
    Layer::onEnterTransitionDidFinish();
}

void StartLayer::onExit()
{
    log("StartLayer on Exit");
    Director::getInstance()->getTouchDispatcher()->removeDelegate(this);
    Director::getInstance()->getKeypadDispatcher()->removeDelegate(this);
    Layer::onExit();
}

bool StartLayer::ccTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    if(isGuideShow) return true;
    beforePoint = Director::getInstance()->convertToGL(touch->getLocationInView());
    return true;
}

void StartLayer::ccTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
}

void StartLayer::ccTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
    if(isGuideShow){
        isGuideShow=false;
        FadeOut *fadeOut = FadeOut::create(0.5);
        ScaleTo *scale = ScaleTo::create(0.5, 0.01);
        Sequence *seq = Sequence::create(scale,
                                         CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, guidSprite, true)),
                                         NULL);
        guidSprite->runAction(fadeOut);
        guidSprite->runAction(seq);
        return;
    }
    Point location = Director::getInstance()->convertToGL(touch->getLocationInView());
    if(fabs(location.x-beforePoint.x)>30 || fabs(location.y-beforePoint.y)>30){
        if(location.x > beforePoint.x){
            buttonCallback(nextItemSprite);
        }else
            buttonCallback(prevItemSprite);
        return;
    }
    
    Size size = achieveModeSprite->getContentSize();
    Size winSize = Director::getInstance()->getVisibleSize();
    if(location.x < winSize.width/2-size.width/2 || location.x > winSize.width/2 + size.width/2
       || location.y < winSize.height/2-size.height/2 || location.y > winSize.height/2 + size.height/2){
        return;
    }
    Scene *scene;
    if(current==0){
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        GameData::getInstance()->tmpData->startLayerMode = TempData::kModeHandBook;
        scene = handBookLayer::scene();
    }else if (current == 1) {
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        GameData::getInstance()->tmpData->startLayerMode = TempData::kModeSetup;
		scene = SetupLayer::scene();
    }else if(current==2){
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        GameData::getInstance()->tmpData->startLayerMode = TempData::kModeGame;
        scene = StageSelectLayer::scene();
    }else if (current == 3) {
//        return;
        bool locked = GameData::getInstance()->isLargeStageLocked(5);
        if(locked){
            log("locked");
            if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("wrong.mp3");
            return;
        }
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        GameData::getInstance()->tmpData->startLayerMode = TempData::kModeEternal;
        GameData::getInstance()->setCurrentGame(5, 0);//recored game mode
        scene = GameLayer::scene();
	}else if (current == 4) {
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        GameData::getInstance()->tmpData->startLayerMode = TempData::kModeShop;
        scene = ShopSelectLayer::scene();
    }
	else return;
    TransitionFade *fade = TransitionFade::create(0.3, scene, Color3B::BLACK);
    Director::getInstance()->replaceScene(fade);
}

StartLayer::~StartLayer()
{
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("bg_logo-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("modebg-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("mode-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("modeRotate-hd.plist");
    modeSprites->release();
    maskSprites->release();
}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
void StartLayer::keyBackClicked(){
    log("Android- KeyBackClicked!");
    Director::getInstance()->end();
    //exit(0);
}
#endif

void StartLayer::asyncLoadAllResources()
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("mode-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("modeRotate-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("modebg-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("ingame-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("animate_time-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bg_pause-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("effect-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bad_effect-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bg_ingame_0-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("food-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bg_ingame_1-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("toy-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bg_ingame_2-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("role-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bg_ingame_3-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tool-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bg_ingame_sp-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("food-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("toy-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("role-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tool-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("guid-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("handbook-hd.plist");//for mode and rotateSprite
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("stageBack-hd.plist");//background
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("setup-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("shop-hd.plist");//for background
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("shop_mode-hd.plist");//for mode
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("stageBack-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("stageBg-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("stageLogo-hd.plist");
    
    //music
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("button.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("change.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("cleaner.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("clock.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("coin.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("correct.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("lost.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("paper.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("pause.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("plane.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("star.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("steel.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("timeup.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("tips.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("tool.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("victory.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("water1.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("water2.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("wood.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("wrong.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("zoom.mp3");
}
void StartLayer::updateLoadState()
{
    
}