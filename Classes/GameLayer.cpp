//
//  GameLayer.cpp
//  notfound
//
//  Created by 付卓 on 13-11-7.
//
//

#include "GameLayer.h"
#include "SimpleAudioEngine.h"
#include "StageSelectLayer.h"
#include "StageMenuSub.h"
#include <ctime>
#include <cstdlib>
#include "GameData.h"
#include "StartLayer.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	static int random()
	{
		return rand();
	}
#define mlog log
#else

#define mlog(msg...) do{\
    if(GAMELAYER_DEBIG) log(msg);\
    }while(0)

#endif

#define ANG0 8
#define ANG1 8
#define ANG2 7
#define ANG3 6
#define rThreshold 60
#define ROTATE_SPEED 4

#define GAMELAYER_DEBIG 0

Scene *GameLayer::scene()
{
    GameLayer *layer = GameLayer::create();
    if(layer){
        Scene *scene = Scene::create();
        scene->addChild(layer);
        return scene;
    }
    log("error create GameLaye scene failed");
    return NULL;
}

bool GameLayer::init()
{
    isGuideShow = false;
    if(GameData::getInstance()->isFirstTime("gameLayer")){
        isGuideShow = true;
    }
    mGameState = kGameRunning;
    gameOver=false;
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    if(GameData::getInstance()->iSBackGroundMusicOn())
        CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("ingame.mp3", true);

    targets = Array::create();
    TempData *tmpData = GameData::getInstance()->tmpData;
    if(tmpData->startLayerMode == TempData::kModeGame){
        if(tmpData->gameLayerType == TempData::kGameTypeBuffet) gameMode = kGameModeBuffet;
        else if(tmpData->gameLayerType == TempData::kGameTypeToyCastle) gameMode = kGameModeToyCastle;
        else if(tmpData->gameLayerType == TempData::kGameTypeTvShow) gameMode = kGameModeTvShow;
        else if(tmpData->gameLayerType == TempData::kGameTypeBazaar) gameMode = kGameModeBazaar;
    }else if(tmpData->startLayerMode == TempData::kModeEternal){
        gameMode = kGameModeEternal;
    }
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("ingame-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("animate_time-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bg_pause-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("effect-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bad_effect-hd.plist");
        ingameMaskBallSprite = Sprite::createWithSpriteFrameName("ingame_mask_ball_blue_0.png");
    if(gameMode==kGameModeBuffet){
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bg_ingame_0-hd.plist");
        bg = Sprite::createWithSpriteFrameName("bg_ingame_0.png");
        itembarSprite = Sprite::createWithSpriteFrameName("bg_ingame_itembar_0.png");
        topbarSprite = Sprite::createWithSpriteFrameName("bg_ingame_topbar_0.png");
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("food-hd.plist");
    }else if(gameMode==kGameModeToyCastle){
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bg_ingame_1-hd.plist");
        bg = Sprite::createWithSpriteFrameName("bg_ingame_1.png");
        itembarSprite = Sprite::createWithSpriteFrameName("bg_ingame_itembar_1.png");
        topbarSprite = Sprite::createWithSpriteFrameName("bg_ingame_topbar_1.png");
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("toy-hd.plist");
    }else if(gameMode==kGameModeTvShow){
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bg_ingame_2-hd.plist");
        bg = Sprite::createWithSpriteFrameName("bg_ingame_2.png");
        itembarSprite = Sprite::createWithSpriteFrameName("bg_ingame_itembar_2.png");
        topbarSprite = Sprite::createWithSpriteFrameName("bg_ingame_topbar_2.png");
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("role-hd.plist");
    }else if(gameMode==kGameModeBazaar){
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bg_ingame_3-hd.plist");
        bg = Sprite::createWithSpriteFrameName("bg_ingame_3.png");
        itembarSprite = Sprite::createWithSpriteFrameName("bg_ingame_itembar_3.png");
        topbarSprite = Sprite::createWithSpriteFrameName("bg_ingame_topbar_3.png");
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tool-hd.plist");
    }else if(gameMode==kGameModeEternal){
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bg_ingame_sp-hd.plist");
        bg = Sprite::createWithSpriteFrameName("bg_ingame_sp.png");
        itembarSprite = Sprite::createWithSpriteFrameName("bg_ingame_itembar_sp.png");
        topbarSprite = Sprite::createWithSpriteFrameName("bg_ingame_topbar_sp.png");
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("food-hd.plist");
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("toy-hd.plist");
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("role-hd.plist");
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tool-hd.plist");
    }else{
        log("ERROR, game Mode ERROR");
    }
    
	srand(time(NULL));

    Size size = Director::getInstance()->getVisibleSize();
    //log("visible size[%f,%f]", size.width, size.height);
    
    bg->setPosition(Point(size.width/2, size.height/2));
    Size bgSize = bg->getContentSize();
    float scale = (size.width/bgSize.width)>(size.height/bgSize.height)?(size.width/bgSize.width):(size.height/bgSize.height);
    log("scale = %f", scale);
    bg->setScale(scale);
    this->addChild(bg, 0);
    
    if(gameMode != kGameModeEternal) initItems();
    else initEternalItems();
    
    rotateSpriteArray0 = Array::create();
    rotateSpriteArray0->retain();
    rotateSpriteArray1 = Array::create();
    rotateSpriteArray1->retain();
    rotateSpriteArray2 = Array::create();
    rotateSpriteArray2->retain();
    rotateSpriteArray3 = Array::create();
    rotateSpriteArray3->retain();
    
    rotateSpriteNameArray0 = Array::create();
    rotateSpriteNameArray0->retain();
    rotateSpriteNameArray1 = Array::create();
    rotateSpriteNameArray1->retain();
    rotateSpriteNameArray2 = Array::create();
    rotateSpriteNameArray2->retain();
    rotateSpriteNameArray3 = Array::create();
    rotateSpriteNameArray3->retain();
    
    initRotateSprites();
    
    Size itemSize = itembarSprite->getContentSize();
    itembarSprite->setScale(2);
    itembarSprite->setPosition(Point(size.width/2, itemSize.height/1));
    itemSize = topbarSprite->getContentSize();
    topbarSprite->setScale(2);
    topbarSprite->setPosition(Point(size.width/2, size.height-itemSize.height/1));
    itemSize = ingameMaskBallSprite->getContentSize();
    ingameMaskBallSprite->setPosition(Point(size.width/2, itemSize.height/2));
    
    Sprite *ingameMaskGlassBallSprite = Sprite::createWithSpriteFrameName("ingame_mask_glassball.png");
    ingameMaskGlassBallSprite->setPosition(ingameMaskBallSprite->getPosition()-Point(0,5));
    
    Sprite *ingameMaskGlassBallSprite2 = Sprite::createWithSpriteFrameName("ingame_mask_glassball2.png");
    ingameMaskGlassBallSprite2->setOpacity(0xaa);
    ingameMaskGlassBallSprite2->setPosition(ingameMaskGlassBallSprite->getPosition());
    
    Sprite *ingameMaskGlassBallSprite3 = Sprite::createWithSpriteFrameName("ingame_mask_glassball3.png");
    ingameMaskGlassBallSprite3->setPosition(ingameMaskGlassBallSprite->getPosition());
    
    animateTimeSprite = Sprite::createWithSpriteFrameName("time_0.png");
    animateTimeSprite->setPosition(Point(ingameMaskGlassBallSprite->getPosition())-Point(0,5));
    
    this->addChild(itembarSprite, 22);
    this->addChild(topbarSprite, 22);
    this->addChild(ingameMaskGlassBallSprite3, 22);
    this->addChild(animateTimeSprite, 22);
    this->addChild(ingameMaskBallSprite, 22);
    this->addChild(ingameMaskGlassBallSprite, 22);
    this->addChild(ingameMaskGlassBallSprite2, 22);
    
    //create a animator
    Array *arrayFrames = Array::create();
    char frameName[255];
    for(int i=0; i<=8;i++){
        sprintf(frameName, "time_%d.png", i);
        SpriteFrame *frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(frameName);
        if(!frame) break;
        arrayFrames->addObject(frame);
    }
    Animation *animation = Animation::createWithSpriteFrames(arrayFrames, 1.0/8);
    Action *timeAction = RepeatForever::create(Animate::create(animation));
    animateTimeSprite->runAction(timeAction);
    
    pauseMenuItemSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ingame_btn_pause.png"),
            Sprite::createWithSpriteFrameName("ingame_btn_pause_selected.png"),
            CC_CALLBACK_1(GameLayer::buttonCallback, this));
    itemSize = pauseMenuItemSprite->getContentSize();
    pauseMenuItemSprite->setPosition(Point(size.width-itemSize.width/2-10, size.height-itemSize.height/2-10));
    
    ingameClockSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ingame_clock.png"),
            Sprite::createWithSpriteFrameName("ingame_clock_selected.png"),
            CC_CALLBACK_1(GameLayer::buttonCallback,this));
    itemSize = ingameClockSprite->getContentSize();
    ingameClockSprite->setPosition(Point(size.width/2-380, itemSize.height/2+10));
    
    ingameGlassSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ingame_glass.png"),
            Sprite::createWithSpriteFrameName("ingame_glass_selected.png"),
            CC_CALLBACK_1(GameLayer::buttonCallback, this));
    itemSize = ingameGlassSprite->getContentSize();
    ingameGlassSprite->setPosition(Point(size.width/2-250, itemSize.height/2+20));
    
    ingameRotateSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ingame_rotate.png"),
            Sprite::createWithSpriteFrameName("ingame_rotate_selected.png"),
            CC_CALLBACK_1(GameLayer::buttonCallback, this));
    itemSize = ingameRotateSprite->getContentSize();
    ingameRotateSprite->setPosition(Point(size.width/2+250, itemSize.height/2+20));
    
    ingameCleanSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ingame_clean.png"),
            Sprite::createWithSpriteFrameName("ingame_clean_selected.png"),
            CC_CALLBACK_1(GameLayer::buttonCallback, this));
    itemSize = ingameCleanSprite->getContentSize();
    ingameCleanSprite->setPosition(Point(size.width/2+380, itemSize.height/2+10));
    
    menuIngame = Menu::create(pauseMenuItemSprite,
                              ingameClockSprite,
                              ingameGlassSprite,
                              ingameRotateSprite,
                              ingameCleanSprite,
                              NULL);
    menuIngame->setPosition(Point::ZERO);
    this->addChild(menuIngame, 24);
    
    ingameGoldFrameSprite = Sprite::createWithSpriteFrameName("ingame_gold_frame.png");
    initCoinBMFont();
    itemSize = ingameGoldFrameSprite->getContentSize();
    ingameGoldFrameSprite->setPosition(Point(itemSize.width/2, size.height - itemSize.height/2));
    this->addChild(ingameGoldFrameSprite, 54);
    initTime();
    
    initPauseGUI();
    canRemoveResource = true;
    errorClickTimes = 0;
    initToolNumSprites();
    currentRunBadTool = kBadToolInvalid;
    rotateToolRunning = false;
    badToolRunning = false;
    rightTargetNum=0;
    rightTargetTypeNum=0;
    glassToolRunning = false;
    effectIndex = -1;
    fadeLayer = LayerColor::create(Color4B(0, 0, 0, 220));
    //fadeLayer->setOpacity(0xaa);
    fadeLayer->setPosition(Point::ZERO);
    fadeLayer->setVisible(false);
    this->addChild(fadeLayer, 30);
    inCleaning = false;
    blockStartClean = false;
    
    if(isGuideShow){
        log("yes first time");
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("zoom.mp3");
        isGuideShow = true;
        
        guidSprite = Sprite::createWithSpriteFrameName("guid_2.png");
        guidSprite->setPosition(Point(size.width/2, size.height/2));
        Size guidSize = guidSprite->getContentSize();
        float scaleX = size.width/guidSize.width;
        float scaleY = size.height/guidSize.height;
        ScaleTo *scaleTo = ScaleTo::create(1, scaleX, scaleY);
        guidSprite->runAction(EaseBackOut::create(scaleTo));
        this->addChild(guidSprite, 55);
    }else{
        this->schedule(schedule_selector(GameLayer::timeOut), 1.0);
    }
    showNormal = true;
    glassFindSprite=NULL;
    return true;
}

void GameLayer::initToolNumSprites()
{
    char name[5];
    unsigned int num = GameData::getInstance()->getToolNum(GameData::kToolClock);
    sprintf(name, "%u", num);
    clockNumLabel = LabelTTF::create(name, "STHeitiTC-Bold", 20);
    clockNumLabel->setPosition(ingameClockSprite->getPosition() + Point(33,-32));
    clockNumLabel->setColor(Color3B::BLACK);
    this->addChild(clockNumLabel, 24);
    
    num = GameData::getInstance()->getToolNum(GameData::kToolGlass);
    sprintf(name, "%u", num);
    glassNumLabel = LabelTTF::create(name, "STHeitiTC-Bold", 20);
    glassNumLabel->setPosition(ingameGlassSprite->getPosition() + Point(44,-55));
    glassNumLabel->setColor(Color3B::BLACK);
    this->addChild(glassNumLabel, 24);
    
    num = GameData::getInstance()->getToolNum(GameData::kToolRotate);
    sprintf(name, "%u", num);
    rotateNumLabel = LabelTTF::create(name, "STHeitiTC-Bold", 20);
    rotateNumLabel->setPosition(ingameRotateSprite->getPosition() + Point(-43,-55));
    rotateNumLabel->setColor(Color3B::BLACK);
    this->addChild(rotateNumLabel, 24);
    
    num = GameData::getInstance()->getToolNum(GameData::kToolClean);
    sprintf(name, "%u", num);
    cleanNumLabel = LabelTTF::create(name, "STHeitiTC-Bold", 20);
    cleanNumLabel->setPosition(ingameCleanSprite->getPosition() + Point(-32,-32));
    cleanNumLabel->setColor(Color3B::BLACK);
    this->addChild(cleanNumLabel, 24);
}

void GameLayer::initPauseGUI()
{
    Size size = Director::getInstance()->getWinSize();
    //bgPauseSprite = Sprite::createWithSpriteFrameName("bg_pause-hd.pvr.ccz");
    bgPauseSprite = Sprite::createWithSpriteFrameName("bg_pause.png");
    bgPauseSprite->setPosition(Point(size.width/2, size.height/2));
    bgPauseSprite->setVisible(false);
    Size bgPSize = bgPauseSprite->getContentSize();
    bgPauseSprite->setScaleX(size.width/bgPSize.width);
    bgPauseSprite->setScaleY(size.height/bgPSize.height);
    this->addChild(bgPauseSprite, 30);
    
    //Menu and buttons
    menuSelectButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ingame_btn_menu.png"),
            Sprite::createWithSpriteFrameName("ingame_btn_menu_selected.png"),
            CC_CALLBACK_1(GameLayer::buttonCallback, this));
    menuSelectButton->setPosition(Point(size.width/2 - size.width/4,250));
    
    menuAgainButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ingame_btn_again.png"),
            Sprite::createWithSpriteFrameName("ingame_btn_again_selected.png"),
            CC_CALLBACK_1(GameLayer::buttonCallback, this));
    menuAgainButton->setPosition(Point(size.width/2,250));
    
    menuResumeButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ingame_btn_resume.png"),
            Sprite::createWithSpriteFrameName("ingame_btn_resume_selected.png"),
            CC_CALLBACK_1(GameLayer::buttonCallback, this));
    menuResumeButton->setPosition(Point(size.width/2 + size.width/4,250));
    
	pauseClockSprite = Sprite::createWithSpriteFrameName("shop_goods2.png");
	pauseClockSprite->setPosition(Point(size.width/2 - size.width * 9/32, 480));
	pauseClockSpriteNum = Sprite::createWithSpriteFrameName("ingame_item_num_frame.png");
	pauseClockSpriteNum->setAnchorPoint(Point(0,1));
	pauseClockSpriteNum->setPosition((Point(pauseClockSpriteNum->getPosition()) + Point(78,25)));
	pauseClockSprite->addChild(pauseClockSpriteNum);
	
	pauseGlassSprite = Sprite::createWithSpriteFrameName("shop_goods1.png");
	pauseGlassSpriteNum = Sprite::createWithSpriteFrameName("ingame_item_num_frame.png");
	pauseGlassSpriteNum->setAnchorPoint(Point(0,1));
	pauseGlassSpriteNum->setPosition((Point(pauseGlassSpriteNum->getPosition()) + Point(85,25)));

	pauseGlassSprite->setPosition(Point(size.width/2 - size.width * 3/32,480));
	pauseGlassSprite->addChild(pauseGlassSpriteNum);

	pauseRotateSprite = Sprite::createWithSpriteFrameName("shop_goods4.png");
	pauseRotateSprite->setScale(0.625f);
	pauseRotateSprite->setPosition(Point(size.width/2 + size.width * 3/32 ,480));
	pauseRotateSpriteNum = Sprite::createWithSpriteFrameName("ingame_item_num_frame.png");
	pauseRotateSpriteNum->setScale(1.6f);
	pauseRotateSpriteNum->setAnchorPoint(Point(0,1));
	pauseRotateSpriteNum->setPosition((Point(pauseRotateSpriteNum->getPosition()) + Point(99,35)));
	pauseRotateSprite->addChild(pauseRotateSpriteNum);

	pauseCleanSprite = Sprite::createWithSpriteFrameName("shop_goods3.png");
	pauseCleanSprite->setPosition(Point(size.width/2 + size.width * 9/32,480));
	pauseCleanSpriteNum = Sprite::createWithSpriteFrameName("ingame_item_num_frame.png");
	pauseCleanSpriteNum->setAnchorPoint(Point(0,1));
	pauseCleanSpriteNum->setPosition((Point(pauseCleanSpriteNum->getPosition()) + Point(85,25)));

	pauseCleanSprite->addChild(pauseCleanSpriteNum);
	
	buyClock = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ingame_btn_buy.png"),
            Sprite::createWithSpriteFrameName("ingame_btn_buy_selected.png"),
            CC_CALLBACK_1(GameLayer::buttonCallback, this));
	buyClock->setPosition(Point(size.width/2 - size.width * 9/32, 380));
	buyClock->setScale(0.75f);

	buyGlass = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ingame_btn_buy.png"),
            Sprite::createWithSpriteFrameName("ingame_btn_buy_selected.png"),
            CC_CALLBACK_1(GameLayer::buttonCallback, this));
	buyGlass->setPosition(Point(size.width/2 - size.width * 3/32,380));
	buyGlass->setScale(0.75f);

	buyRotate = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ingame_btn_buy.png"),
            Sprite::createWithSpriteFrameName("ingame_btn_buy_selected.png"),
            CC_CALLBACK_1(GameLayer::buttonCallback, this));
	buyRotate->setPosition(Point(size.width/2 + size.width * 3/32 ,380));
	buyRotate->setScale(0.75f);

	buyClean = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ingame_btn_buy.png"),
            Sprite::createWithSpriteFrameName("ingame_btn_buy_selected.png"),
            CC_CALLBACK_1(GameLayer::buttonCallback, this));
	buyClean->setScale(0.75f);
	buyClean->setPosition(Point(size.width/2 + size.width * 9/32,380));
	
	menuPause = Menu::create(menuSelectButton,
                             menuAgainButton,
                             menuResumeButton,
							 buyGlass,
							 buyRotate,
							 buyClean,
							 buyClock,
                              NULL);
    menuPause->setPosition(Point::ZERO);
    this->addChild(menuPause, 31);
    menuPause->setVisible(false);
    menuPause->setEnabled(false);
	this->addChild(pauseCleanSprite, 51);
	this->addChild(pauseRotateSprite, 51);
	this->addChild(pauseClockSprite, 51);
	this->addChild(pauseGlassSprite, 51);
	pauseCleanSprite->setVisible(false);
	pauseClockSprite->setVisible(false);
	pauseGlassSprite->setVisible(false);
	pauseRotateSprite->setVisible(false);

    //initial the number of tools
	char name[5];
    unsigned int num = GameData::getInstance()->getToolNum(GameData::kToolClock);
    sprintf(name, "%u", num);
    pauseClockNumLabel = LabelTTF::create(name, "STHeitiTC-Bold", 20);
    pauseClockNumLabel->setPosition(pauseClockNumLabel->getPosition() + Point(104,0));
	pauseClockNumLabel->setColor(Color3B::RED);
    pauseClockSprite->addChild(pauseClockNumLabel);
    
    num = GameData::getInstance()->getToolNum(GameData::kToolGlass);
    sprintf(name, "%u", num);
    pauseGlassNumLabel = LabelTTF::create(name, "STHeitiTC-Bold", 20);
    pauseGlassNumLabel->setPosition(pauseGlassNumLabel->getPosition() + Point(111,-0));
    pauseGlassNumLabel->setColor(Color3B::RED);
	pauseGlassSprite->addChild(pauseGlassNumLabel);
    
    num = GameData::getInstance()->getToolNum(GameData::kToolRotate);
    sprintf(name, "%u", num);
    pauseRotateNumLabel = LabelTTF::create(name, "STHeitiTC-Bold", 20);
    pauseRotateNumLabel->setPosition(pauseRotateNumLabel->getPosition() + Point(140,-4));
    pauseRotateNumLabel->setColor(Color3B::RED);
	pauseRotateNumLabel->setScale(1.6);
	pauseRotateSprite->addChild(pauseRotateNumLabel);
    
    num = GameData::getInstance()->getToolNum(GameData::kToolClean);
    sprintf(name, "%u", num);
    pauseCleanNumLabel = LabelTTF::create(name, "STHeitiTC-Bold", 20);
    pauseCleanNumLabel->setPosition(pauseCleanNumLabel->getPosition() + Point(111,0));
    pauseCleanNumLabel->setColor(Color3B::RED);
	pauseCleanSprite->addChild(pauseCleanNumLabel);
    
    unsigned int price = 0;
    price = GameData::getInstance()->getToolPrice(2);
    sprintf(name, "%u", price);
	buyClockNumLabel = LabelTTF::create(name, "STHeitiTC-Bold", 40);
    buyClockNumLabel->setPosition(pauseClockNumLabel->getPosition() + Point(-25,60));
	buyClockNumLabel->setColor(Color3B::RED);
	buyClock->addChild(buyClockNumLabel);
   
    price = GameData::getInstance()->getToolPrice(1);
    sprintf(name, "%u", price);
    buyGlassNumLabel = LabelTTF::create(name, "STHeitiTC-Bold", 40);
    buyGlassNumLabel->setPosition(pauseGlassNumLabel->getPosition() + Point(-30,60));
    buyGlassNumLabel->setColor(Color3B::RED);
	buyGlass->addChild(buyGlassNumLabel);
    
    price = GameData::getInstance()->getToolPrice(4);
    sprintf(name, "%u", price);
    buyRotateNumLabel = LabelTTF::create(name, "STHeitiTC-Bold", 40);
    buyRotateNumLabel->setPosition(pauseRotateNumLabel->getPosition() + Point(-60,68));
    buyRotateNumLabel->setColor(Color3B::RED);
	buyRotate->addChild(buyRotateNumLabel);
    
    price = GameData::getInstance()->getToolPrice(3);
    sprintf(name, "%u", price);
    buyCleanNumLabel = LabelTTF::create(name, "STHeitiTC-Bold", 40);
    buyCleanNumLabel->setPosition(pauseCleanNumLabel->getPosition() + Point(-30,60));
    buyCleanNumLabel->setColor(Color3B::RED);
	buyClean->addChild(buyCleanNumLabel);
    
    dialogSprite = Sprite::createWithSpriteFrameName("shop_coin_alert.png");
    dialogSprite->setPosition(Point(size.width/2, size.height/2));
    this->addChild(dialogSprite, 52);
    dialogSprite->setVisible(false);
    isShowDialog = false;
    
    //add Show current stage %d - %d
    char stageInfo[255];
    sprintf(stageInfo, "%d - %d", GameData::getInstance()->currentStage, GameData::getInstance()->currentSubStage);
    stageSprite = Sprite::createWithSpriteFrameName("font_stage.png");
    stageInfoLabel = LabelBMFont::create(stageInfo, "number_stage.fnt");
    stageSprite->setPosition(Point(size.width/2 - 120, size.height/2 + 234));
    stageInfoLabel->setPosition(Point(size.width/2 + 60, size.height/2 + 276));
    this->addChild(stageSprite, 51);
    this->addChild(stageInfoLabel, 51);
    stageSprite->setVisible(false);
    stageInfoLabel->setVisible(false);
}

void GameLayer::resetGameLayer()
{//reset time and every thing
    return;
    initRotateSprites();
    timeValue = timeTotal;
}

/*
 *  init all items and targets, box tools
 */
void GameLayer::initItems()
{
    log("start init Items");
    
    log("get targets begin");
    //for targets
    int num;
    int *target;
    GameData::getInstance()->getTarget(&num, &target);//get target from GameData
    totalTargetsNum = num;//for game win conditions
    curTargetsNum = num;
    log("get targets OK, num = %d", num);
    
    //get current target with name and num
    currentTargets=Array::create();
    currentTargets->retain();
    currentTargetNum = Array::create();
    currentTargetNum->retain();
    currentTargets->removeAllObjects();
    currentTargetNum->removeAllObjects();
    
    //the target is fill rotateSpriteArray
    rotateSpriteArray = Array::create();
    rotateSpriteArray->retain();
    for(int i=0; i<num; i++){
        char name[255];
        sprintf(name, "%d.png", target[i]);
        targets->addObject(String::create(name));
        rotateSpriteArray->addObject(String::create(name));//add all targets to rotateSpriteArray
    }
    
    for(int i=0; i<targets->count(); i++)
    {//update targets num by types
        String *strT = (String*)targets->objectAtIndex(i);
        bool find = false;
        int j=0;
        for(j=0; j<currentTargets->count(); j++){
            String *str = (String*)currentTargets->objectAtIndex(j);
            if(strcmp(str->getCString(), strT->getCString()) == 0){
                find = true;
                break;
            }
        }
        
        if(!find){//add one with num 1
            currentTargets->addObject(String::createWithFormat("%s", strT->getCString()));
            //currentTargetNum.push_back(1);
            currentTargetNum->addObject(Integer::create(1));
        }else{//add one num
            //log("add one num!!");
            Integer* value = (Integer*)currentTargetNum->objectAtIndex(j);
            currentTargetNum->removeObjectAtIndex(j);
            int data = value->getValue();
            data++;
            currentTargetNum->insertObject(Integer::create(data), j);
        }
    }
    
    Array *array = Array::createWithCapacity(5*55);
    array->retain();
    unsigned int stage = GameData::getInstance()->currentStage;
    unsigned int subStage = GameData::getInstance()->currentSubStage;
    char name[255];
    unsigned int totalItemType=0;
    for(int i=0; i<55; i++){//1.2.3...50
        sprintf(name, "%d0%02d.png", stage, i+(((int)subStage+2)/3-1)*5);//1:40xx 2:50xx 3:60xx 4:70xx
        bool find = false;
        for(int j=0; j<currentTargets->count(); j++){
            String *str = (String*)currentTargets->objectAtIndex(j);
            if(strcmp(str->getCString(), name)==0){
                find = true;
                break;
            }
        }
        if(find) continue;
        totalItemType++;
        array->addObject(String::create(name));
        array->addObject(String::create(name));
        array->addObject(String::create(name));
        array->addObject(String::create(name));
        array->addObject(String::create(name));//5 object one time
    }//totoal 5*totalItemType items
    CCASSERT(array->count()==totalItemType*5, "ERROR array num is not OK");
    //random get 200 Items origin items
    unsigned int count = 5*totalItemType;
    for(int i=0; i<200-num; i++){//only need 200 of them
        unsigned int j;
        j = rand()%count;//0~count-1
        count--;
        rotateSpriteArray->addObject(array->objectAtIndex(j));
        array->removeObjectAtIndex(j);
    }//set everything to rotateSpriteaArray for 200 items
    CCASSERT(rotateSpriteArray->count()==200, "ERROR, item num not OK");
    array->release();
    log("random get OK");
    
    //replace 3 nice boxes and 1~3 tools or coins but cant be target
    int numTools = rand()%3+1;//1~3
    log("+++++add [%d]tools", numTools);
    for(int i=0; i<3+numTools; i++){
        while(true){//random get a target
            unsigned int j=rand()%(rotateSpriteArray->count());
            String* str = (String*)rotateSpriteArray->objectAtIndex(j);
            bool find = false;
            for(unsigned int m=0; m<num; m++){//can't be a target
                String *str2 = String::createWithFormat("%d.png", target[m]);
                //String *str2 = (String*)array->objectAtIndex(m);
                if(strcmp(str->getCString(), str2->getCString())==0){
                    find = true;
                    break;//find one
                }
            }
            if(find) continue;//is one of target skip it
            else{//replace one with nice box or tools or coins
                rotateSpriteArray->removeObjectAtIndex(j);
                log("replace one nicebox or tools at[%d]", j);
                if(i<=2) rotateSpriteArray->insertObject(String::createWithFormat("%s", "10006.png"), j);//3 box
                else{
                    int r = rand()%5;//0~4
                    if(r==0) rotateSpriteArray->insertObject(String::createWithFormat("%s", "10000.png"), j);//clean
                    else if(r==1) rotateSpriteArray->insertObject(String::createWithFormat("%s", "10001.png"), j);//glass
                    else if(r==2) rotateSpriteArray->insertObject(String::createWithFormat("%s", "10002.png"), j);//coin
                    else if(r==3) rotateSpriteArray->insertObject(String::createWithFormat("%s", "10004.png"), j);//rotate
                    else if(r==4) rotateSpriteArray->insertObject(String::createWithFormat("%s", "10005.png"), j);//clock
                    else CCASSERT(false, "shouldn't go here");
                }
                break;
            }
        }
    }
    delete target;
    log("random add nice box ok");
    
    //show targets
    //for targets
    target0 = Sprite::create();
    target1 = Sprite::create();
    target2 = Sprite::create();
    targetNum0 = LabelTTF::create("", "STHeitiTC-Bold", 30);
    targetNum1 = LabelTTF::create("", "STHeitiTC-Bold", 30);
    targetNum2 = LabelTTF::create("", "STHeitiTC-Bold", 30);
    target0->setScale(0.5);
    target1->setScale(0.5);
    target2->setScale(0.5);
    this->addChild(target0, 24);
    this->addChild(target1, 24);
    this->addChild(target2, 24);
    this->addChild(targetNum0, 24);
    this->addChild(targetNum1, 24);
    this->addChild(targetNum2, 24);
    
    printAllTarget();
    updateTargets();
    
    //check target and nums
    log("------------check------------");
    for(int i=0; i<currentTargets->count(); i++){
        String *str = (String*)currentTargets->objectAtIndex(i);
        int num2 = ((Integer*)currentTargetNum->objectAtIndex(i))->getValue();
        int count=0;
        for(int j=0; j<rotateSpriteArray->count(); j++){
            String *str2 = (String*)rotateSpriteArray->objectAtIndex(j);
            if(strcmp(str->getCString(), str2->getCString())==0){
                count++;
                log("target [%s] @[%d]", str->getCString(), j);
            }
        }
        log("check target[%s]-[%d==%d]", str->getCString(), num2, count);
        CCASSERT(num2==count, "target num not right");
    }
    log("------------check---passed------");
}

void GameLayer::playPolluteEffect()
{
    if(GameData::getInstance()->isEffectOn()){
        effectIndex = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("plane.mp3");
    }
}

void GameLayer::initEternalItems()
{
    log("~~~~~~~~~~~start init Eternal Items~~~~~~~~~~");
    //get all possible items
    int _stage=-1, _substage=-1;
    bool ok = false;
    for(int i=1;i<=4;i++){
        if(GameData::getInstance()->isStageLocked(i, 1)){
            _stage = i-1;
            ok = true;
            break;
        }
    }
    if(!ok) _stage = 4;
    ok = false;
    for(int i=1;i<=15;i++){
        if(GameData::getInstance()->isStageLocked(_stage, i)){
            _substage = i-1;
            ok = true;
            break;
        }
    }
    if(!ok) _substage = 15;
    log("current unlocked[%d-%d]", _stage, _substage);
    int itemTotalNum = (_stage-1)*75 + ((((int)(_substage+2)/3)-1)*5 + 55);
    
    Array *array = Array::createWithCapacity(5*itemTotalNum);
    array->retain();
    char name[255];
    for(int i=1; i<_stage; i++){//75 items
        for(int j=0; j<75; j++){
            sprintf(name, "%d0%02d.png", i,j);
            array->addObject(String::create(name));
            array->addObject(String::create(name));
            array->addObject(String::create(name));
            array->addObject(String::create(name));
            array->addObject(String::create(name));//5 object one time
        }
    }
    log("itemTotalNum=%d*5=%d, array->count=%d", itemTotalNum, itemTotalNum*5, array->count());
    int numSub = ((((int)(_substage+2)/3)-1)*5 + 55);
    log("numSub = %d, _stage = %d, _substage = %d", numSub, _stage, _substage);
    for(int j=0; j<numSub; j++){//last items
        sprintf(name, "%d0%02d.png", _stage,j);
        array->addObject(String::create(name));
        array->addObject(String::create(name));
        array->addObject(String::create(name));
        array->addObject(String::create(name));
        array->addObject(String::create(name));//5 object one time
    }
    log("itemTotalNum=%d*5=%d, array->count=%d", itemTotalNum, itemTotalNum*5, array->count());
    CCASSERT(itemTotalNum*5 == array->count(), "Item not OK");
    //rande get items from array to rotateSpriteArray
    rotateSpriteArray = Array::create();
    rotateSpriteArray->retain();
    
    //for targets
    int num;
    int *target;
    GameData::getInstance()->getEternalTarget(&num, &target);//get target from GameData
    totalTargetsNum = num;//for game win conditions
    curTargetsNum = num;
    /*
    for(int i=0; i<num; i++){
        log("get targets[%d]", target[i]);
    }
    */
    //random get 200 Items from all origin items but can't be one of target
    unsigned int count = 5*itemTotalNum;
    for(int i=0; i<200; i++){//only need 200 of them
        unsigned int j;
        while(true){//random get a item but not a target
            j = rand()%count;//0~count-1
            String *str2 = (String*)array->objectAtIndex(j);
            bool find = false;
            for(int i=0; i<num; i++){//can't be a target
                String *str = String::createWithFormat("%d.png", target[i]);
                if(strcmp(str->getCString(), str2->getCString())==0){
                    find = true;
                    break;
                }
            }
            if(!find) break;
        }
        count--;
        rotateSpriteArray->addObject(array->objectAtIndex(j));
        array->removeObjectAtIndex(j);
    }//set everything to rotateSpriteaArray for 200 items
    array->release();
    
    //replace some items by targets
    for(int i=0; i<num; i++){
        String *str = String::createWithFormat("%d.png", target[i]);//need add
        while(true){//repalce one once
            unsigned int j=rand()%200;//rande fill targets
            String *str2 = (String*)rotateSpriteArray->objectAtIndex(j);
            bool find = false;
            for(unsigned int m=0; m<num; m++){//can't be a target
                String *str3 = String::createWithFormat("%d.png", target[m]);
                if(strcmp(str2->getCString(), str3->getCString())==0){
                    find = true;
                    break;
                }
            }
            if(find) continue;
            log("j=%d,\tupdate target[%s]", j, str->getCString());
            rotateSpriteArray->removeObjectAtIndex(j);//rm one
            rotateSpriteArray->insertObject(str, j);//update one
            targets->addObject(String::createWithFormat("%d.png", target[i]));
            break;
        }
    }
    
    //replace 3 nice boxes and 1~3 tools or coins but cant be target
    for(int i=0; i<3; i++){
        while(true){//random get a place
            unsigned int j=rand()%(rotateSpriteArray->count());
            String* str = (String*)rotateSpriteArray->objectAtIndex(j);
            bool find = false;
            for(unsigned int m=0; m<num; m++){//can't be a target
                String *str2 = String::createWithFormat("%d.png", target[m]);
                //String *str2 = (String*)array->objectAtIndex(m);
                if(strcmp(str->getCString(), str2->getCString())==0){
                    find = true;
                    break;//find one
                }
            }
            if(find) continue;//is one of target skip it
            else{//replace one with nice box or tools or coins
                rotateSpriteArray->removeObjectAtIndex(j);
                log("replace one nicebox or tools at[%d]", j);
                rotateSpriteArray->insertObject(String::createWithFormat("%s", "10006.png"), j);//3 box
                break;
            }
        }
    }
    delete target;
    /*
    log("*******************************************");
    for(int i=0; i<200; i++){
        String *str = (String*)rotateSpriteArray->objectAtIndex(i);
        log("rotate Sprite debug [%s]@[%d]", str->getCString(), i);
    }//for now all 200 item are OK
    log("*******************************************");
     */
    
    //show targets
    //for targets
    target0 = Sprite::create();
    target1 = Sprite::create();
    target2 = Sprite::create();
    targetNum0 = LabelTTF::create("", "STHeitiTC-Bold", 30);
    targetNum1 = LabelTTF::create("", "STHeitiTC-Bold", 30);
    targetNum2 = LabelTTF::create("", "STHeitiTC-Bold", 30);
    target0->setScale(0.5);
    target1->setScale(0.5);
    target2->setScale(0.5);
    this->addChild(target0, 24);
    this->addChild(target1, 24);
    this->addChild(target2, 24);
    this->addChild(targetNum0, 24);
    this->addChild(targetNum1, 24);
    this->addChild(targetNum2, 24);
    
    //get current target with name and num
    currentTargets=Array::create();
    currentTargets->retain();
    currentTargetNum = Array::create();
    currentTargetNum->retain();
    currentTargets->removeAllObjects();
    currentTargetNum->removeAllObjects();
    //currentTargetNum.clear();
    for(int i=0; i<targets->count(); i++)
    {//update targets num by types
        String *strT = (String*)targets->objectAtIndex(i);
        bool find = false;
        int j=0;
        for(j=0; j<currentTargets->count(); j++){
            String *str = (String*)currentTargets->objectAtIndex(j);
            if(strcmp(str->getCString(), strT->getCString()) == 0){
                find = true;
                break;
            }
        }
        
        if(!find){//add one with num 1
            currentTargets->addObject(String::createWithFormat("%s", strT->getCString()));
            //currentTargetNum.push_back(1);
            currentTargetNum->addObject(Integer::create(1));
        }else{//add one num
            //log("add one num!!");
            Integer* value = (Integer*)currentTargetNum->objectAtIndex(j);
            currentTargetNum->removeObjectAtIndex(j);
            int data = value->getValue();
            data++;
            currentTargetNum->insertObject(Integer::create(data), j);
        }
    }
    printAllTarget();
    updateTargets();
    
    //check target and nums
    log("------------check------------");
    for(int i=0; i<currentTargets->count(); i++){
        String *str = (String*)currentTargets->objectAtIndex(i);
        int num2 = ((Integer*)currentTargetNum->objectAtIndex(i))->getValue();
        int count=0;
        for(int j=0; j<rotateSpriteArray->count(); j++){
            String *str2 = (String*)rotateSpriteArray->objectAtIndex(j);
            if(strcmp(str->getCString(), str2->getCString())==0){
                count++;
                log("target [%s] @[%d]", str->getCString(), j);
            }
        }
        log("check target[%s]-[%d==%d]", str->getCString(), num2, count);
        CCASSERT(num2==count, "target num not right");
    }
    log("------------check---passed------");
}

std::string GameLayer::getOneEternalItem()
{
    log("--------------start getOneEternalItem----------");
    char name[1024];
    unsigned int count = rand()%4+1;//1~4 targets
//    unsigned int count = 2;//1~4 targets
    while(true){//random generator a target but can't already existed
        unsigned int itemName = GameData::getInstance()->getEternalItem();
        sprintf(name, "%d.png", itemName);
        bool find = false;
        CCASSERT(currentTargets->count()==2, "targets num not ok");
        for (int i=0; i<currentTargets->count(); i++) {
            String *str = (String*)currentTargets->objectAtIndex(i);
            if(strcmp(str->getCString(), name)==0){
                find = true;
                break;
            }
        }
        if(find) continue;
        break;
    }
    log("update One External Item[%s]*%d", name, count);
    currentTargets->addObject(String::create(string(name)));
    currentTargetNum->addObject(Integer::create(count));
    curTargetsNum+=count;
    {
        int num[3]={0};
        for(int line=0; line<4; line++){
            Array *arrName=NULL;
            Array *arrSprite=NULL;
            if(line==0){arrName = rotateSpriteNameArray0;arrSprite = rotateSpriteArray0;}
            else if(line==1){arrName = rotateSpriteNameArray1;arrSprite = rotateSpriteArray1;}
            else if(line==2){arrName = rotateSpriteNameArray2;arrSprite = rotateSpriteArray2;}
            else if(line==3){arrName = rotateSpriteNameArray3;arrSprite = rotateSpriteArray3;}
            for(int index=0; index<arrName->count(); index++){
                String *str = (String*)arrName->objectAtIndex(index);
                String *str2 = (String*)currentTargets->objectAtIndex(0);
                if(strcmp(str->getCString(), str2->getCString())==0) num[0]++;
                str2 = (String*)currentTargets->objectAtIndex(1);
                if(strcmp(str->getCString(), str2->getCString())==0) num[1]++;
                str2 = (String*)currentTargets->objectAtIndex(2);
                if(strcmp(str->getCString(), str2->getCString())==0) num[2]++;
            }
        }
        log("--------check item num1------------");
        int num0 = ((Integer*)currentTargetNum->objectAtIndex(0))->getValue();
        int num1 = ((Integer*)currentTargetNum->objectAtIndex(1))->getValue();
        int num2 = ((Integer*)currentTargetNum->objectAtIndex(2))->getValue();
        String *str0 = (String*)currentTargets->objectAtIndex(0);
        String *str1 = (String*)currentTargets->objectAtIndex(1);
        String *str2 = (String*)currentTargets->objectAtIndex(2);
        log("[0:%s][1:%s][2:%s]", str0->getCString(), str1->getCString(), str2->getCString());
        log("num[0:%d][1:%d][2:%d]", num[0],num[1],num[2]);
        log("num[0:%d][1:%d][2:%d]", num0,num1,num2);
        //CCASSERT((num[0]==num0 && num1==num[1]), "target num not ok");//target[2]==0
    }
    
    for(int line=0; line<4; line++){//clear targets on the map
        Array *arrName=NULL;
        Array *arrSprite=NULL;
        if(line==0){arrName = rotateSpriteNameArray0;arrSprite = rotateSpriteArray0;}
        else if(line==1){arrName = rotateSpriteNameArray1;arrSprite = rotateSpriteArray1;}
        else if(line==2){arrName = rotateSpriteNameArray2;arrSprite = rotateSpriteArray2;}
        else if(line==3){arrName = rotateSpriteNameArray3;arrSprite = rotateSpriteArray3;}
        for(int index=0; index<arrName->count(); index++){
            String *str = (String*)arrName->objectAtIndex(index);
            if(strcmp(str->getCString(), name) != 0) continue;//needn't replace
            //need replace the random generate target with other item but not a already existed item
            char replaceName[1024];
            while(true){//using a none target item replace it
                unsigned int itemName = GameData::getInstance()->getEternalItem();
                sprintf(replaceName, "%d.png", itemName);
                bool find = false;
                for (int i=0; i<currentTargets->count(); i++) {//must 3 type of targets
                    String *str2 = (String*)currentTargets->objectAtIndex(i);
                    if(strcmp(str2->getCString(), replaceName)==0){
                        find = true;
                        break;
                    }
                }
                if(find) continue;
                break;
            }
            //replace by a normal item(not a target)
            arrName->removeObjectAtIndex(index);
            arrName->insertObject(String::createWithFormat("%s", replaceName), index);
            SpriteFrame *frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(replaceName);
            Sprite *sprite = (Sprite*)arrSprite->objectAtIndex(index);
            sprite->setDisplayFrame(frame);
        }
    }
    {
        int num[3]={0};
        for(int line=0; line<4; line++){
            Array *arrName=NULL;
            Array *arrSprite=NULL;
            if(line==0){arrName = rotateSpriteNameArray0;arrSprite = rotateSpriteArray0;}
            else if(line==1){arrName = rotateSpriteNameArray1;arrSprite = rotateSpriteArray1;}
            else if(line==2){arrName = rotateSpriteNameArray2;arrSprite = rotateSpriteArray2;}
            else if(line==3){arrName = rotateSpriteNameArray3;arrSprite = rotateSpriteArray3;}
            for(int index=0; index<arrName->count(); index++){
                String *str = (String*)arrName->objectAtIndex(index);
                String *str2 = (String*)currentTargets->objectAtIndex(0);
                if(strcmp(str->getCString(), str2->getCString())==0) num[0]++;
                str2 = (String*)currentTargets->objectAtIndex(1);
                if(strcmp(str->getCString(), str2->getCString())==0) num[1]++;
                str2 = (String*)currentTargets->objectAtIndex(2);
                if(strcmp(str->getCString(), str2->getCString())==0) num[2]++;
            }
        }
        log("--------check item num------------");
        int num0 = ((Integer*)currentTargetNum->objectAtIndex(0))->getValue();
        int num1 = ((Integer*)currentTargetNum->objectAtIndex(1))->getValue();
        int num2 = ((Integer*)currentTargetNum->objectAtIndex(2))->getValue();
        String *str0 = (String*)currentTargets->objectAtIndex(0);
        String *str1 = (String*)currentTargets->objectAtIndex(1);
        String *str2 = (String*)currentTargets->objectAtIndex(2);
        log("[0:%s][1:%s][2:%s]", str0->getCString(), str1->getCString(), str2->getCString());
        log("num[0:%d][1:%d][2:%d]", num[0],num[1],num[2]);
        log("num[0:%d][1:%d][2:%d]", num0,num1,num2);
        CCASSERT((num[0]==num0 && num1==num[1] && num[2]==0), "target num not ok");//target[2]==0
    }
    
    //update targets on rotateSpriteaArray*
    for(int i=0; i<count; i++){
        while(true){
            unsigned int line = rand()%4;
            Array *arrName=NULL;
            Array *arrSprite=NULL;
            if(line==0){arrName = rotateSpriteNameArray0;arrSprite = rotateSpriteArray0;}
            else if(line==1){arrName = rotateSpriteNameArray1;arrSprite = rotateSpriteArray1;}
            else if(line==2){arrName = rotateSpriteNameArray2;arrSprite = rotateSpriteArray2;}
            else if(line==3){arrName = rotateSpriteNameArray3;arrSprite = rotateSpriteArray3;}
            unsigned int index = rand()%arrName->count();//random index
            Sprite *sprite1 = (Sprite*)arrSprite->objectAtIndex(index);
            if(sprite1 == currentClickedSprite){
                log("can't update on current location");
                continue;
            }
            
            String *str2 = (String*)arrName->objectAtIndex(index);
            bool find=false;
            for (int j=0; j<currentTargets->count(); j++) {
                String *str = (String*)currentTargets->objectAtIndex(j);
                if(strcmp(str->getCString(), str2->getCString())==0){
                    find = true;
                    break;
                }
                /*
                std::string namestr = string(str->getCString());
                namestr = namestr.substr(0, namestr.length()-4);
                if(atoi(namestr.c_str())>4074){//not a normal item
                    find = true;
                    break;
                }*/
            }
            if(find) continue;
            arrName->removeObjectAtIndex(index);
            arrName->insertObject(String::createWithFormat("%s", name), index);
            SpriteFrame *frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(name);
            Sprite *sprite = (Sprite*)arrSprite->objectAtIndex(index);
            sprite->setDisplayFrame(frame);
            log("rand replace Sprite[%s]", name);
            break;
        }
    }
    //update at rotateSprite Array
    //check here
    checkTargetNum();
    return std::string(name);
}

void GameLayer::printAllTarget()
{
    log("------------------------------------");
    //print all targets
    for(int i=0; i<currentTargets->count(); i++){
        String *str = (String*)currentTargets->objectAtIndex(i);
        log("Target:[%d] [%d]-[%s]", i, ((Integer*)currentTargetNum->objectAtIndex(i))->getValue(), str->getCString());
    }
    log("------------------------------------");
}

void GameLayer::checkTargetNum()
{
    int num[3]={0};
    for(int line=0; line<4; line++){
        Array *arrName=NULL;
        Array *arrSprite=NULL;
        if(line==0){arrName = rotateSpriteNameArray0;arrSprite = rotateSpriteArray0;}
        else if(line==1){arrName = rotateSpriteNameArray1;arrSprite = rotateSpriteArray1;}
        else if(line==2){arrName = rotateSpriteNameArray2;arrSprite = rotateSpriteArray2;}
        else if(line==3){arrName = rotateSpriteNameArray3;arrSprite = rotateSpriteArray3;}
        for(int index=0; index<arrName->count(); index++){
            String *str = (String*)arrName->objectAtIndex(index);
            String *str2 = (String*)currentTargets->objectAtIndex(0);
            if(strcmp(str->getCString(), str2->getCString())==0) num[0]++;
            str2 = (String*)currentTargets->objectAtIndex(1);
            if(strcmp(str->getCString(), str2->getCString())==0) num[1]++;
            str2 = (String*)currentTargets->objectAtIndex(2);
            if(strcmp(str->getCString(), str2->getCString())==0) num[2]++;
        }
    }
    log("--------check item num------------");
    int num0 = ((Integer*)currentTargetNum->objectAtIndex(0))->getValue();
    int num1 = ((Integer*)currentTargetNum->objectAtIndex(1))->getValue();
    int num2 = ((Integer*)currentTargetNum->objectAtIndex(2))->getValue();
    log("num[0:%d][1:%d][2:%d]", num[0],num[1],num[2]);
    log("num[0:%d][1:%d][2:%d]", num0,num1,num2);
    CCASSERT((num[0]==num0 && num1==num[1] && num2==num[2]), "target num not ok");
}

void GameLayer::updateTargets()
{//update targets on top right of the screen
    //return;
    SpriteFrame *frame0=NULL;
    SpriteFrame *frame1=NULL;
    SpriteFrame *frame2=NULL;
    
    Size s = topbarSprite->getContentSize();
    Size winS = Director::getInstance()->getWinSize();
    Point pos0 = Point(winS.width/2+10, winS.height-42);
    Point pos1 = Point(winS.width/2+120, winS.height-47);
    Point pos2 = Point(winS.width/2+230, winS.height-58);
    Point offset = Point(45, 0);
    Point offset1 = Point(45, 0);
    Point offset2 = Point(45, -5);
    
    target0->setVisible(false);
    target1->setVisible(false);
    target2->setVisible(false);
    targetNum0->setVisible(false);
    targetNum1->setVisible(false);
    targetNum2->setVisible(false);
    
    log("update Targets, total = %d", currentTargets->count());
    
    if(currentTargets->count() == 1){
        String *str = (String*)currentTargets->objectAtIndex(0);
        frame0 = SpriteFrameCache::getInstance()->getSpriteFrameByName(str->getCString());
        target0->setDisplayFrame(frame0);
        target0->setPosition(pos0);
        target0->setVisible(true);
        targetNum0->setString(String::createWithFormat("x %d", ((Integer*)currentTargetNum->objectAtIndex(0))->getValue())->getCString());
        targetNum0->setVisible(true);
        targetNum0->setPosition(pos0 + offset);
        //log("set target0-[%s]", str->getCString());
        
    }else if(currentTargets->count() == 2){
        String *str = (String*)currentTargets->objectAtIndex(0);
        frame0 = SpriteFrameCache::getInstance()->getSpriteFrameByName(str->getCString());
        target0->setDisplayFrame(frame0);
        target0->setPosition(pos0);
        target0->setVisible(true);
        targetNum0->setString(String::createWithFormat("x %d", ((Integer*)currentTargetNum->objectAtIndex(0))->getValue())->getCString());
        targetNum0->setVisible(true);
        targetNum0->setPosition(pos0 + offset);
        //log("set target0-[%s]", str->getCString());
        
        String *str1 = (String*)currentTargets->objectAtIndex(1);
        frame1 = SpriteFrameCache::getInstance()->getSpriteFrameByName(str1->getCString());
        target1->setDisplayFrame(frame1);
        target1->setPosition(pos2);
        target1->setVisible(true);
        targetNum1->setString(String::createWithFormat("x %d", ((Integer*)currentTargetNum->objectAtIndex(1))->getValue())->getCString());
        targetNum1->setVisible(true);
        targetNum1->setPosition(pos2 + offset2);
        //log("set target1-[%s]", str1->getCString());
        
    }else if(currentTargets->count() >= 3){
        log("for count == 3 <---------------------->");
        String *str = (String*)currentTargets->objectAtIndex(0);
        frame0 = SpriteFrameCache::getInstance()->getSpriteFrameByName(str->getCString());
        target0->setDisplayFrame(frame0);
        target0->setPosition(pos0);
        target0->setVisible(true);
        targetNum0->setString(String::createWithFormat("x %d", ((Integer*)currentTargetNum->objectAtIndex(0))->getValue())->getCString());
        targetNum0->setVisible(true);
        targetNum0->setPosition(pos0 + offset);
        //log("set target0-[%s]", str->getCString());
        
        String *str1 = (String*)currentTargets->objectAtIndex(1);
        frame1 = SpriteFrameCache::getInstance()->getSpriteFrameByName(str1->getCString());
        target1->setDisplayFrame(frame1);
        target1->setPosition(pos1);
        target1->setVisible(true);
        targetNum1->setString(String::createWithFormat("x %d", ((Integer*)currentTargetNum->objectAtIndex(1))->getValue())->getCString());
        targetNum1->setVisible(true);
        targetNum1->setPosition(pos1 + offset1);
        //log("set target1-[%s]", str1->getCString());
        
        String *str2 = (String*)currentTargets->objectAtIndex(2);
        frame2 = SpriteFrameCache::getInstance()->getSpriteFrameByName(str2->getCString());
        target2->setDisplayFrame(frame2);
        target2->setPosition(pos2);
        target2->setVisible(true);
        targetNum2->setString(String::createWithFormat("x %d", ((Integer*)currentTargetNum->objectAtIndex(2))->getValue())->getCString());
        targetNum2->setVisible(true);
        targetNum2->setPosition(pos2 + offset2);
        //log("set target2-[%s]", str2->getCString());
    }else{
        log("no target any more");
        //Win the game!!!!!!
        doGameWin();
    }
    return;
}

void GameLayer::clickTarget(Sprite *clickedSprite, const char *targetName)
{
    log("-----------------------------------click start------------------------");
    log("click One Target");
    onAction=true;
    for(int i=0; i<currentTargets->count() && i<3; i++){//find current object on target panel
        String *str = (String*)currentTargets->objectAtIndex(i);
        //log("currentTarget %d:[%s]", i, str->getCString());
        if(strcmp(str->getCString(), targetName) == 0){
            if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("zoom.mp3");
            log("FIND ONE!!! [%s] @index %d", targetName, i);
            //here play a action of clickedSprite -------> targetSprite
            //Point srcP = convertToWorldSpace(clickedSprite->getPosition());
            Point dstP = Point::ZERO;
            float dstScale = 0.5;
            Sprite *targetSprite = NULL;
            if(currentTargets->count() == 1){
                targetSprite = target0;
                dstP = target0->getPosition();
                dstScale = target0->getScale();
            }else if(currentTargets->count() == 2){
                if(i==0) {dstP = target0->getPosition();dstScale = target0->getScale();targetSprite=target0;}
                else if(i==1) {dstP = target1->getPosition();dstScale = target1->getScale();targetSprite=target1;}
            }else if(currentTargets->count() >= 3){
                if(i==0) {dstP = target0->getPosition();dstScale = target0->getScale();targetSprite=target0;}
                else if(i==1) {dstP = target1->getPosition();dstScale = target1->getScale();targetSprite=target1;}
                else if(i==2) {dstP = target2->getPosition();dstScale = target2->getScale();targetSprite=target2;}
            }else{
                CCASSERT(true, "ERROR, currentTargets is invalid");
            }
            Sprite *sprite = Sprite::createWithSpriteFrameName(targetName);
            //sprite->setScale(0.5);
            sprite->setPosition(clickedSpritePosition);
            sprite->setRotation(clickedSprite->getRotation());
            this->addChild(sprite, 25);
            
            clickedSprite->setVisible(false);
            //MoveTo *move = MoveTo::create(0.4, dstP);
            //ScaleTo *scale = ScaleTo::create(0.4, dstScale);
            ccBezierConfig bezierc;
            bezierc.controlPoint_1 = Point(sprite->getPosition().x, dstP.y);
            bezierc.endPosition = dstP;
            BezierTo *move = BezierTo::create(0.8, bezierc);
            Sequence *seq = Sequence::create(move, CallFunc::create(CC_CALLBACK_0(GameLayer::actionClikedOver, this, clickedSprite, sprite, i)),NULL);
            //sprite->runAction(scale);
            sprite->runAction(seq);
            return;
        }
    }
    
    //get coins and tools
    if(strcmp(targetName, "10000.png") == 0 ||
       strcmp(targetName, "10001.png") == 0 ||
       strcmp(targetName, "10002.png") == 0 ||
       strcmp(targetName, "10004.png") == 0 ||
       strcmp(targetName, "10005.png") == 0){
        log("click for a tool or coin %s", targetName);
        
        clickedSprite->setVisible(false);
        Sprite *sprite = Sprite::createWithSpriteFrameName(targetName);
        sprite->setPosition(clickedSpritePosition);
        Point dstP = Point::ZERO;
        if(strcmp(targetName, "10000.png") == 0){
            dstP = ingameCleanSprite->getPosition();
            GameData::getInstance()->addToolNum(GameData::kToolClean, 1);
        }else if(strcmp(targetName, "10001.png") == 0){
            dstP = ingameGlassSprite->getPosition();
            GameData::getInstance()->addToolNum(GameData::kToolGlass, 1);
        }else if(strcmp(targetName, "10002.png") == 0){//coin
            sprite->setTag(10002);
            dstP = ingameGoldFrameSprite->getPosition();
            unsigned int level = GameData::getInstance()->getSkillLevel(1);//coin nice
            unsigned int min = GameData::getInstance()->getSkillminValue(1, level);
            unsigned int max = GameData::getInstance()->getSkillmaxValue(1, level);
            unsigned int coinNum = min + rand()%(max-min+1);
            log("get coin num [%d-%d],[%d]", min, max, coinNum);
            GameData::getInstance()->addCoinNum(coinNum);//TODO change to level
        }else if(strcmp(targetName, "10004.png") == 0){
            dstP = ingameRotateSprite->getPosition();
            GameData::getInstance()->addToolNum(GameData::kToolRotate, 1);
        }else if(strcmp(targetName, "10005.png") == 0){
            dstP = ingameClockSprite->getPosition();
            GameData::getInstance()->addToolNum(GameData::kToolClock, 1);
        }
        this->addChild(sprite, 25);
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        //MoveTo *move = MoveTo::create(1.0, dstP);
        ccBezierConfig bezierc;
        bezierc.controlPoint_1 = Point(sprite->getPosition().x, dstP.y);
        bezierc.endPosition = dstP;
        BezierTo *move = BezierTo::create(0.8, bezierc);
        Sequence *seq = Sequence::create(move, CallFunc::create(CC_CALLBACK_0(GameLayer::getToolFinish, this, clickedSprite, sprite)), NULL);
        sprite->runAction(seq);
        return;
    }
    
    if(strcmp(targetName, "10006.png") == 0){
        int toolIndex = rand()%kBadToolInvalid;//TODO here come from level
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        //int toolIndex=kBadToolPollute;
        if(currentRunBadTool != kBadToolInvalid){//already running a bad tool
            //onAction = false;
            log("current running, return!, but set can clicked");//TODO here can replace by get coins with box
            //return;//can't click box here
            toolIndex = 4;//set coin
        }
        log("click for a nice box, tool is %d", toolIndex);
        //firsrt change the box sprite frame to open
        Sprite *sprite = NULL;
        Point dstP = Point::ZERO;
        if(toolIndex<=4){//for tools
            if(toolIndex==0){//clock
                sprite = Sprite::createWithSpriteFrameName("10005.png");
                //sprite->setScale(0.5);
                sprite->setPosition(clickedSpritePosition);
                dstP = ingameClockSprite->getPosition();
                GameData::getInstance()->addToolNum(GameData::kToolClock, 1);
            }else if(toolIndex == 1){//glass
                sprite = Sprite::createWithSpriteFrameName("10001.png");
                //sprite->setScale(0.5);
                sprite->setPosition(clickedSpritePosition);
                dstP = ingameGlassSprite->getPosition();
                GameData::getInstance()->addToolNum(GameData::kToolGlass, 1);
            }else if(toolIndex == 2){//rotate
                sprite = Sprite::createWithSpriteFrameName("10004.png");
                //sprite->setScale(0.5);
                sprite->setPosition(clickedSpritePosition);
                dstP = ingameRotateSprite->getPosition();
                GameData::getInstance()->addToolNum(GameData::kToolRotate, 1);
            }else if(toolIndex == 3){//clean
                sprite = Sprite::createWithSpriteFrameName("10000.png");
                //sprite->setScale(0.5);
                sprite->setPosition(clickedSpritePosition);
                dstP = ingameCleanSprite->getPosition();
                GameData::getInstance()->addToolNum(GameData::kToolClean, 1);
            }else if(toolIndex == 4){//coin
                sprite = Sprite::createWithSpriteFrameName("10002.png");
                sprite->setTag(10002);
                //sprite->setScale(0.5);
                sprite->setPosition(clickedSpritePosition);
                dstP = ingameGoldFrameSprite->getPosition();
                unsigned int level = GameData::getInstance()->getSkillLevel(1);//coin nice
                unsigned int min = GameData::getInstance()->getSkillminValue(1, level);
                unsigned int max = GameData::getInstance()->getSkillmaxValue(1, level);
                unsigned int coinNum = min + rand()%(max-min+1);
                log("get coin num [%d-%d],[%d]", min, max, coinNum);
                GameData::getInstance()->addCoinNum(coinNum);//TODO change to level
            }
            this->addChild(sprite, 25);
            //MoveTo *move = MoveTo::create(1.0, dstP);
            ccBezierConfig bezierc;
            bezierc.controlPoint_1 = Point(sprite->getPosition().x, dstP.y);
            bezierc.endPosition = dstP;
            BezierTo *move = BezierTo::create(0.8, bezierc);
            Sequence *seq = Sequence::create(move,
                    CallFunc::create(CC_CALLBACK_0(GameLayer::getToolFinish,this, clickedSprite, sprite)),
                    NULL);
            SpriteFrame *frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("10006_open.png");
            clickedSprite->setDisplayFrame(frame);
            sprite->runAction(seq);
        }else{//bad tools
            /* TODO
             * Note: here to check bad tools, when run action bad tools can click normal sprite, but cant do bad action
             * until current bad action run end.
             */
            //onAction=false;
            if(currentRunBadTool != kBadToolInvalid) {log("can't run more than one bad effect, return");return;}//can't run more than one bad effect
            dstP = ingameMaskBallSprite->getPosition();
            if(toolIndex==kBadToolWater){
                log("water");
                currentRunBadTool = kBadToolWater;
                sprite = Sprite::createWithSpriteFrameName("10011.png");
                //sprite->setScale(0.5);
                sprite->setPosition(clickedSpritePosition);
            }else if(toolIndex==kBadToolBlock){
                log("wood block");
                currentRunBadTool = kBadToolBlock;
                sprite = Sprite::createWithSpriteFrameName("10008.png");
                //sprite->setScale(0.5);
                sprite->setPosition(clickedSpritePosition);
            }else if(toolIndex==kBadToolPollute){
                log("pollute plane");
                currentRunBadTool = kBadToolPollute;
                sprite = Sprite::createWithSpriteFrameName("10010.png");
                //sprite->setScale(0.5);
                sprite->setPosition(clickedSpritePosition);
            }else if(toolIndex==kBadToolForbid){
                log("forbid");
                currentRunBadTool = kBadToolForbid;
                sprite = Sprite::createWithSpriteFrameName("10009.png");
                //sprite->setScale(0.5);
                sprite->setPosition(clickedSpritePosition);
            }else if(toolIndex==kBadToolAntiRotate){
                log("anti-rotate");
                //if(rotateToolRunning) {onAction=false;return;}//can't run
                currentRunBadTool = kBadToolAntiRotate;
                sprite = Sprite::createWithSpriteFrameName("10007.png");
                //sprite->setScale(0.5);
                sprite->setPosition(clickedSpritePosition);
            }else if(toolIndex==kBadToolSlowRotation){
                log("slow-rotate");
                //if(rotateToolRunning) {onAction=false;return;}//can't run
                currentRunBadTool = kBadToolSlowRotation;
                sprite = Sprite::createWithSpriteFrameName("10003.png");
                //sprite->setScale(0.5);
                sprite->setPosition(clickedSpritePosition);
            }
            this->addChild(sprite, 25);
            currentBadActionSprite = sprite;
            //MoveTo *move = MoveTo::create(1.0, dstP);
            ccBezierConfig bezierc;
            bezierc.controlPoint_1 = Point(sprite->getPosition().x, dstP.y);
            bezierc.endPosition = dstP;
            BezierTo *move = BezierTo::create(0.8, bezierc);
            Sequence *seq = Sequence::create(move, CallFunc::create(CC_CALLBACK_0(GameLayer::getBadTool, this, clickedSprite, sprite, toolIndex)), NULL);
            SpriteFrame *frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("10006_open.png");
            clickedSprite->setDisplayFrame(frame);
            sprite->runAction(seq);
        }
        return;
    }
    //click target show shake
    MoveBy *moveLeft = MoveBy::create(0.4, Point(20,0));
    MoveBy *moveRight = MoveBy::create(0.4, Point(-20,0));
    Sequence *seq2 = Sequence::create(moveLeft, moveRight, NULL);
    Action *action = EaseElasticInOut::create(seq2, 0.2);
    clickedSprite->runAction(action);
    
    errorClickTimes++;
    if(timeValue-5<0){
        timeValue=0;
        onAction=true;
    }else timeValue-=5;
    showTimeIncDecAction(false);
    log("NO! NO! Error clicked[%s], timeValue=%d", targetName, timeValue);
    
    if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("wrong.mp3");
    updateTime();
    return;
}

void GameLayer::getToolFinish(cocos2d::Sprite *targetSprite, cocos2d::Sprite *actionSprite)
{log("getTool Finish");
    //finish, first update toolNums
    updateToolNum();
    //update coin num
    if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("coin.mp3");
    if(actionSprite->getTag() == 10002){
        log("get Coin , update Nums");
        updateCoin();
    }
    
    //need replace the opened box with other sprite
    char name[1024];
    int iRandom;// = rand()%75 + GameData::getInstance()->currentStage*1000;
    unsigned int stage = GameData::getInstance()->currentStage;
    unsigned int substage = GameData::getInstance()->currentSubStage;
    while(true){
        if(gameMode != kGameModeEternal) iRandom = rand()%55 + ((substage+2)/3-1)*5 + stage*1000;
        else iRandom = GameData::getInstance()->getEternalItem();
        sprintf(name, "%d.png", iRandom);
        bool find = false;
        for(int i=0; i<currentTargets->count(); i++){
            String* str = (String*)currentTargets->objectAtIndex(i);
            if(strcmp(str->getCString(), name)==0){
                find=true;
                break;
            }
        }
        if(find) continue;
        break;
    }
    
    SpriteFrame *frame=SpriteFrameCache::getInstance()->getSpriteFrameByName(name);
    log("rand replace Sprite[%s]", name);
    float scaleSize = targetSprite->getScale();
    targetSprite->setVisible(true);
    targetSprite->setScale(0.01);
    targetSprite->setDisplayFrame(frame);
    //targetSprite->setVisible(true);
    
    Array *arrName=NULL;
    if(clickedLine==0) arrName = rotateSpriteNameArray0;
    else if(clickedLine==1) arrName = rotateSpriteNameArray1;
    else if(clickedLine==2) arrName = rotateSpriteNameArray2;
    else if(clickedLine==3) arrName = rotateSpriteNameArray3;
    arrName->removeObjectAtIndex(clickedIndex);
    arrName->insertObject(String::createWithFormat("%s", name), clickedIndex);
    //action scale up
    ScaleTo *scale = ScaleTo::create(0.5, scaleSize);
    targetSprite->runAction(EaseElasticIn::create(scale, 0.5));
    
    //remove the action tool
    if(actionSprite != NULL){//actionSprite->removeFromParentAndCleanup(true);
        FadeOut *fadeOut = FadeOut::create(0.5);
        ScaleBy *scaleUp = ScaleBy::create(0.49, 1.6);
        Sequence *seq = Sequence::create(fadeOut,
                                         CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, actionSprite, true)),
                                         NULL);
        actionSprite->runAction(seq);
        actionSprite->runAction(scaleUp);
    }
    
    //now can clicked again
    onAction = false;
    log("now set can clicked again");
}

void GameLayer::stopEffectById(unsigned int index)
{
    CocosDenshion::SimpleAudioEngine *engine = CocosDenshion::SimpleAudioEngine::getInstance();
    engine->stopEffect(index);
}

void GameLayer::playBlockEffect()
{
    if(GameData::getInstance()->isEffectOn()){
        effectIndex = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("wood.mp3");
    }
}

void GameLayer::getBadTool(cocos2d::Sprite *targetSprite, cocos2d::Sprite *actionSprite, int toolIndex)
{log("getBadTool");
    //need replace the opened box with other sprite
    char name[1024];
    int iRandom;// = rand()%75 + GameData::getInstance()->currentStage*1000;
    unsigned int stage = GameData::getInstance()->currentStage;
    unsigned int substage = GameData::getInstance()->currentSubStage;
//    if(gameMode != kGameModeEternal) iRandom = rand()%55 + ((substage+2)/3-1)*5 + stage*1000;
//    else iRandom = GameData::getInstance()->getEternalItem();
    while(true){
        if(gameMode != kGameModeEternal) iRandom = rand()%55 + ((substage+2)/3-1)*5 + stage*1000;
        else iRandom = GameData::getInstance()->getEternalItem();
        sprintf(name, "%d.png", iRandom);
        bool find = false;
        for(int i=0; i<currentTargets->count(); i++){
            String* str = (String*)currentTargets->objectAtIndex(i);
            if(strcmp(str->getCString(), name)==0){
                find=true;
                break;
            }
        }
        if(find) continue;
        break;
    }
//    int iRandom = rand()%75 + GameData::getInstance()->currentStage*1000;
    sprintf(name, "%d.png", iRandom);
    
    SpriteFrame *frame=SpriteFrameCache::getInstance()->getSpriteFrameByName(name);
    log("rand replace Sprite[%s]", name);
    targetSprite->setScale(0.01);
    targetSprite->setDisplayFrame(frame);
    //targetSprite->setVisible(true);
    
    Array *arrName=NULL;
    if(clickedLine==0) arrName = rotateSpriteNameArray0;
    else if(clickedLine==1) arrName = rotateSpriteNameArray1;
    else if(clickedLine==2) arrName = rotateSpriteNameArray2;
    else if(clickedLine==3) arrName = rotateSpriteNameArray3;
    arrName->removeObjectAtIndex(clickedIndex);
    arrName->insertObject(String::createWithFormat("%s", name), clickedIndex);
    //action scale up
    ScaleTo *scale = ScaleTo::create(0.2, 1);
    targetSprite->runAction(scale);
    
    //retain until the water over
    //actionSprite->removeFromParentAndCleanup(true);
    
    //now can clicked again
    onAction = false;
    Size winSize = Director::getInstance()->getVisibleSize();
    unsigned int level = GameData::getInstance()->getSkillLevel(6);
    unsigned int diff = GameData::getInstance()->getSkillValue(6, level);
    log("bad value diff = [%d/100]", diff);
    if(toolIndex==kBadToolWater){
        //if(waterSprite) waterSprite->removeFromParentAndCleanup(true);
        currentRunBadTool = kBadToolWater;
        waterSprite = Sprite::createWithSpriteFrameName("bg_water.png");
        waterSprite->setOpacity(192);
        Size itemSize = waterSprite->getContentSize();
        waterSprite->setScale(2);
        waterSprite->setPosition(Point(winSize.width/2, -itemSize.height/1));
        float defaultTime = 15;
        defaultTime = defaultTime*(100-diff)/100.0;
        log("water for time[%f]", defaultTime);
        MoveBy *moveUp = MoveBy::create(defaultTime*1.0/15, Point(0,winSize.height));
        MoveBy *moveDown = MoveBy::create(defaultTime*14.0/15, Point(0,-winSize.height));
        if(GameData::getInstance()->isEffectOn())
            effectIndex = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("water1.mp3", true);
        Sequence *seq = Sequence::create(moveUp, moveDown,
                                         CallFunc::create(CC_CALLBACK_0(GameLayer::badToolActionOver,this, actionSprite, toolIndex))
                                         , NULL);
        this->addChild(waterSprite, 5);//should on all rotate layer sprite up
        waterSprite->runAction(seq);
        badToolRunning = true;
    }else if(toolIndex == kBadToolBlock){
        currentRunBadTool = kBadToolBlock;
        if(GameData::getInstance()->isEffectOn()){
            effectIndex = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("wood.mp3");
        }
        blockSideSprite = Sprite::createWithSpriteFrameName("block.png");
        blockLeftSprite = Sprite::createWithSpriteFrameName("block_left.png");
        blockMidSprite = Sprite::createWithSpriteFrameName("block_mid.png");
        blockRightSprite = Sprite::createWithSpriteFrameName("block_right.png");
        blockSideMirrorSprite = Sprite::createWithSpriteFrameName("block.png");
        blockSideMirrorSprite->setFlipX(true);
        blockLeftMirrorSprite = Sprite::createWithSpriteFrameName("block_left.png");
        blockLeftMirrorSprite->setFlipX(true);
        blockMidMirrorSprite = Sprite::createWithSpriteFrameName("block_mid.png");
        blockMidMirrorSprite->setFlipX(true);
        blockRightMirrorSprite = Sprite::createWithSpriteFrameName("block_right.png");
        blockRightMirrorSprite->setFlipX(true);
        
        Point offset = Point::ZERO;//Point(winSize.width/2, 0);
        Size leftSize = blockLeftSprite->getContentSize();
        Size midSize = blockMidSprite->getContentSize();
        Size rightSize = blockRightSprite->getContentSize();
        Size sideSize = blockSideSprite->getContentSize();
        Point start = Point(sideSize.width/2, 0);
        blockSideSprite->setPosition(Point(sideSize.width/2 - 150, sideSize.height/2) - offset);
        blockLeftSprite->setPosition(Point(leftSize.width/2, leftSize.height/2) - offset + start);
        
        blockMidSprite->setAnchorPoint(Point(1,0));
        blockMidSprite->setPosition(Point(midSize.width/2, midSize.height/2) - offset + start
                                          +Point(midSize.width/2, -midSize.height/2));
        blockMidSprite->setRotation(-60);
        
        blockRightSprite->setAnchorPoint(Point(1,0));
        blockRightSprite->setPosition(Point(midSize.width/1 - 65, rightSize.height/2) - offset + start
                                            +Point(rightSize.width/2, -rightSize.height/2));
        blockRightSprite->setRotation(-90);
        
        blockSideMirrorSprite->setPosition(Point(winSize.width - sideSize.width/2 + 150, sideSize.height/2) + offset);
        blockLeftMirrorSprite->setPosition(Point(winSize.width - leftSize.width/2, leftSize.height/2) + offset - start);
        
        blockMidMirrorSprite->setAnchorPoint(Point(0,0));
        blockMidMirrorSprite->setPosition(Point(winSize.width - midSize.width/2, midSize.height/2) + offset - start//);
                                          +Point(-midSize.width/2, -midSize.height/2));
        blockMidMirrorSprite->setRotation(60);
        
        blockRightMirrorSprite->setAnchorPoint(Point(0,0));
        blockRightMirrorSprite->setPosition(Point(winSize.width - (midSize.width/1 - 65), rightSize.height/2) + offset - start//);
                                            +Point(-rightSize.width/2, -rightSize.height/2));
        blockRightMirrorSprite->setRotation(90);
        
        //do Action
        float defaultTime = 15.0/8;
        float time = defaultTime * (100-diff)/100.0;
        log("block for time %f", time*2);
        
        RotateBy *rotateMid = RotateBy::create(0.6, 60);
        blockMidSprite->runAction(rotateMid);
        
        RotateBy *rotateRight = RotateBy::create(0.6, 90);
        Sequence *seq = Sequence::create(DelayTime::create(0.6),
                                         CallFunc::create(CC_CALLBACK_0(GameLayer::playBlockEffect, this)),
                                         rotateRight,
                                         CallFunc::create(CC_CALLBACK_0(GameLayer::playBlockEffect, this)),
                                         NULL);
        blockRightSprite->runAction(seq);
        
        RotateBy *rotateMidMirror = RotateBy::create(0.6, -60);
        blockMidMirrorSprite->runAction(rotateMidMirror);
        
        RotateBy *rotateRightMirror = RotateBy::create(0.6, -90);
        Sequence *seqMirror = Sequence::create(DelayTime::create(0.6),
                                               CallFunc::create(CC_CALLBACK_0(GameLayer::playBlockEffect, this)),
                                               rotateRightMirror,
                                               CallFunc::create(CC_CALLBACK_0(GameLayer::playBlockEffect, this)),
                                               DelayTime::create(time),
                                               CallFunc::create(CC_CALLBACK_0(GameLayer::badToolActionOver, this, actionSprite, toolIndex)),
                                               NULL);
        blockRightMirrorSprite->runAction(seqMirror);
        
        this->addChild(blockRightSprite, 5);
        this->addChild(blockMidSprite, 5);
        this->addChild(blockSideSprite, 5);
        this->addChild(blockLeftSprite, 5);
        
        this->addChild(blockRightMirrorSprite, 5);
        this->addChild(blockMidMirrorSprite, 5);
        this->addChild(blockSideMirrorSprite, 5);
        this->addChild(blockLeftMirrorSprite, 5);
        
        badToolRunning = true;
    }else if(toolIndex == kBadToolPollute){
        currentRunBadTool = kBadToolPollute;
        polluteToRightSprite = Sprite::createWithSpriteFrameName("pollute_flyToRight.png");
        polluteToRightSprite->setScale(1.1);
        polluteToLeftSprite = Sprite::createWithSpriteFrameName("pollute_flyToLeft.png");
        polluteToLeftSprite->setScale(1.6);
        this->addChild(polluteToRightSprite, 25);
        this->addChild(polluteToLeftSprite, 25);
        
        polluteToRightSprite2 = Sprite::createWithSpriteFrameName("pollute_flyToRight.png");
        polluteToRightSprite2->setScale(1.6);
        polluteToLeftSprite2 = Sprite::createWithSpriteFrameName("pollute_flyToLeft.png");
        polluteToLeftSprite2->setScale(1.1);
        this->addChild(polluteToRightSprite2, 25);
        this->addChild(polluteToLeftSprite2, 25);
        Size itemSize = polluteToRightSprite->getContentSize();
        
        if(GameData::getInstance()->isEffectOn()){
            effectIndex = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("plane.mp3");
        }
        polluteToRightSprite->setPosition(Point(-itemSize.width, winSize.height/2+itemSize.height*3/2-20));
        polluteToLeftSprite->setPosition(Point(winSize.width+itemSize.width, winSize.height/2-10/*-itemSize.height/2*/));
        
        polluteToRightSprite2->setPosition(Point(-itemSize.width, winSize.height/2-10/*-itemSize.height/2*/));
        polluteToLeftSprite2->setPosition(Point(winSize.width+itemSize.width/2+10, winSize.height/2+itemSize.height*3/2-20));
        
        float defaultTime = 10;
        defaultTime = defaultTime * (100 - diff)/100.0;
        log("pollute for time[%f]", defaultTime);
        MoveBy *moveRight = MoveBy::create(defaultTime, Point(winSize.width + itemSize.width*2, 0));
        MoveBy *moveLeft = MoveBy::create(defaultTime, Point(-(winSize.width + itemSize.width*2), 0));
        
        MoveBy *moveRight2 = MoveBy::create(defaultTime, Point(winSize.width + itemSize.width*2, 0));
        MoveBy *moveLeft2 = MoveBy::create(defaultTime, Point(-(winSize.width + itemSize.width), 0));
        Sequence *seqRight = Sequence::create(DelayTime::create(defaultTime),
                                              CallFunc::create(CC_CALLBACK_0(GameLayer::playPolluteEffect, this)),
                                              moveRight2,
                                              CallFunc::create(CC_CALLBACK_0(GameLayer::badToolActionOver, this, actionSprite, toolIndex)),
                                              NULL);
        Sequence *seqLeft = Sequence::create(DelayTime::create(defaultTime), moveLeft2, NULL);
        polluteToRightSprite->runAction(moveRight);
        polluteToLeftSprite->runAction(moveLeft);
        polluteToRightSprite2->runAction(seqRight);
        polluteToLeftSprite2->runAction(seqLeft);
        
        badToolRunning = true;
    }else if(toolIndex == kBadToolForbid){//
        currentRunBadTool = kBadToolForbid;
        forbidUpSprite = Sprite::createWithSpriteFrameName("forbid_up.png");
        forbidDownSprite = Sprite::createWithSpriteFrameName("forbid_down.png");
        Size itemSize = forbidUpSprite->getContentSize();
        forbidUpSprite->setPosition(Point(winSize.width/2, winSize.height-itemSize.height/2));
        itemSize = forbidDownSprite->getContentSize();
        forbidDownSprite->setPosition(Point(winSize.width/2, itemSize.height/2-60));
        //forbidSprite->setScale(0.5);
        float defaultTime = 5;
        defaultTime = defaultTime * (100-diff)/100.0;
        log("forbid for time[%f]", defaultTime);
        this->addChild(forbidUpSprite, 15);
        this->addChild(forbidDownSprite, 15);
        MoveBy *moveDown = MoveBy::create(0.5, Point(0,-80));
        MoveBy *moveUp = MoveBy::create(0.5, Point(0,60));
        forbidUpSprite->runAction(moveDown);
        forbidDownSprite->runAction(moveUp);
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("steel.mp3");
        Sequence *seq = Sequence::create(DelayTime::create(defaultTime), CallFunc::create(CC_CALLBACK_0(GameLayer::badToolActionOver, this, actionSprite, toolIndex)),NULL);
        forbidUpSprite->runAction(seq);
        badToolRunning = true;
    }else if(toolIndex == kBadToolAntiRotate){
        currentRunBadTool = kBadToolAntiRotate;
        if(!rotateToolRunning){
            rotateLayer->stopAllActions();
            float speed = ROTATE_SPEED*negtive;
            speed = speed*2;
            log("rand rotate speed[%f]", speed);
            RotateBy *rotate = RotateBy::create(0.4, speed);
            Action *action = RepeatForever::create(rotate);
            rotateLayer->runAction(action);
        }
        
        float defaultTime = 5;
        defaultTime = defaultTime * (100-diff)/100.0;
        log("AntiRotate for time[%f]", defaultTime);
        lastBadRotateRunTime = defaultTime;
        Sequence *seq = Sequence::create(DelayTime::create(defaultTime), CallFunc::create(CC_CALLBACK_0(GameLayer::badToolActionOver, this, actionSprite, toolIndex)),NULL);
        rotateLayer->runAction(seq);
        badToolRunning = true;
    }else if(toolIndex == kBadToolSlowRotation){
        currentRunBadTool = kBadToolSlowRotation;
        if(!rotateToolRunning){
            rotateLayer->stopAllActions();
            float speed = ROTATE_SPEED*negtive;
            speed = speed/2;
            log("rand rotate speed[%f]", speed);
            RotateBy *rotate = RotateBy::create(0.4, speed);
            Action *action = RepeatForever::create(rotate);
            rotateLayer->runAction(action);
        }
        float defaultTime = 5;
        defaultTime = defaultTime * (100-diff)/100.0;
        log("AntiRotate for time[%f]", defaultTime);
        lastBadRotateRunTime = defaultTime;
        Sequence *seq = Sequence::create(DelayTime::create(defaultTime),
                                         CallFunc::create(CC_CALLBACK_0(GameLayer::badToolActionOver, this, actionSprite, toolIndex)),
                                         NULL);
        rotateLayer->runAction(seq);
        badToolRunning = true;
    }
}

void GameLayer::clearBlock()
{
    RotateBy *rotateRight = RotateBy::create(0.6, -90);
    Sequence *seqRight = Sequence::create(
                                          CallFunc::create(CC_CALLBACK_0(GameLayer::playBlockEffect, this)),
                                          rotateRight,
                                          CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, blockRightSprite, true)),
                                          NULL);
    blockRightSprite->runAction(seqRight);
    RotateBy *rotateMid = RotateBy::create(0.6, -60);
    Sequence *seqMid = Sequence::create(
                                        DelayTime::create(0.6),
                                        CallFunc::create(CC_CALLBACK_0(GameLayer::playBlockEffect, this)),
                                        rotateMid,
                                        CallFunc::create(CC_CALLBACK_0(GameLayer::playBlockEffect, this)),
                                        CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, blockMidSprite, true)),
                                        NULL);
    blockMidSprite->runAction(seqMid);
    
    RotateBy *rotateRightMirror = RotateBy::create(0.6, 90);
    Sequence *seqRightMirror = Sequence::create(rotateRightMirror,
                                                CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, blockRightMirrorSprite, true)),
                                                NULL);
    blockRightMirrorSprite->runAction(seqRightMirror);
    
    RotateBy *rotateMidMirrir = RotateBy::create(0.6, 60);
    Sequence *seqMidMirror = Sequence::create(DelayTime::create(0.6),
                                              rotateMidMirrir,
                                              CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, blockMidMirrorSprite, true)),
                                              NULL);
    blockMidMirrorSprite->runAction(seqMidMirror);
    
    Sequence *seq = Sequence::create(
                                     DelayTime::create(1.4),
                                     CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, blockSideSprite, true)),
                                     CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, blockSideMirrorSprite, true)),
                                     CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, blockLeftSprite, true)),
                                     CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, blockLeftMirrorSprite, true)),
                                     CallFunc::create(CC_CALLBACK_0(GameLayer::blockOver, this)),
                                     NULL);
    this->runAction(seq);
}

void GameLayer::blockOver()
{
    currentRunBadTool = kBadToolInvalid;
    badToolRunning = false;
    blockStartClean = false;
}

void GameLayer::badToolActionOver(Sprite *actionSprite, int type)
{log("badToolAction Over");
    if(inCleaning) return;//clean by clean tool no need clean automatically
    if(currentRunBadTool == kBadToolInvalid) return;//no need to do things
    if(currentRunBadTool != kBadToolBlock) currentRunBadTool = kBadToolInvalid;//not run
    if(type == kBadToolWater){
        waterSprite->removeFromParentAndCleanup(true);
        stopEffectById(effectIndex);
        effectIndex = -1;
    }else if(type == kBadToolBlock){
        blockStartClean=true;
        stopEffectById(effectIndex);
        effectIndex = -1;
        clearBlock();
        /*
        blockSideSprite->removeFromParentAndCleanup(true);
        blockLeftSprite->removeFromParentAndCleanup(true);
        blockMidSprite->removeFromParentAndCleanup(true);
        blockRightSprite->removeFromParentAndCleanup(true);
        
        blockSideMirrorSprite->removeFromParentAndCleanup(true);
        blockLeftMirrorSprite->removeFromParentAndCleanup(true);
        blockMidMirrorSprite->removeFromParentAndCleanup(true);
        blockRightMirrorSprite->removeFromParentAndCleanup(true);
         */
    }else if(type == kBadToolPollute){
        stopEffectById(effectIndex);
        effectIndex = -1;
        polluteToLeftSprite->removeFromParentAndCleanup(true);
        polluteToRightSprite->removeFromParentAndCleanup(true);
        polluteToLeftSprite2->removeFromParentAndCleanup(true);
        polluteToRightSprite2->removeFromParentAndCleanup(true);
    }else if(type == kBadToolForbid){
        forbidUpSprite->removeFromParentAndCleanup(true);
        forbidDownSprite->removeFromParentAndCleanup(true);
    }else if(type == kBadToolAntiRotate){
        if(!rotateToolRunning){
            log("no rotateToolRuning");
            rotateLayer->stopAllActions();
            RotateBy *rotate = RotateBy::create(0.4, ROTATE_SPEED*negtive);
            Action *action = RepeatForever::create(rotate);
            rotateLayer->runAction(action);
        }else{
            log("rotate tool running");
        }
    }else if(type == kBadToolSlowRotation){
        if(!rotateToolRunning){
            log("no rotateToolRuning");
            rotateLayer->stopAllActions();
            RotateBy *rotate = RotateBy::create(0.4, ROTATE_SPEED*negtive);
            Action *action = RepeatForever::create(rotate);
            rotateLayer->runAction(action);
        }else{
            log("rotate tool running");
        }
    }
    //actionSprite->removeFromParentAndCleanup(true);
    if(currentRunBadTool != kBadToolBlock){
        FadeOut *fade = FadeOut::create(0.8);
        Sequence *seq = Sequence::create(fade,
                                         CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, actionSprite, true)),
                                         NULL);
        actionSprite->runAction(seq);
        badToolRunning = false;
    }else{
        FadeOut *fade = FadeOut::create(0.8);
        Sequence *seq = Sequence::create(DelayTime::create(1.4),
                                         fade,
                                         CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, actionSprite, true)),
                                         NULL);
        actionSprite->runAction(seq);
        badToolRunning = false;
    }
    log("can running bad tool again");
}

void GameLayer::updateToolNum()
{
    char name[5];
    unsigned int num = GameData::getInstance()->getToolNum(GameData::kToolClock);
    sprintf(name, "%u", num);
    clockNumLabel->setString(name);
    
    num = GameData::getInstance()->getToolNum(GameData::kToolGlass);
    sprintf(name, "%u", num);
    glassNumLabel->setString(name);
    
    num = GameData::getInstance()->getToolNum(GameData::kToolRotate);
    sprintf(name, "%u", num);
    rotateNumLabel->setString(name);
    
    num = GameData::getInstance()->getToolNum(GameData::kToolClean);
    sprintf(name, "%u", num);
    cleanNumLabel->setString(name);
}

void GameLayer::updateTime()
{
    float percent = timeValue*1.0/timeTotal;
    Point position = ingameMaskBallSprite->getPosition()-Point(0,5);//animateTimeSprite->getPosition();
    Size size = ingameMaskBallSprite->getContentSize();
    position.y -= 155*(1-percent);
    //animateTimeSprite->setPosition(position);
    MoveTo *mov = MoveTo::create(0.2, position);
    Sequence *seq = Sequence::create(mov, CallFunc::create(CC_CALLBACK_0(GameLayer::updateTimeEnd, this)), NULL);
    animateTimeSprite->runAction(seq);
}

void GameLayer::updateTimeEnd()
{
    onAction=false;
    timeLabel->setString(timeString);
}

void GameLayer::actionClikedOver(Sprite *clickedSprite, Sprite *sprite, int index)
{
    //add Coin
    if(gameMode != kGameModeEternal){
        GameData::getInstance()->addCoinNum(1);
        currentStageGetCoin+=1;
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("coin.mp3");
        updateCoin();
    }
    
    //onAction = false;
    log("action ClikedOver, set can click target again");
    //sprite->removeFromParentAndCleanup(true);
    FadeOut *fadeOut = FadeOut::create(0.5);
    ScaleBy *scaleUp = ScaleBy::create(0.5, 1.6);
    Sequence *seq = Sequence::create(fadeOut,
                                     CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, sprite, true)),
                                     NULL);
    sprite->runAction(seq);
    sprite->runAction(scaleUp);
    
    bool clearType = false;
    int i=index;
    int value = ((Integer*)currentTargetNum->objectAtIndex(i))->getValue();
    curTargetsNum--;//must decress some type of target
    if(value==1){//need remove this target, and add 5 second
        rightTargetNum++;
        rightTargetTypeNum++;
        clearType = true;
        currentTargets->removeObjectAtIndex(i);
        currentTargetNum->removeObjectAtIndex(i);
        currentClickedSprite = clickedSprite;
        if(gameMode == kGameModeEternal) getOneEternalItem();//only update targets, BUT current sprite maybe replace as a target!!
        updateTargets();
        if(gameMode!=kGameModeEternal) timeValue+=10;
        else timeValue+=15;
        if(timeValue > timeTotal) timeTotal = timeValue;
        showTimeIncDecAction(true);
        //showTimeIncDecAction(true);
        //show scale down with rotate action
        updateTime();
    }else{
        rightTargetNum++;
        if(gameMode==kGameModeEternal){
            timeValue+=5;
            if(timeValue > timeTotal) timeTotal = timeValue;
            showTimeIncDecAction(true);
            updateTime();
        }
        //Integer* value = (Integer*)currentTargetNum->objectAtIndex(i);
        //*value = Integer(value->getValue()+1);
        Integer* value = (Integer*)currentTargetNum->objectAtIndex(i);
        int data = value->getValue();
        currentTargetNum->removeObjectAtIndex(i);
        data--;
        log("udpate num, Data=%d", data);
        currentTargetNum->insertObject(Integer::create(data), i);
        updateTargets();
    }
    
    //random replace one
    char name[1024];
    //int iRandom = rand()%75 + GameData::getInstance()->currentStage*1000;
    int randbox = rand()%100;
    //get lucky from skill
    unsigned int level = GameData::getInstance()->getSkillLevel(5);//lucky
    unsigned int lucky_value = GameData::getInstance()->getSkillValue(5, level);
    unsigned int lucky = 50 + lucky_value;
    log("add lucky with [%d+%d] -- [%d]", 50, lucky_value, randbox);
    if(randbox<lucky && clearType){
        int randtool = rand()%6;
        if(randtool==0) sprintf(name, "10006.png");
        else if(randtool==1) sprintf(name, "10000.png");
        else if(randtool==2) sprintf(name, "10001.png");
        else if(randtool==3) sprintf(name, "10002.png");
        else if(randtool==4) sprintf(name, "10004.png");
        else if(randtool==5) sprintf(name, "10005.png");
        else CCASSERT(false, "No possible here");
    }else{
        int iRandom=-1;
        while(true){
            unsigned int stage = GameData::getInstance()->currentStage;
            unsigned int substage = GameData::getInstance()->currentSubStage;
            if(gameMode != kGameModeEternal) iRandom = rand()%55 + ((substage+2)/3-1)*5 + stage*1000;
            else iRandom = GameData::getInstance()->getEternalItem();
            char n[255];
            sprintf(n, "%d.png", iRandom);
            bool find = false;
            for(int i=0; i<currentTargets->count(); i++){//can't be a target
                String* str = (String*)currentTargets->objectAtIndex(i);
                if(strcmp(str->getCString(), n)==0){
                    find = true;
                    break;
                }
            }
            if(find) continue;
            break;
        }
        sprintf(name, "%d.png", iRandom);
    }
    
    SpriteFrame *frame=SpriteFrameCache::getInstance()->getSpriteFrameByName(name);
    clickedSprite->setDisplayFrame(frame);
    clickedSprite->setScale(0.01);
    clickedSprite->setVisible(true);
    
    Array *arrName=NULL;
    if(clickedLine==0) arrName = rotateSpriteNameArray0;
    else if(clickedLine==1) arrName = rotateSpriteNameArray1;
    else if(clickedLine==2) arrName = rotateSpriteNameArray2;
    else if(clickedLine==3) arrName = rotateSpriteNameArray3;
    arrName->removeObjectAtIndex(clickedIndex);
    arrName->insertObject(String::createWithFormat("%s", name), clickedIndex);
    log("rand replace Sprite[%s]", name);
    onAction = false;
    //action scale up
    float tScale = 0.6 + rand()%70/100.0;//0.6 ~ 1.3
    ScaleTo *scale = ScaleTo::create(0.5, tScale);
    clickedSprite->runAction(EaseElasticInOut::create(scale,0.5));
    if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("correct.mp3");
    printAllTarget();//for debug
}

void GameLayer::initTime()
{
    //timeTotal = 60;//read from skill
    lastTime = 0;
    unsigned int level = GameData::getInstance()->getSkillLevel(4);
    timeTotal = GameData::getInstance()->getSkillValue(4, level);
    timeValue = timeTotal;
    log("start time of level[%d]-time[%d]", level, timeTotal);
    timeLabel = LabelTTF::create("", "STHeitiTC-Bold", 70);
    if(timeValue<60) sprintf(timeString, "%d", timeValue);
    else{
        unsigned int min = timeValue/60;
        unsigned int second = timeValue - min*60;
        sprintf(timeString, "%d:%d", min, second);
    }
    timeLabel->setString(timeString);
    timeLabel->setPosition(ingameMaskBallSprite->getPosition()-Point(0,5));
    this->addChild(timeLabel, 25);
}

void GameLayer::playTimeOutEffect()
{
    if(GameData::getInstance()->isEffectOn())
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("timeup.mp3");
}
void GameLayer::timeOut(float delta)
{
    if(gameOver) return;
    if(glassToolRunning && glassFindSprite!=NULL){
        if(GameData::getInstance()->isEffectOn())
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("zoom.mp3");
        //Sprite *gSprite = glassFindSprite;
        glassActionSprite = Sprite::createWithSpriteFrameName(glassFindName->getCString());
        glassActionSprite->setPosition(glassFindSprite->getPosition());
        glassActionSprite->setRotation(glassFindSprite->getRotation());
        glassActionSprite->setScale(glassFindSprite->getScale());
        rotateLayer->addChild(glassActionSprite,40);
        ScaleBy *scaleUp = ScaleBy::create(0.3, 3);
        FadeOut *fadeout = FadeOut::create(0.3);
        Spawn *spawn = Spawn::create(scaleUp, fadeout, NULL);
        Sequence *seq = Sequence::create(spawn,
                                         CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, glassActionSprite, true)),
                                         NULL);
        glassActionSprite->runAction(seq);
    }
    lastTime += (int)delta;
    timeValue-=(int)delta;
    if(lastBadRotateRunTime - delta>=0)
        lastBadRotateRunTime -= delta;
    else lastBadRotateRunTime = 0;
    //log("lastBadRotateRunTime = %f", lastBadRotateRunTime);
//    log("timeValue=%d, delta=%f", timeValue, delta);
    if(timeValue<=20 && timeValue>10){
        SpriteFrame *frameNormal = SpriteFrameCache::getInstance()->getSpriteFrameByName("ingame_mask_ball_blue_0.png");
        SpriteFrame *frameSpecial = SpriteFrameCache::getInstance()->getSpriteFrameByName("ingame_mask_ball_blue_1.png");
        Sequence *seq = Sequence::create(
                                         CallFunc::create(CC_CALLBACK_0(Sprite::setDisplayFrame, ingameMaskBallSprite, frameSpecial)),
                                         DelayTime::create(0.3),
                                         CallFunc::create(CC_CALLBACK_0(GameLayer::playTimeOutEffect, this)),
                                         DelayTime::create(0.3),
                                         CallFunc::create(CC_CALLBACK_0(GameLayer::playTimeOutEffect, this)),
                                         CallFunc::create(CC_CALLBACK_0(Sprite::setDisplayFrame, ingameMaskBallSprite, frameNormal)),
                                         NULL);
        ingameMaskBallSprite->runAction(seq);
    }else if(timeValue<=10){
        SpriteFrame *frameNormal = SpriteFrameCache::getInstance()->getSpriteFrameByName("ingame_mask_ball_blue_0.png");
        SpriteFrame *frameSpecial = SpriteFrameCache::getInstance()->getSpriteFrameByName("ingame_mask_ball_blue_1.png");
        SpriteFrame *frameSpecial2 = SpriteFrameCache::getInstance()->getSpriteFrameByName("ingame_mask_ball_yellow.png");
        Sequence *seq = Sequence::create(
                                         CallFunc::create(CC_CALLBACK_0(Sprite::setDisplayFrame, ingameMaskBallSprite, frameSpecial)),
                                         CallFunc::create(CC_CALLBACK_0(GameLayer::playTimeOutEffect, this)),
                                         DelayTime::create(0.25),
                                         CallFunc::create(CC_CALLBACK_0(Sprite::setDisplayFrame, ingameMaskBallSprite, frameSpecial2)),
                                         CallFunc::create(CC_CALLBACK_0(GameLayer::playTimeOutEffect, this)),
                                         DelayTime::create(0.25),
                                         CallFunc::create(CC_CALLBACK_0(Sprite::setDisplayFrame, ingameMaskBallSprite, frameSpecial)),
                                         CallFunc::create(CC_CALLBACK_0(GameLayer::playTimeOutEffect, this)),
                                         DelayTime::create(0.25),
                                         CallFunc::create(CC_CALLBACK_0(GameLayer::playTimeOutEffect, this)),
                                         CallFunc::create(CC_CALLBACK_0(Sprite::setDisplayFrame, ingameMaskBallSprite, frameNormal)),
                                         NULL);
        ingameMaskBallSprite->runAction(seq);
    }
    if(timeValue<=0) timeValue=0;
    if(timeValue<60) sprintf(timeString, "%d", timeValue);
    else{
        unsigned int min = timeValue/60;
        unsigned int second = timeValue - min*60;
        sprintf(timeString, "%d:%d", min, second);
    }
    
    float percent = timeValue*1.0/timeTotal;
    Point position = ingameMaskBallSprite->getPosition()-Point(0,5);//animateTimeSprite->getPosition();
    Size size = ingameMaskBallSprite->getContentSize();
    position.y -= 155*(1-percent);
    //animateTimeSprite->setPosition(position);
    MoveTo *mov = MoveTo::create(1, position);
    animateTimeSprite->runAction(mov);//update time by move action
    
    timeLabel->setString(timeString);
    if(timeValue==0){
        if(GameData::getInstance()->isEffectOn())
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("clock.mp3");
        gameOver = true;
        if(curTargetsNum <= 0) doGameWin();
        else doGameFailed();
    }
}

void GameLayer::showTimeIncDecAction(bool inc)
{
    //show time add sprite
    Sprite *timeIncSprite = NULL;
    if(inc) timeIncSprite = Sprite::createWithSpriteFrameName("ingame_incrtime.png");
    else timeIncSprite = Sprite::createWithSpriteFrameName("ingame_punishTime.png");
    timeIncSprite->setPosition(clickedSpritePosition + Point(30,30));
    MoveBy *moveup = MoveBy::create(0.8, Point(0, 30));
    this->addChild(timeIncSprite, 22);
    Sequence *seq = Sequence::create(moveup, CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParentAndCleanup, timeIncSprite, true)),NULL);
    timeIncSprite->runAction(seq);
}

void GameLayer::initRotateSprites()
{
    rotateLayer = Layer::create();
    rotateLayer->setPosition(Point::ZERO);
    unsigned int randValue = rand()%100;
    if(randValue<50) negtive = 1;
    else negtive = -1;
    log("negtive = %d, randValue=%d", negtive, randValue);
    
    //rotateSpriteArray->removeAllObjects();
    rotateSpriteArray0->removeAllObjects();
    rotateSpriteArray1->removeAllObjects();
    rotateSpriteArray2->removeAllObjects();
    rotateSpriteArray3->removeAllObjects();
    rotateSpriteNameArray0->removeAllObjects();
    rotateSpriteNameArray1->removeAllObjects();
    rotateSpriteNameArray2->removeAllObjects();
    rotateSpriteNameArray3->removeAllObjects();
    
    int mode = 1;
    if(gameMode==kGameModeBuffet) mode=1;
    else if(gameMode==kGameModeToyCastle) mode=2;
    else if(gameMode==kGameModeTvShow) mode=3;
    else if(gameMode==kGameModeBazaar) mode=4;
    else if(gameMode==kGameModeEternal) mode=1;//forever
    
    this->addChild(rotateLayer, 1);
    Size winSize = Director::getInstance()->getVisibleSize();
    //int rlen = winSize.height;
    //int rlen = 818;
    r0=950;
    r1=r0+120;
    r2=r1+120;
    r3=r2+125;
    //r0=rlen*17/30, r1=rlen*20/30, r2=rlen*25/30, r3=rlen*28/30;
    char name[1024];
#define PI 3.141592653
    Point o = Point(winSize.width/2, winSize.height/2);
    //unsigned int tCount = 200;//total Items
    Array *partArray0 = Array::create();
    Array *partArray1 = Array::create();
    Array *partArray2 = Array::create();
    Array *partArray3 = Array::create();
    Array *partArray4 = Array::create();
    std::set<std::string> set0;
    std::set<std::string> set1;
    std::set<std::string> set2;
    std::set<std::string> set3;
    std::set<std::string> set4;
    int i=0;
    Array *tmpSpritesArray = Array::create();
    while(i<200){
        std::string tmpstr = ((String*)rotateSpriteArray->objectAtIndex(i))->getCString();
        i++;
        if(set0.find(tmpstr) == set0.end()){
            if(partArray0->count()<41){
                partArray0->addObject(String::create(tmpstr));
                set0.insert(tmpstr);
                continue;
            }
        }
        if(set2.find(tmpstr) == set2.end()){
            if(partArray2->count()<40){
                partArray2->addObject(String::create(tmpstr));
                set2.insert(tmpstr);
                continue;
            }
        }
        if(set4.find(tmpstr) == set4.end()){
            if(partArray4->count()<38){
                partArray4->addObject(String::create(tmpstr));
                set4.insert(tmpstr);
                continue;
            }
        }
        if(set1.find(tmpstr) == set1.end()){
            if(partArray1->count()<40){
                partArray1->addObject(String::create(tmpstr));
                set1.insert(tmpstr);
                continue;
            }
        }
        if(set3.find(tmpstr) == set3.end()){
            if(partArray3->count()<41){
                partArray3->addObject(String::create(tmpstr));
                set3.insert(tmpstr);
                continue;
            }
        }
        tmpSpritesArray->addObject(rotateSpriteArray->objectAtIndex(i-1));
    }
    log("last objects[%d]", tmpSpritesArray->count());
    i = tmpSpritesArray->count()-1;
    for(; i>=0; ){
        if(partArray0->count()<41) partArray0->addObject(tmpSpritesArray->objectAtIndex(i--));
        if(partArray1->count()<40) partArray1->addObject(tmpSpritesArray->objectAtIndex(i--));
        if(partArray2->count()<40) partArray2->addObject(tmpSpritesArray->objectAtIndex(i--));
        if(partArray3->count()<41) partArray3->addObject(tmpSpritesArray->objectAtIndex(i--));
        if(partArray4->count()<38) partArray4->addObject(tmpSpritesArray->objectAtIndex(i--));
    }
    CCASSERT(partArray0->count()==41, "Error num for partArray0");
    CCASSERT(partArray1->count()==40, "Error num for partArray1");
    CCASSERT(partArray2->count()==40, "Error num for partArray2");
    CCASSERT(partArray3->count()==41, "Error num for partArray3");
    CCASSERT(partArray4->count()==38, "Error num for partArray4");
    for(float ang=0; ang<354; ang+=1){
        int randAng=0;
        float offset = -40 + rand()%80;
        float scale = 0.5 + rand()%90/100.0;//0.6 ~ 1.3
        float angOffset = -2.0 + rand()%401/100.0;//-0.1 ~ 0.1
        Array *spritesArray = NULL;
        if(ang>=72*0 && ang<72*1) spritesArray=partArray0;
        else if(ang>=72*1 && ang<72*2) spritesArray=partArray1;
        else if(ang>=72*2 && ang<72*3) spritesArray=partArray2;
        else if(ang>=72*3 && ang<72*4) spritesArray=partArray3;
        else if(ang>=72*4 && ang<72*5) spritesArray=partArray4;
        
        if((int)ang%ANG0 == 0 && 360-ang>=ANG0/2){//0
            unsigned int randIndex = rand()%spritesArray->count();
            sprintf(name, "%s", ((String*)spritesArray->objectAtIndex(randIndex))->getCString());
            spritesArray->removeObjectAtIndex(randIndex);
            /*
            unsigned int randIndex = rand()%tCount;
            tCount--;
            sprintf(name, "%s", ((String*)rotateSpriteArray->objectAtIndex(randIndex))->getCString());
            rotateSpriteArray->removeObjectAtIndex(randIndex);
             */
            Sprite *sprite0 = Sprite::createWithSpriteFrameName(name);
            sprite0->setPosition(o + Point((r0+offset)*cos(PI*(ang+angOffset)/180),(r0+offset)*sin(PI*(ang+angOffset)/180)));
            sprite0->setRotation(randAng);
            sprite0->setScale(scale);
            rotateSpriteArray0->addObject(sprite0);
            rotateSpriteNameArray0->addObject(String::create(name));
            rotateLayer->addChild(sprite0, 2);
        }
        
        if((int)ang%ANG1 == 0 && 360-ang>=ANG1){//1
            unsigned int randIndex = rand()%spritesArray->count();
            sprintf(name, "%s", ((String*)spritesArray->objectAtIndex(randIndex))->getCString());
            spritesArray->removeObjectAtIndex(randIndex);
            /*
            unsigned int randIndex = rand()%tCount;
            tCount--;
            sprintf(name, "%s", ((String*)rotateSpriteArray->objectAtIndex(randIndex))->getCString());
            rotateSpriteArray->removeObjectAtIndex(randIndex);
             */
            Sprite *sprite1 = Sprite::createWithSpriteFrameName(name);
            sprite1->setPosition(o + Point((r1+offset)*cos(PI*(ang+angOffset)/180),(r1+offset)*sin(PI*(ang+angOffset)/180)));
            sprite1->setRotation(randAng);
            sprite1->setScale(scale);
            rotateSpriteArray1->addObject(sprite1);
            rotateSpriteNameArray1->addObject(String::create(name));
            rotateLayer->addChild(sprite1, 2);
        }
        
        if((int)ang%ANG2 == 0 && 360-ang>=ANG2){//2
            unsigned int randIndex = rand()%spritesArray->count();
            sprintf(name, "%s", ((String*)spritesArray->objectAtIndex(randIndex))->getCString());
            spritesArray->removeObjectAtIndex(randIndex);
            /*
            unsigned int randIndex = rand()%tCount;
            tCount--;
            sprintf(name, "%s", ((String*)rotateSpriteArray->objectAtIndex(randIndex))->getCString());
            rotateSpriteArray->removeObjectAtIndex(randIndex);
             */
            Sprite *sprite2 = Sprite::createWithSpriteFrameName(name);
            sprite2->setPosition(o + Point((r2+offset)*cos(PI*(ang+angOffset)/180),(r2+offset)*sin(PI*(ang+angOffset)/180)));
            sprite2->setRotation(randAng);
            sprite2->setScale(scale);
            rotateSpriteArray2->addObject(sprite2);
            rotateSpriteNameArray2->addObject(String::create(name));
            rotateLayer->addChild(sprite2, 2);
        }
        
        if((int)ang%ANG3 == 0 && 360-ang>=ANG3){//3
            unsigned int randIndex = rand()%spritesArray->count();
            sprintf(name, "%s", ((String*)spritesArray->objectAtIndex(randIndex))->getCString());
            spritesArray->removeObjectAtIndex(randIndex);
            /*
            unsigned int randIndex = rand()%tCount;
            tCount--;
            sprintf(name, "%s", ((String*)rotateSpriteArray->objectAtIndex(randIndex))->getCString());
            rotateSpriteArray->removeObjectAtIndex(randIndex);
             */
            Sprite *sprite3 = Sprite::createWithSpriteFrameName(name);
            sprite3->setPosition(o + Point((r3+offset)*cos(PI*(ang+angOffset)/180),(r3+offset)*sin(PI*(ang+angOffset)/180)));
            sprite3->setRotation(randAng);
            sprite3->setScale(scale);
            rotateSpriteArray3->addObject(sprite3);
            rotateSpriteNameArray3->addObject(String::create(name));
            rotateLayer->addChild(sprite3, 2);
        }
    }
    log("arrayNum 0[%d], 1[%d], 2[%d], 3[%d], 4[%d]", partArray0->count(), partArray1->count(), partArray2->count(), partArray3->count(), partArray4->count());
    log("++++++finish add rotate sprites total sprite items[%d]",
        rotateSpriteArray0->count()+rotateSpriteArray1->count()+rotateSpriteArray2->count()+rotateSpriteArray3->count());
    if(!isGuideShow){
        RotateBy *rotate = RotateBy::create(0.4, ROTATE_SPEED*negtive);
        Action *action = RepeatForever::create(rotate);
        rotateLayer->runAction(action);
    }
    rotateLayer->setPosition(Point(0, -810+110-winSize.height/2));
}

GameLayer::~GameLayer()
{
    rotateSpriteArray->release();
    
    rotateSpriteArray0->release();
    rotateSpriteArray1->release();
    rotateSpriteArray2->release();
    rotateSpriteArray3->release();
    
    rotateSpriteNameArray0->release();
    rotateSpriteNameArray1->release();
    rotateSpriteNameArray2->release();
    rotateSpriteNameArray3->release();
    
    currentTargets->release();
    currentTargetNum->release();
}

void GameLayer::onEnterTransitionDidFinish()
{
    log("GameLayer on Enter");
    onAction=false;
    Director::getInstance()->getTouchDispatcher()->addTargetedDelegate(this, 1, true);
    Layer::onEnterTransitionDidFinish();
}

void GameLayer::onExit()
{
    log("GameLayer on Exit");
    Director::getInstance()->getTouchDispatcher()->removeDelegate(this);
    Layer::onExit();
    //SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("bg_ingame_0-hd.plist");
    if(!canRemoveResource) return;
    if(gameMode==kGameModeBuffet){
        SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("bg_ingame_0-hd.plist");
        SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("food-hd.plist");
    }else if(gameMode==kGameModeToyCastle){
        SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("bg_ingame_1-hd.plist");
        SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("toy-hd.plist");
    }else if(gameMode==kGameModeTvShow){
        SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("bg_ingame_2-hd.plist");
        SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("role-hd.plist");
    }else if(gameMode==kGameModeBazaar){
        SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("bg_ingame_3-hd.plist");
        SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("tool-hd.plist");
    }else if(gameMode==kGameModeEternal){
        SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("bg_ingame_sp-hd.plist");
        SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("food-hd.plist");
        SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("toy-hd.plist");
        SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("role-hd.plist");
        SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("tool-hd.plist");
    }else{
        log("ERROR, game Mode ERROR");
    }
}

void GameLayer::touchCallBack()
{
    onAction=false;
}

bool GameLayer::ccTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    if(isGuideShow) return true;
    if(isShowDialog){
        dialogSprite->setVisible(false);
        isShowDialog=false;
    }
    if(currentRunBadTool == kBadToolForbid){
        return true;
    }
    beforeRotatePoint = Director::getInstance()->convertToGL(touch->getLocationInView());
    if(currentRunBadTool == kBadToolBlock){
        Size winSize = Director::getInstance()->getVisibleSize();
        if(beforeRotatePoint.x <winSize.width/2-80 ||
           beforeRotatePoint.x >winSize.width/2+80){
            log("wood block");
            return true;
        }
    }
    rotated = false;
    //if(rotateToolRunning){
        beforeRotateLayerAng = ((int)rotateLayer->getRotation())%360;
        //log("touch Start, beforeRotateAng = %f", beforeRotateLayerAng);
    //}
    return true;
}

void GameLayer::ccTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
    if(isGuideShow) return;
    if(currentRunBadTool == kBadToolForbid){
        //shake the forbit sprite
        return;
    }
    //log("touch Moved");
    int diffAng = 0;
    //if(rotateToolRunning){
        Point afterPoint = Director::getInstance()->convertToGL(touch->getLocationInView());
    if(currentRunBadTool == kBadToolBlock){
        Size winSize = Director::getInstance()->getVisibleSize();
        if(beforeRotatePoint.x <winSize.width/2-80 ||
           beforeRotatePoint.x >winSize.width/2+80){
            log("wood block");
            return;
        }
    }
        Size winSize = Director::getInstance()->getVisibleSize();
        Point o = Point(winSize.width/2, rotateLayer->getPosition().y+winSize.height/2);
        //float ang1 = beforeRotatePoint.getAngle(o);
        //float ang2 = afterPoint.getAngle(o);
        float ang1 = atan2(beforeRotatePoint.y-o.y, beforeRotatePoint.x-o.x)*180/PI;
        float ang2 = atan2(afterPoint.y-o.y, afterPoint.x-o.x)*180/PI;
        diffAng = (ang2-ang1);
        //log("before RotateAng = %f-%d, diffAng = %d", beforeRotateLayerAng, ((int)rotateLayer->getRotation())%360,diffAng);
    if(rotateToolRunning)
        rotateLayer->setRotation(beforeRotateLayerAng - diffAng);
        //log("after RotateAng = %f, diffAng = %d", rotateLayer->getRotation(),diffAng);
    //}
    if(abs(diffAng)>2) rotated = true;
}

void GameLayer::ccTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
    if(isGuideShow){
        if(currentGuid==1){
            isGuideShow=false;
            if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("zoom.mp3");
            FadeOut *fadeOut = FadeOut::create(0.5);
            ScaleTo *scale = ScaleTo::create(0.5, 0.01);
            Sequence *seq = Sequence::create(scale,
                                             CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, guidSprite, true)),
                                             NULL);
            guidSprite->runAction(fadeOut);
            guidSprite->runAction(seq);
            RotateBy *rotate = RotateBy::create(0.4, ROTATE_SPEED*negtive);
            Action *action = RepeatForever::create(rotate);
            rotateLayer->runAction(action);
            this->schedule(schedule_selector(GameLayer::timeOut), 1.0);
            return;
        }else{
            currentGuid=1;
            FadeOut *fadeOut = FadeOut::create(0.5);
            FadeIn *fadeIn = FadeIn::create(0.5);
            ScaleTo *scale = ScaleTo::create(0.5, 0.01);
            Size size = Director::getInstance()->getVisibleSize();
            Size itemSize = guidSprite->getContentSize();
            float sx = size.width/itemSize.width;
            float sy = size.height/itemSize.height;
            ScaleTo *scaleUp = ScaleTo::create(0.5, sx, sy);
            SpriteFrame *frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("guid_3.png");
            Spawn *spawn = Spawn::create(scaleUp, fadeIn, NULL);
            Sequence *seq = Sequence::create(scale,
                                             CallFunc::create(CC_CALLBACK_0(Sprite::setDisplayFrame, guidSprite, frame)),
                                             spawn,
                                             NULL);
            guidSprite->runAction(fadeOut);
            guidSprite->runAction(seq);
        }
        return;
    }
    if(currentRunBadTool == kBadToolForbid){
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("wrong.mp3");
        //click target show shake
        MoveBy *moveLeft = MoveBy::create(0.4, Point(20,0));
        MoveBy *moveRight = MoveBy::create(0.4, Point(-20,0));
        Sequence *seq = Sequence::create(moveLeft, moveRight, NULL);
        Action *action = EaseElasticInOut::create(seq, 0.2);
        forbidUpSprite->runAction(action);
        
        MoveBy *moveLeft1 = MoveBy::create(0.4, Point(20,0));
        MoveBy *moveRight1 = MoveBy::create(0.4, Point(-20,0));
        Sequence *seq1 = Sequence::create(moveLeft1, moveRight1, NULL);
        Action *action1 = EaseElasticInOut::create(seq1, 0.2);
        forbidDownSprite->runAction(action1);
        return;
    }
    if(currentRunBadTool == kBadToolBlock){
        Size winSize = Director::getInstance()->getVisibleSize();
        if(beforeRotatePoint.x <winSize.width/2-80 ||
           beforeRotatePoint.x >winSize.width/2+80){
            log("wood block");
            return;
        }
    }
    if(glassToolRunning){
        log("call glass tool run over");
        glassFindSprite->stopAllActions();
        glassToolRunOver();
    }
    //log("Touch Ended!!!");
    //if(rotateToolRunning && rotated) return;
    if(onAction) {log("onAction can't touch!!"); return;}
    if(rotated){
        if(rotateToolRunning) log("rotate action not clicked");
        else log("ignore not click");
        return;//dismiss this touch
    }
    if(mGameState == kGameRunning){
        Point location;// = Director::getInstance()->convertToGL(touch->getLocationInView());
        location = beforeRotatePoint;
        log("+++++++++++++++++++++++++++++++++++++++++++++++\nstart start location[%f,%f]", location.x, location.y);
        //get ang
        Size winSize = Director::getInstance()->getVisibleSize();
        float angOri = rotateLayer->getRotation();
        Point o;
        o.x = winSize.width/2;
        o.y = rotateLayer->getPosition().y + winSize.height/2;//-winSize.height/2;
        float rlen = sqrtf((location.x-o.x)*(location.x-o.x) + (location.y-o.y)*(location.y-o.y));//-winSize.height/2;//distance
        float ang = atan2f(location.y-o.y, location.x-o.x)*180/PI;
        mlog("angOri[%f]", angOri);
        int n = (ang+angOri)/360;
        float targetAngle = (ang + angOri)-n*360;
        if(targetAngle<0) targetAngle+=360;
        mlog("n[%d], o[%f,%f], r0[%d], r1[%d], r2[%d], r3[%d]", n, o.x, o.y, r0, r1, r2, r3);
        mlog("rlen[%f], ang[%f], angOri[%f], targetAngle[%f]", rlen, ang, angOri, targetAngle);
        
        Sprite *sprite=NULL;
        String *spriteName=NULL;
        /* first get line, then get index at line*/
        if(rlen>= r0-rThreshold && rlen <=r0+rThreshold){
            int num = targetAngle/360;
            targetAngle-=num*360;//[0-360)
            int index1 = targetAngle/ANG0;
            float offset = targetAngle-ANG0*index1;
            if(offset > ANG0/2.0) index1++;
            
            int index = ((int)(targetAngle + ANG0/2)%360)/ANG0;
            if(targetAngle>360-ANG0/2) index1=0;
            log("r0[%d/%d] - index1[%d]", index, rotateSpriteArray0->count(), index1);
            if(index1 >= rotateSpriteArray0->count()) return;//index=rotateSpriteArray0->count()-1;
            sprite = (Sprite*)rotateSpriteArray0->objectAtIndex(index1);
            spriteName = (String*)rotateSpriteNameArray0->objectAtIndex(index1);
            clickedLine = 0;
            clickedIndex = index1;
        } else if(rlen >= r1-rThreshold && rlen <= r1+rThreshold){
            int num = targetAngle/360;
            targetAngle-=num*360;//[0-360)
            int index1 = targetAngle/ANG1;
            float offset = targetAngle-ANG1*index1;
            if(offset > ANG1/2.0) index1++;
            
            int index = ((int)(targetAngle + ANG1/2)%360)/ANG1;
            if(targetAngle>360-ANG1/2) index1=0;
            //if(index%2==1) return;
            //else index/=2;
            log("r1[%d/%d] - index1[%d]", index, rotateSpriteArray1->count(), index1);
            if(index1 >= rotateSpriteArray1->count()) return;//index=rotateSpriteArray1->count()-1;
            sprite = (Sprite*)rotateSpriteArray1->objectAtIndex(index1);
            spriteName = (String*)rotateSpriteNameArray1->objectAtIndex(index1);
            clickedLine = 1;
            clickedIndex = index1;
        } else if(rlen >= r2-rThreshold && rlen <= r2+rThreshold){
            int num = targetAngle/360;
            targetAngle-=num*360;//[0-360)
            int index1 = targetAngle/ANG2;
            float offset = targetAngle-ANG2*index1;
            if(offset > ANG2/2.0) index1++;
            //if(index1 == rotateSpriteArray2->count()) index1=0;
            
            int index = ((int)(targetAngle + ANG2/2)%360)/ANG2;
            if(targetAngle>360-ANG2/2) index1=0;
            log("r2[%d/%d] - index1[%d]", index, rotateSpriteArray2->count(), index1);
            //if(index%2==1) return;
            //else index/=2;
            if(index1 >= rotateSpriteArray2->count()) return;//index=rotateSpriteArray2->count()-1;
            sprite = (Sprite*)rotateSpriteArray2->objectAtIndex(index1);
            spriteName = (String*)rotateSpriteNameArray2->objectAtIndex(index1);
            clickedLine = 2;
            clickedIndex = index1;
        } else if(rlen >= r3-rThreshold && rlen <= r3+rThreshold){
            int num = targetAngle/360;
            targetAngle-=num*360;//[0-360)
            int index1 = targetAngle/ANG3;
            float offset = targetAngle-ANG3*index1;
            if(offset > ANG3/2.0) index1++;
            log("target Angle = %f, offset = %f, index1=%d", targetAngle, offset, index1);
            
            int index = ((int)(targetAngle + ANG3/2)%360)/ANG3;
            if(targetAngle>360-ANG3/2) index1=0;
            //if(index%2==1) return;
            //else index/=2;
            log("r3[%d/%d] - index1[%d]", index, rotateSpriteArray3->count(), index1);
            if(index1 >= rotateSpriteArray3->count()) return;//index=rotateSpriteArray3->count()-1;
            sprite = (Sprite*)rotateSpriteArray3->objectAtIndex(index1);
            spriteName = (String*)rotateSpriteNameArray3->objectAtIndex(index1);
            clickedLine = 3;
            clickedIndex = index1;
        } else {
            log("not click anybody");
            return;
        }
        if(sprite){//if sprite exist, correct clicked!!
            log("---------Click Sprite[%s]", spriteName->getCString());
            GameData::getInstance()->updateHandBookItem(spriteName->getCString());
            onAction=true;
            clickedSpritePosition = location;
            clickTarget(sprite ,spriteName->getCString());
        }
        return;
    }
}

void GameLayer::resetClean()
{
    inCleaning = false;
}

void GameLayer::buttonCallback(Object* pSender)
{
    if(isGuideShow) return;
    if(isShowDialog){
        dialogSprite->setVisible(false);
        isShowDialog=false;
        return;
    }
    if(pSender == pauseMenuItemSprite){//pause the game
        if(mGameState == kGameOver) return;
        log("gamePaused");
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("pause.mp3");
        
        //update num
        char numstr[16];
        sprintf(numstr, "%d", GameData::getInstance()->getToolNum(GameData::ToolType::kToolClean));
        pauseCleanNumLabel->setString(numstr);
        sprintf(numstr, "%d", GameData::getInstance()->getToolNum(GameData::ToolType::kToolClock));
        pauseClockNumLabel->setString(numstr);
        sprintf(numstr, "%d", GameData::getInstance()->getToolNum(GameData::ToolType::kToolGlass));
        pauseGlassNumLabel->setString(numstr);
        sprintf(numstr, "%d", GameData::getInstance()->getToolNum(GameData::ToolType::kToolRotate));
        pauseRotateNumLabel->setString(numstr);
        
        Director::getInstance()->pause();
        CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
        CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
        //rotateLayer->stopAllActions();
        //stop time
        //this->unschedule(schedule_selector(GameLayer::timeOut));
        
        bgPauseSprite->setVisible(true);
		pauseCleanSprite->setVisible(true);
		pauseClockSprite->setVisible(true);
		pauseGlassSprite->setVisible(true);
		pauseRotateSprite->setVisible(true);
        mGameState = kGamePaused;
        menuIngame->setEnabled(false);
        menuPause->setVisible(true);
        menuPause->setEnabled(true);
        stageSprite->setVisible(true);
        stageInfoLabel->setVisible(true);
        return;
    }else if(pSender == menuSelectButton || pSender == gameScoreSelectButton || pSender == gameOverSelectButton){//return to Game StagetMenuSub
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        log("to Stage SelectMenu");
        Director::getInstance()->resume();
        CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
        CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
        CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
        Scene *scene = NULL;
        if(gameMode != kGameModeEternal) scene = StageMenuSub::scene();
        else scene = StartLayer::scene();
        TransitionFade *fade = TransitionFade::create(0.3, scene, Color3B::BLACK);
        Director::getInstance()->replaceScene(fade);
    }else if(pSender == menuAgainButton || pSender == gameScoreAgainButton || pSender == gameOverAgainButton){//run a new game
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        Director::getInstance()->resume();
        CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
        CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
        log("again");
        canRemoveResource=false;
        //reset timevalue
        Scene *scene = GameLayer::scene();
        TransitionFade *fade = TransitionFade::create(1, scene, Color3B::BLACK);
        Director::getInstance()->replaceScene(fade);
    }else if(pSender == menuResumeButton){//resume current game
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("pause.mp3");
        log("resume");
        menuIngame->setEnabled(true);
        menuPause->setVisible(false);
        menuPause->setEnabled(false);
        mGameState = kGameRunning;
        bgPauseSprite->setVisible(false);
        pauseCleanSprite->setVisible(false);
		pauseClockSprite->setVisible(false);
		pauseGlassSprite->setVisible(false);
		pauseRotateSprite->setVisible(false);
        stageSprite->setVisible(false);
        stageInfoLabel->setVisible(false);
        Director::getInstance()->resume();
        CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
        CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
    }else if(pSender == gameScoreNextButton){
        int stage = GameData::getInstance()->currentStage;
        int subStage = GameData::getInstance()->currentSubStage;
        if(subStage<15) subStage++;
        else if(stage<4){
            stage++;
            subStage=1;
        }
        log("-------------> to stage[%d]-[%d]", stage, subStage);
        CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
        GameData::getInstance()->setCurrentGame(stage, subStage);
        canRemoveResource=false;
        //reset timevalue
        Scene *scene = GameLayer::scene();
        TransitionFade *fade = TransitionFade::create(1, scene, Color3B::BLACK);
        Director::getInstance()->replaceScene(fade);
    }else if(pSender == buyClean || pSender == buyClock || pSender == buyGlass || pSender == buyRotate){
        unsigned int price = -1;
        GameData::ToolType toolType;
        if(pSender==buyClean){
            toolType = GameData::ToolType::kToolClean;
            price = GameData::getInstance()->getToolPrice(3);
        }else if(pSender==buyClock){
            toolType = GameData::ToolType::kToolClock;
            price = GameData::getInstance()->getToolPrice(2);
        }else if(pSender==buyGlass){
            toolType = GameData::ToolType::kToolGlass;
            price = GameData::getInstance()->getToolPrice(1);
        }else if(pSender==buyRotate){
            toolType = GameData::ToolType::kToolRotate;
            price = GameData::getInstance()->getToolPrice(4);
        }
        if(GameData::getInstance()->useCoinNum(price)){
            log("buy OK[%d]", toolType);
            if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("coin.mp3");
            GameData::getInstance()->addToolNum(toolType, 1);
            updateToolNum();//update tool num show in game
            //update tool num on pause menu
            char numstr[16];
            sprintf(numstr, "%d", GameData::getInstance()->getToolNum(GameData::ToolType::kToolClean));
            pauseCleanNumLabel->setString(numstr);
            sprintf(numstr, "%d", GameData::getInstance()->getToolNum(GameData::ToolType::kToolClock));
            pauseClockNumLabel->setString(numstr);
            sprintf(numstr, "%d", GameData::getInstance()->getToolNum(GameData::ToolType::kToolGlass));
            pauseGlassNumLabel->setString(numstr);
            sprintf(numstr, "%d", GameData::getInstance()->getToolNum(GameData::ToolType::kToolRotate));
            pauseRotateNumLabel->setString(numstr);
        }else{
            log("no enough coins");//need show dialog
            dialogSprite->setVisible(true);
            isShowDialog = true;
        }
        updateCoin();
    }else{
        log("tools");
        MoveBy *moveLeft = MoveBy::create(0.4, Point(5,0));
        MoveBy *moveRight = MoveBy::create(0.4, Point(-5,0));
        Sequence *seq2 = Sequence::create(moveLeft, moveRight, NULL);
        Action *action = EaseElasticInOut::create(seq2, 0.2);
        if(pSender == ingameClockSprite){
            log("Tool clock");
            if(GameData::getInstance()->useTool(GameData::kToolClock)){
                updateToolNum();
                if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("clock.mp3");
                unsigned int level = GameData::getInstance()->getSkillLevel(2);
                unsigned int addTime = GameData::getInstance()->getSkillValue(2, level);
                log("tool add time[%d]", addTime);
                timeValue+=addTime;//TODO value depends on level
                if(timeValue > timeTotal) timeTotal = timeValue;
                updateTime();
            }else{
                if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("wrong.mp3");
                ingameClockSprite->runAction(action);
            }
        }else if(pSender == ingameGlassSprite){
            log("Tool Glass, glassToolRuning = %d", glassToolRunning);
            if(glassToolRunning || onAction){
                if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("wrong.mp3");
                ingameGlassSprite->runAction(action);
                return;
            }
            /*
            if(currentRunBadTool == kBadToolAntiRotate || currentRunBadTool == kBadToolSlowRotation){
                if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("wrong.mp3");
                ingameGlassSprite->runAction(action);
                return;
            }*/
            if(GameData::getInstance()->useTool(GameData::kToolGlass)){
                updateToolNum();
                glassToolRunning = true;
                int i=0;
                if(currentTargets->count()<=3) i=rand()%(currentTargets->count());
                else i=rand()%3;
                String *str = (String*)currentTargets->objectAtIndex(i);
                log("select target[%d]-[%s]", i, str->getCString());
                Array *arrName = NULL;
                Array *arrSprite = NULL;
                int line=-1;
                int index=-1;
                for(line=0; line<4; line++){//try find it
                    if(line==0){
                        arrName = rotateSpriteNameArray0;
                        arrSprite = rotateSpriteArray0;
                    }else if(line==1){
                        arrName = rotateSpriteNameArray1;
                        arrSprite = rotateSpriteArray1;
                    }else if(line==2){
                        arrName = rotateSpriteNameArray2;
                        arrSprite = rotateSpriteArray2;
                    }else if(line==3){
                        arrName = rotateSpriteNameArray3;
                        arrSprite = rotateSpriteArray3;
                    }else{
                        CCASSERT(true, "ERROR, never run here");
                    }
                    for(index=0; index<arrName->count(); index++){
                        if(strcmp(str->getCString(), ((String*)arrName->objectAtIndex(index))->getCString())==0)
                            break;
                    }
                    if(index<arrName->count()) break;
                    else {
                        if(line<4) continue;
                        else{
                            log("ERROR, not found");
                            CCASSERT(true, "ERROR, never run here, not found the sprite");
                            return;
                        }
                    }
                }
                log("line=%d,",line);
                CCASSERT(line!=4, "ERROR, not found it");
                log("select sprite at[%d]-[%d]-[%s]", line, index, ((String*)arrName->objectAtIndex(index))->getCString());
                Size winSize = Director::getInstance()->getVisibleSize();
                Point o = Point(winSize.width/2, winSize.height/2);
                Sprite *sprite = (Sprite*)arrSprite->objectAtIndex(index);
                glassFindName = (String*)arrName->objectAtIndex(index);
                Point p = sprite->getPosition();
                rotateLayer->setRotation((int)rotateLayer->getRotation()%360);
                float ang = atan2f(p.y-o.y, p.x-o.x)*180/PI;
                float targetAng = 0;
                if(ang>0) targetAng = 90-ang;
                else targetAng = 90 - ang;
                log("ang = %f, targetAng=%f, currentRotate = %f", ang, targetAng, rotateLayer->getRotation());
                //rotateLayer->setRotation(-ang);
                RotateTo *rotateAction = RotateTo::create(0.3, -targetAng);
                log("afterRotate = %f", rotateLayer->getRotation());
                //ScaleBy *scaleUp = ScaleBy::create(0.2, 2);
                
                rotateLayer->stopAllActions();
                rotateLayer->runAction(rotateAction);
                if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("zoom.mp3");
                glassFindSprite = sprite;
                Sequence *seq = Sequence::create(//DelayTime::create(0.3),
                                                 /*scaleUp,*/ DelayTime::create(3.1),
                                                 CallFunc::create(CC_CALLBACK_0(GameLayer::glassToolRunOver, this)),
                                                 NULL);
                glassFindSprite->runAction(seq);
            }else{
                if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("wrong.mp3");
                if(ingameGlassSprite->isRunning()) return;
                ingameGlassSprite->runAction(action);
            }
        }else if(pSender == ingameRotateSprite){
            log("Tool Rotate");
            if(rotateToolRunning){
                if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("wrong.mp3");
                ingameRotateSprite->runAction(action);
                return;//can't run two at once
            }
            /*
            if(currentRunBadTool == kBadToolAntiRotate || currentRunBadTool == kBadToolSlowRotation){//can't run rotateTool with anti-rotate
                if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("wrong.mp3");
                ingameRotateSprite->runAction(action);
                return;//can't run two at once
            }*/
            if(GameData::getInstance()->useTool(GameData::kToolRotate)){
                rotateToolRunning = true;
                updateToolNum();
                rotateLayer->stopAllActions();
                unsigned int level = GameData::getInstance()->getSkillLevel(3);
                unsigned int time = GameData::getInstance()->getSkillValue(3, level);
                log("+++ skill rotate free for time[%d]", time);
                Sequence *seq = Sequence::create(DelayTime::create(time), CallFunc::create(CC_CALLBACK_0(GameLayer::rotateToolRunOver, this)),NULL);
                if(lastBadRotateRunTime<time &&
                   (currentRunBadTool==kBadToolAntiRotate || currentRunBadTool==kBadToolSlowRotation) &&
                   currentBadActionSprite){
                    log("only delete the bad action sprite file");
                    Sequence *seq = Sequence::create(DelayTime::create(lastBadRotateRunTime),
                                                     CallFunc::create(CC_CALLBACK_0(GameLayer::badToolActionOver, this, currentBadActionSprite, currentRunBadTool)),
                                                     NULL);
                    currentBadActionSprite->runAction(seq);
                }
                this->runAction(seq);
            }else{
                if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("wrong.mp3");
                ingameRotateSprite->runAction(action);
            }
        }else if(pSender == ingameCleanSprite){
            log("Tool clean");
            //if(currentRunBadTool == kBadToolInvalid) {log("no need run this tool");return;}
            if(inCleaning || blockStartClean) return;
            if(!badToolRunning || currentRunBadTool == kBadToolInvalid) {log("no need run this tool");
                if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("wrong.mp3");
                ingameCleanSprite->runAction(action);
                return;
            }
            //currentBadActionSprite->removeFromParentAndCleanup(true);
            RotateBy *rotate = RotateBy::create(0.4, ROTATE_SPEED*negtive);
            Action *actionRotate = RepeatForever::create(rotate);
            
            //if(onAction || currentRunBadTool!= kBadToolInvalid) return;
            if(GameData::getInstance()->useTool(GameData::kToolClean)){
                inCleaning = true;
                FadeOut *fade = FadeOut::create(0.8);
                Sequence *seq = Sequence::create(fade,
                                                 CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, currentBadActionSprite, true)),
                                                 CallFunc::create(CC_CALLBACK_0(GameLayer::resetClean, this)),
                                                 NULL);
                currentBadActionSprite->runAction(seq);
                if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("cleaner.mp3");
                updateToolNum();
                switch (currentRunBadTool) {
                    case kBadToolWater:
                        currentRunBadTool = kBadToolInvalid;
                        waterSprite->stopAllActions();
                        waterSprite->removeFromParentAndCleanup(true);
                        if(GameData::getInstance()->isEffectOn())
                            CocosDenshion::SimpleAudioEngine::getInstance()->stopEffect(effectIndex);
                        break;
                    case kBadToolBlock:
                        clearBlock();
                        break;
                    case kBadToolPollute:
                        currentRunBadTool = kBadToolInvalid;
                        polluteToRightSprite->stopAllActions();
                        polluteToRightSprite->removeFromParentAndCleanup(true);
                        polluteToLeftSprite->stopAllActions();
                        polluteToLeftSprite->removeFromParentAndCleanup(true);
                        polluteToRightSprite2->stopAllActions();
                        polluteToRightSprite2->removeFromParentAndCleanup(true);
                        polluteToLeftSprite2->stopAllActions();
                        polluteToLeftSprite2->removeFromParentAndCleanup(true);
                        break;
                    case kBadToolForbid:
                        currentRunBadTool = kBadToolInvalid;
                        forbidUpSprite->stopAllActions();
                        forbidUpSprite->removeFromParentAndCleanup(true);
                        forbidDownSprite->removeFromParentAndCleanup(true);
                        break;
                    case kBadToolAntiRotate:
                        currentRunBadTool = kBadToolInvalid;
                        rotateLayer->stopAllActions();
                        rotateLayer->runAction(actionRotate);
                    case kBadToolSlowRotation:
                        currentRunBadTool = kBadToolInvalid;
                        rotateLayer->stopAllActions();
                        rotateLayer->runAction(actionRotate);
                        break;
                        
                    default:
                        break;
                }
            }else{
                if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("wrong.mp3");
                ingameCleanSprite->runAction(action);
            }
        }
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("tool.mp3");
    }
}

void GameLayer::rotateToolRunOver()
{
    float speed = ROTATE_SPEED*negtive;
    float time = lastBadRotateRunTime;
    if(time>=0 &&(currentRunBadTool==kBadToolAntiRotate || currentRunBadTool==kBadToolSlowRotation)){
        log("bad tool not over, keep running[%f]", time);
        if(time==0){
            badToolActionOver(currentBadActionSprite, currentRunBadTool);
        }
        if(currentRunBadTool == kBadToolAntiRotate)//add speed
            speed = speed*2;
        if(currentRunBadTool == kBadToolSlowRotation)//dec speed
            speed = speed/2;
        RotateBy *rotate = RotateBy::create(0.4, speed);
        Action *action = RepeatForever::create(rotate);
        Sequence *seq = Sequence::create(DelayTime::create(time),
                                         CallFunc::create(CC_CALLBACK_0(GameLayer::badToolActionOver, this, currentBadActionSprite, currentRunBadTool)),
                                         NULL);
        currentBadActionSprite->runAction(seq);
        rotateLayer->runAction(action);
        rotateToolRunning = false;
        return;
    }
    rotateLayer->stopAllActions();
    rotateToolRunning = false;
    log("set rotate tool running false");
    RotateBy *rotate = RotateBy::create(0.4, ROTATE_SPEED*negtive);
    Action *action = RepeatForever::create(rotate);
    rotateLayer->runAction(action);
}

void GameLayer::glassToolRunOver()
{
    log("try glass Tool run over");
    if(glassToolRunning == false) return;
    log("really glass Tool run over");
    //glassFindSprite->stopAllActions();
    glassToolRunning = false;
    //ScaleBy *scaleDown = ScaleBy::create(0.2, 0.5);
    //glassFindSprite->runAction(scaleDown);
    glassFindSprite=NULL;
    if(rotateToolRunning) {log("no need recover layer rotate"); return;}
    float speed = ROTATE_SPEED*negtive;
    float time = lastBadRotateRunTime;
    if(time>=0 &&(currentRunBadTool==kBadToolAntiRotate || currentRunBadTool==kBadToolSlowRotation)){
        log("bad tool not over, keep running[%f]", time);
        if(time==0){
            badToolActionOver(currentBadActionSprite, currentRunBadTool);
        }
        if(currentRunBadTool == kBadToolAntiRotate)//add speed
            speed = speed*2;
        if(currentRunBadTool == kBadToolSlowRotation)//dec speed
            speed = speed/2;
        RotateBy *rotate = RotateBy::create(0.4, speed);
        Action *action = RepeatForever::create(rotate);
        Sequence *seq = Sequence::create(DelayTime::create(time),
                                         CallFunc::create(CC_CALLBACK_0(GameLayer::badToolActionOver, this, currentBadActionSprite, currentRunBadTool)),
                                         NULL);
        currentBadActionSprite->runAction(seq);
        rotateLayer->runAction(action);
        return;
    }
    RotateBy *rotate = RotateBy::create(0.4, speed);
    Action *action = RepeatForever::create(rotate);
    rotateLayer->runAction(action);
}

void GameLayer::doGameWin()
{
    gameOver = true;
    fadeLayer->setVisible(true);
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    if(effectIndex!=-1){
        CocosDenshion::SimpleAudioEngine::getInstance()->stopEffect(effectIndex);
        effectIndex = -1;
    }
    mGameState = kGameOver;
    this->unschedule(schedule_selector(GameLayer::timeOut));
    rotateLayer->stopAllActions();
    menuIngame->setEnabled(false);
    
    if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("victory.mp3");
    log("do GameWin!!");
    //mlog("error clicked times[%d]", errorClickTimes);
    int starNum=0;
    if(errorClickTimes > 3) {starNum=1;/*log("Has error click: STAR x 1");*/}
    else{
        if(timeValue < 20) {
            starNum=2;/*log("time small the 20: STAR x 2");*/
        }
        else {starNum=3;/*log("time bigger than 20: STAR x 3");*/}
    }
    GameData::getInstance()->updateCurrentStageStar(starNum);
    
    //get next stage is locked?
    bool locked = GameData::getInstance()->isNextStageLocked();
    
    Size size = Director::getInstance()->getWinSize();
    Sprite *scorePanelSprite = Sprite::createWithSpriteFrameName("ingame_grade_frame.png");
    scorePanelSprite->setScale(0.01);
    scorePanelSprite->setPosition(Point(size.width/2, size.height/2 + 150));
    ScaleTo *scale = ScaleTo::create(0.5, 1);
    EaseBackOut *ease = EaseBackOut::create(scale);
    scorePanelSprite->runAction(ease);
    
    Size itemSize = scorePanelSprite->getContentSize();//here for coin get
    Sprite *scoreCoinSprite = Sprite::createWithSpriteFrameName("ingame_grade_frame_bottom.png");
    //scoreCoinSprite->setScale(0.5);
    scoreCoinSprite->setVisible(false);
    scoreCoinSprite->setPosition(Point(size.width/2, size.height/2 - itemSize.height/2 + 148));
    
    MoveBy *move = MoveBy::create(0.2, Point(0,-20.0));
    Sequence *seq = Sequence::create(DelayTime::create(0.5),
                                     CallFunc::create(CC_CALLBACK_0(Node::setVisible,scoreCoinSprite, true)),
                                     move,
                                     NULL);
    scoreCoinSprite->runAction(seq);
    
    char coinNumString[64];
    log("targetNum[%d], targetTypes[%d], timeValue[%d]", rightTargetNum, rightTargetTypeNum, timeValue);
    currentStageGetCoin = (1*(rightTargetNum*2)+rightTargetTypeNum*3+timeValue*2)/3;
    sprintf(coinNumString, "+%d", currentStageGetCoin);
    GameData::getInstance()->addCoinNum(currentStageGetCoin);
    updateCoin();
    LabelBMFont *currentStageCoinNumLabel = LabelBMFont::create(coinNumString, "number_gold.fnt");
    Size coinBottomSize = scoreCoinSprite->getContentSize();
    currentStageCoinNumLabel->setPosition(Point(coinBottomSize.width/2 + 33, coinBottomSize.height/2+33));
    scoreCoinSprite->addChild(currentStageCoinNumLabel);
    
    //Menu and buttons
    gameScoreSelectButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ingame_btn_menu.png"),
            Sprite::createWithSpriteFrameName("ingame_btn_menu_selected.png"),
            CC_CALLBACK_1(GameLayer::buttonCallback, this));
    //gameScoreSelectButton->setScale(0.5);
    gameScoreSelectButton->setPosition(Point(size.width/2 - size.width/7, size.height/2 - itemSize.height/2 + 148));
    //gameScoreSelectButton->setVisible(false);
    
    gameScoreAgainButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ingame_btn_again.png"),
            Sprite::createWithSpriteFrameName("ingame_btn_again_selected.png"),
            CC_CALLBACK_1(GameLayer::buttonCallback, this));
    //gameScoreAgainButton->setScale(0.5);
    gameScoreAgainButton->setPosition(Point(size.width/2,size.height/2 - itemSize.height/2 + 148));
    //gameScoreAgainButton->setVisible(false);
    
    gameScoreNextButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ingame_btn_nextStage.png"),
            Sprite::createWithSpriteFrameName("ingame_btn_nextStage_selected.png"),
            CC_CALLBACK_1(GameLayer::buttonCallback, this));
    //gameScoreNextButton->setScale(0.5);
    gameScoreNextButton->setPosition(Point(size.width/2 + size.width/7, size.height/2 - itemSize.height/2 + 148));
    
    if(locked) gameScoreNextButton->setVisible(false);
    else gameScoreNextButton->setVisible(true);
    
    if(GameData::getInstance()->currentStage==4 && GameData::getInstance()->currentSubStage==15){
        gameScoreMenu = Menu::create(gameScoreSelectButton,
                                     gameScoreAgainButton,
                                     NULL);
    }else{
        gameScoreMenu = Menu::create(gameScoreSelectButton,
                                     gameScoreAgainButton,
                                     gameScoreNextButton,
                                     NULL);
    }
    gameScoreMenu->setPosition(Point::ZERO);
    gameScoreMenu->setVisible(false);
    this->addChild(gameScoreMenu, 30);
    
    MoveBy *move2 = MoveBy::create(0.2, Point(0,-115.0));
    Sequence *seq2 = Sequence::create(DelayTime::create(0.5),
                                     CallFunc::create(CC_CALLBACK_0(Node::setVisible,gameScoreMenu, true)),
                                     move2,
                                     NULL);
    gameScoreMenu->runAction(seq2);
    
    this->addChild(scoreCoinSprite, 30);
    this->addChild(scorePanelSprite, 30);
    
    //stars
    for(int i=0; i<starNum; i++){
        Sprite *sprite = Sprite::createWithSpriteFrameName("ingame_grade_star.png");
        sprite->setScale(0.9*1);
        sprite->setVisible(false);
        sprite->setPosition(Point(size.width/2 + (i-1)*150, size.height/2 - itemSize.height/2 + 270 + abs(i-1)*30));
        ScaleTo *scale = ScaleTo::create(0.2, 1);
        Sequence *mSeq = Sequence::create(DelayTime::create(0.5+i*0.3),
                                          CallFunc::create(CC_CALLBACK_0(Node::setVisible, sprite, true)),
                                          CallFunc::create(CC_CALLBACK_0(GameLayer::starEnd, this)),
                                          scale, NULL);
        sprite->runAction(mSeq);
        this->addChild(sprite, 30);
    }
}

void GameLayer::doGameFailed()
{
    fadeLayer->setVisible(true);
    if(effectIndex!=-1){
        CocosDenshion::SimpleAudioEngine::getInstance()->stopEffect(effectIndex);
        effectIndex = -1;
    }
    mGameState = kGameOver;
    log("do GameFailed!!");
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    rotateLayer->stopAllActions();
    
    if(GameData::getInstance()->isEffectOn()){
        if(gameMode != kGameModeEternal) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("lost.mp3");
        else CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("clock.mp3");
    }
    Size size = Director::getInstance()->getVisibleSize();
    gameOverFrame = Sprite::createWithSpriteFrameName("ingame_lose_frame.png");
    gameOverFrame->setPosition(Point(size.width/2, size.height/2 + 100));
    //gameOverFrame->setScale(0.5);
    
    if(gameMode == kGameModeEternal){//add one coin sprite
        char coinNumString[64];
        currentStageGetCoin = (rightTargetNum*2+rightTargetTypeNum*3+lastTime*2)/3;
        log("lastTime=%d, items[%d], types[%d], coinNum=%d", lastTime, rightTargetNum, rightTargetTypeNum, currentStageGetCoin);
        sprintf(coinNumString, "+%d", currentStageGetCoin);
        Sprite *scoreCoinSprite = Sprite::createWithSpriteFrameName("ingame_grade_frame_bottom.png");
        //scoreCoinSprite->setScale(0.5);
        Size itemSize = gameOverFrame->getContentSize();
        Size mSize = scoreCoinSprite->getContentSize();
        scoreCoinSprite->setVisible(true);
        scoreCoinSprite->setPosition(Point(itemSize.width/2, -mSize.height/2+40));
        gameOverFrame->addChild(scoreCoinSprite, -1);

        LabelBMFont *currentStageCoinNumLabel = LabelBMFont::create(coinNumString, "number_gold.fnt");
        Size coinBottomSize = scoreCoinSprite->getContentSize();
        currentStageCoinNumLabel->setPosition(Point(coinBottomSize.width/2 + 33, coinBottomSize.height/2+33));
        scoreCoinSprite->addChild(currentStageCoinNumLabel);
    }
    Sprite *failedSPrite = NULL;
    if(gameMode != kGameModeEternal) failedSPrite = Sprite::createWithSpriteFrameName("ingame_font_lose.png");
    else failedSPrite = Sprite::createWithSpriteFrameName("ingame_font_finish.png");
    //failedSPrite->setScale(0.5);
    failedSPrite->setPosition(gameOverFrame->getPosition());
    this->addChild(gameOverFrame, 30);
    this->addChild(failedSPrite, 30);
    
    gameOverSelectButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ingame_btn_menu.png"),
                                                  Sprite::createWithSpriteFrameName("ingame_btn_menu_selected.png"),
                                                  CC_CALLBACK_1(GameLayer::buttonCallback, this));
    //gameOverSelectButton->setScale(0.5);
    gameOverSelectButton->setPosition(Point(size.width/2 - size.width/9, size.height/4 + 140));
    //gameScoreSelectButton->setVisible(false);
    
    gameOverAgainButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ingame_btn_again.png"),
            Sprite::createWithSpriteFrameName("ingame_btn_again_selected.png"),
            CC_CALLBACK_1(GameLayer::buttonCallback, this));
    //gameOverAgainButton->setScale(0.5);
    gameOverAgainButton->setPosition(Point(size.width/2 + size.width/9,size.height/4 + 140));
    //gameScoreAgainButton->setVisible(false);
    
    gameOverMenu = Menu::create(gameOverSelectButton,
                                gameOverAgainButton,
                                NULL);
    gameOverMenu->setPosition(Point::ZERO);
    this->addChild(gameOverMenu, 30);
}

void GameLayer::starEnd()
{
    mGameState = kGameOver;
    if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("star.mp3");
}

void GameLayer::initCoinBMFont()
{
    Size size = ingameGoldFrameSprite->getContentSize();
    coinBMFont = LabelBMFont::create("", "number_gold.fnt");
    //coinBMFont = LabelBMFont::create("123", "number_gold.fnt", Label::HAlignment::CENTER);
    coinBMFont->setPosition(Point(size.width/2+23, size.height/2+33));
    ingameGoldFrameSprite->addChild(coinBMFont);
    updateCoin();
    currentStageGetCoin = 0;
}

void GameLayer::updateCoin()
{
    char name[25];
    unsigned int coinNum = GameData::getInstance()->getCoinNums();
    sprintf(name, "%d", coinNum);
    coinBMFont->setString(name);
}

