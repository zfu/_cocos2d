//
//  StageSelectLayer.cpp
//  notfound
//
//  Created by 付卓 on 13-11-5.
//
//

#include "StageSelectLayer.h"
#include "SimpleAudioEngine.h"
#include "GuidLayer.h"
#include "StartLayer.h"
#include "StageMenuSub.h"
#include "SimpleAudioEngine.h"
#include "GameLayer.h"
#include "GameData.h"

#define PI 3.14159265
#define ANG 45
#define TIME 0.4

Scene* StageSelectLayer::scene()
{
    Scene *scene = Scene::create();
    StageSelectLayer *layer = StageSelectLayer::create();
    scene->addChild(layer);
    return scene;
}

bool StageSelectLayer::init()
{
    //SpriteFrameCache::getInstance()->addSpriteFramesWithFile("beding-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("stageBack-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("stageBg-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("stageLogo-hd.plist");
    
    TempData *tmpData = GameData::getInstance()->tmpData;
    if(tmpData->gameLayerType == TempData::kGameTypeInvalid ||
       tmpData->gameLayerType == TempData::kGameEternal)
        tmpData->gameLayerType = TempData::kGameTypeBuffet;
    if(tmpData->gameLayerType == TempData::kGameTypeBuffet){
        bg = Sprite::createWithSpriteFrameName("stageBack_buffet.png");
        bg2 = Sprite::createWithSpriteFrameName("stageBack_buffet.png");
    }else if(tmpData->gameLayerType == TempData::kGameTypeToyCastle){
        bg = Sprite::createWithSpriteFrameName("stageBack_toyCastle.png");
        bg2 = Sprite::createWithSpriteFrameName("stageBack_toyCastle.png");
    }else if(tmpData->gameLayerType == TempData::kGameTypeTvShow){
        bg = Sprite::createWithSpriteFrameName("stageBack_tvShow.png");
        bg2 = Sprite::createWithSpriteFrameName("stageBack_tvShow.png");
    }else if(tmpData->gameLayerType == TempData::kGameTypeBazaar){
        bg = Sprite::createWithSpriteFrameName("stageBack_bazaar.png");
        bg2 = Sprite::createWithSpriteFrameName("stageBack_bazaar.png");
    }
    this->addChild(bg2);
    this->addChild(bg);
    Size size = Director::getInstance()->getVisibleSize();
    bg->setPosition(Point(size.width/2, size.height/2));
    bg2->setPosition(Point(size.width/2, size.height/2));
    Size bgSize = bg->getContentSize();
    float scale = (size.width/bgSize.width)>(size.height/bgSize.height)?(size.width/bgSize.width):(size.height/bgSize.height);
    log("scale = %f", scale);
    bg->setScale(scale);
    bg2->setScale(scale);
    
    initModeSprite();
    
    //for next and prev
    nextItemSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("being_all.png"), Sprite::createWithSpriteFrameName("being_all_selected.png"), CC_CALLBACK_1(StageSelectLayer::buttonCallBack, this));
    //nextItemSprite->setScale(0.5);
	nextItemSprite->setPosition(size.width*5/6, size.height/6);
    nextItemSprite->setVisible(false);
    
    prevItemSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("being_all.png"), Sprite::createWithSpriteFrameName("being_all_selected.png"), CC_CALLBACK_1(StageSelectLayer::buttonCallBack, this));
    //prevItemSprite->setScale(0.5);
    prevItemSprite->setRotation(-180.0);
	prevItemSprite->setPosition(size.width*1/6, size.height/6);
    prevItemSprite->setVisible(false);
    
    returnSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("being_return2.png"), Sprite::createWithSpriteFrameName("being_return2_selected.png"), CC_CALLBACK_1(StageSelectLayer::buttonCallBack, this));
    Size spriteSize = returnSprite->getContentSize();
    //returnSprite->setScale(0.5);
    returnSprite->setPosition(size.width - spriteSize.width/2 - 5, size.height - spriteSize.height/2 - 5);
    
    Menu* menu = Menu::create(nextItemSprite, prevItemSprite, returnSprite/*, guideItemSprite*/, NULL);
    menu->setPosition(Point::ZERO);
    this->addChild(menu);
    
    return true;
}

void StageSelectLayer::initModeSprite()
{
    TempData *tmpData = GameData::getInstance()->tmpData;
    if(tmpData->gameLayerType == TempData::kGameTypeInvalid) tmpData->gameLayerType = TempData::kGameTypeBuffet;
    //current = 0;//story mode
    current = (int)tmpData->gameLayerType;
    
    inRotate = false;
    Size size = Director::getInstance()->getVisibleSize();
    log("size[%f,%f]", size.width, size.height);
    
    rotateLayer = LayerRGBA::create();
    
    Point o = Point(size.width/2,size.height/2);
    float rlen = size.height*2/3;
    
    bool locked = false;
    Size itemSize;
    Sprite *lockSprite=NULL;
    
    //buffet
    buffetSprite = Sprite::createWithSpriteFrameName("stageLogo_buffet.png");
    buffetSprite->setPosition(o + Point(rlen*cos(180*PI/180), rlen*sin(180*PI/180)));
    if(tmpData->gameLayerType == TempData::kGameTypeBuffet) buffetSprite->setScale(2);
    buffetSprite->setRotation(-ANG*2);
    locked = GameData::getInstance()->isLargeStageLocked(1);
    if(locked){
        lockSprite = Sprite::createWithSpriteFrameName("stageLogo_lock.png");
        itemSize = buffetSprite->getContentSize();
        lockSprite->setPosition(Point(itemSize.width/2, itemSize.height/2));
        buffetSprite->addChild(lockSprite);
    }
    rotateLayer->addChild(buffetSprite);
    maskSprite0 = Sprite::createWithSpriteFrameName("stageLogo_buffet.png");
    maskSprite0->setPosition(o + Point(rlen*cos(180*PI/180), rlen*sin(180*PI/180)));
    maskSprite0->setColor(Color3B::BLACK);
    maskSprite0->setOpacity(0xaa);
    if(tmpData->gameLayerType == TempData::kGameTypeBuffet){
        maskSprite0->setScale(2);
        maskSprite0->setOpacity(0x0);
    }
    maskSprite0->setRotation(-ANG*2);
    rotateLayer->addChild(maskSprite0);
    
    //toyCastle
    toyCastleSprite = Sprite::createWithSpriteFrameName("stageLogo_toyCastle.png");
    toyCastleSprite->setPosition(o + Point(rlen*cos(135*PI/180), rlen*sin(135*PI/180)));
    if(tmpData->gameLayerType == TempData::kGameTypeToyCastle) toyCastleSprite->setScale(2);
    toyCastleSprite->setRotation(-ANG);
    rotateLayer->addChild(toyCastleSprite);
    locked = GameData::getInstance()->isLargeStageLocked(2);
    if(locked){
        lockSprite = Sprite::createWithSpriteFrameName("stageLogo_lock.png");
        itemSize = toyCastleSprite->getContentSize();
        lockSprite->setPosition(Point(itemSize.width/2, itemSize.height/2));
        toyCastleSprite->addChild(lockSprite);
        //show star num
        unsigned int totalStarNum = 0;
        for(int i=1;i<=1;i++){
            for(int j=1;j<=15;j++){
                unsigned int num = GameData::getInstance()->getStarNum(i, j);
                if(num==0) break;
                totalStarNum+=num;
            }
        }
        char starNumText[64];
        sprintf(starNumText, "%d/%d", totalStarNum, 35*1);
        LabelBMFont *starNumLabel = LabelBMFont::create(starNumText, "number_dollar.fnt");
        Size sizeLock = lockSprite->getContentSize();
        lockSprite->addChild(starNumLabel);
        starNumLabel->setPosition(Point(sizeLock.width/2, 0));
    }
    
    maskSprite1 = Sprite::createWithSpriteFrameName("stageLogo_toyCastle.png");
    maskSprite1->setPosition(o + Point(rlen*cos(135*PI/180), rlen*sin(135*PI/180)));
    maskSprite1->setColor(Color3B::BLACK);
    maskSprite1->setOpacity(0xaa);
    if(tmpData->gameLayerType == TempData::kGameTypeToyCastle){
        maskSprite1->setScale(2);
        maskSprite1->setOpacity(0x0);
    }
    maskSprite1->setRotation(-ANG);
    rotateLayer->addChild(maskSprite1);
    
    //tvShow
    tvShowSprite = Sprite::createWithSpriteFrameName("stageLogo_tvShow.png");
    tvShowSprite->setPosition(o + Point(rlen*cos(90*PI/180), rlen*sin(90*PI/180)));
    if(tmpData->gameLayerType == TempData::kGameTypeTvShow) tvShowSprite->setScale(2);
    rotateLayer->addChild(tvShowSprite);
    locked = GameData::getInstance()->isLargeStageLocked(3);
    if(locked){
        lockSprite = Sprite::createWithSpriteFrameName("stageLogo_lock.png");
        itemSize = tvShowSprite->getContentSize();
        lockSprite->setPosition(Point(itemSize.width/2, itemSize.height/2));
        tvShowSprite->addChild(lockSprite);
        //show star num
        unsigned int totalStarNum = 0;
        for(int i=1;i<=2;i++){
            for(int j=1;j<=15;j++){
                unsigned int num = GameData::getInstance()->getStarNum(i, j);
                if(num==0) break;
                totalStarNum+=num;
            }
        }
        char starNumText[64];
        sprintf(starNumText, "%d/%d", totalStarNum, 35*2);
        LabelBMFont *starNumLabel = LabelBMFont::create(starNumText, "number_dollar.fnt");
        Size sizeLock = lockSprite->getContentSize();
        lockSprite->addChild(starNumLabel);
        starNumLabel->setPosition(Point(sizeLock.width/2, 0));
    }
    
    maskSprite2 = Sprite::createWithSpriteFrameName("stageLogo_tvShow.png");
    maskSprite2->setPosition(o + Point(rlen*cos(90*PI/180), rlen*sin(90*PI/180)));
    maskSprite2->setColor(Color3B::BLACK);
    maskSprite2->setOpacity(0xaa);
    if(tmpData->gameLayerType == TempData::kGameTypeTvShow){
        maskSprite2->setScale(2);
        maskSprite2->setOpacity(0x0);
    }
    rotateLayer->addChild(maskSprite2);
    
    //bazaar
    bazaarSprite = Sprite::createWithSpriteFrameName("stageLogo_bazaar.png");
    bazaarSprite->setPosition(o + Point(rlen*cos(45*PI/180), rlen*sin(45*PI/180)));
    if(tmpData->gameLayerType == TempData::kGameTypeBazaar) bazaarSprite->setScale(2);
    bazaarSprite->setRotation(ANG);
    rotateLayer->addChild(bazaarSprite);
    locked = GameData::getInstance()->isLargeStageLocked(4);
    if(locked){
        lockSprite = Sprite::createWithSpriteFrameName("stageLogo_lock.png");
        itemSize = bazaarSprite->getContentSize();
        lockSprite->setPosition(Point(itemSize.width/2, itemSize.height/2));
        bazaarSprite->addChild(lockSprite);
        //show star num
        unsigned int totalStarNum = 0;
        for(int i=1;i<=3;i++){
            for(int j=1;j<=15;j++){
                unsigned int num = GameData::getInstance()->getStarNum(i, j);
                if(num==0) break;
                totalStarNum+=num;
            }
        }
        char starNumText[64];
        sprintf(starNumText, "%d/%d", totalStarNum, 35*3);
        LabelBMFont *starNumLabel = LabelBMFont::create(starNumText, "number_dollar.fnt");
        Size sizeLock = lockSprite->getContentSize();
        lockSprite->addChild(starNumLabel);
        starNumLabel->setPosition(Point(sizeLock.width/2, 0));
    }
    
    maskSprite3 = Sprite::createWithSpriteFrameName("stageLogo_bazaar.png");
    maskSprite3->setPosition(o + Point(rlen*cos(45*PI/180), rlen*sin(45*PI/180)));
    maskSprite3->setColor(Color3B::BLACK);
    maskSprite3->setOpacity(0xaa);
    if(tmpData->gameLayerType == TempData::kGameTypeBazaar){
        maskSprite3->setScale(2);
        maskSprite3->setOpacity(0x0);
    }
    maskSprite3->setRotation(ANG);
    rotateLayer->addChild(maskSprite3);
    
    //blank
    blankSprite = Sprite::createWithSpriteFrameName("stageLogo_blank.png");
    blankSprite->setPosition(o + Point(rlen*cos(0*PI/180), rlen*sin(0*PI/180)));
    blankSprite->setRotation(ANG*2);
    rotateLayer->addChild(blankSprite);
    
    maskSprite4 = Sprite::createWithSpriteFrameName("stageLogo_blank.png");
    maskSprite4->setPosition(o + Point(rlen*cos(0*PI/180), rlen*sin(0*PI/180)));
    maskSprite4->setColor(Color3B::BLACK);
    maskSprite4->setOpacity(0xaa);
    maskSprite4->setRotation(ANG*2);
    rotateLayer->addChild(maskSprite4);
    
//    rotateLayer->setPosition(Point(size.width/2, size.height/2));
    rotateLayer->setPosition(Point(0,-size.height*2/3));
    //rotateLayer->setRotation(90);
    rotateLayer->setRotation((2-current)*ANG);
    this->addChild(rotateLayer);
    modeSprites = Array::create(buffetSprite,
                                toyCastleSprite,
                                tvShowSprite,
                                bazaarSprite,
                                blankSprite, NULL);
    modeSprites->retain();
    maskSprites = Array::create(maskSprite0,maskSprite1,maskSprite2,maskSprite3,maskSprite4,NULL);
    maskSprites->retain();
    
    initModeRotateSprite();
}

void StageSelectLayer::buttonCallBack(Object* pSender)
{
    if(pSender == nextItemSprite){//prev mode
        if(inRotate) return;
        //log("prev");
        if(current==0) return;
        inRotate = true;
        
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        RotateBy *rotateAction = RotateBy::create(TIME, ANG);
        rotateLayer->runAction(rotateAction);
        Sequence *seq = Sequence::create(EaseBackOut::create(rotateAction),
                                         CallFunc::create(CC_CALLBACK_0(StageSelectLayer::rotateOver, this)), NULL);
        rotateLayer->runAction(seq);
        RotateBy *rotateAction2 = RotateBy::create(TIME, 360/5);
        modeRotateLayer->runAction(EaseBackOut::create(rotateAction2));
        
        ScaleBy *scaleUpAction = ScaleBy::create(TIME, 0.5);
        ScaleBy *scaleDownAction = ScaleBy::create(TIME, 2);
        ScaleBy *scaleUpAction1 = ScaleBy::create(TIME, 0.5);
        ScaleBy *scaleDownAction1 = ScaleBy::create(TIME, 2);
        FadeTo *fadeIn = FadeTo::create(TIME, 0x0);
        FadeTo *fadeOut = FadeTo::create(TIME, 0x88);
        if(current-1 >=0){
            ((Sprite*)modeSprites->objectAtIndex(current-1))->runAction(EaseBackOut::create(scaleDownAction));
            ((Sprite*)maskSprites->objectAtIndex(current-1))->runAction(EaseBackOut::create(scaleDownAction1));
            ((Sprite*)maskSprites->objectAtIndex(current-1))->runAction(fadeIn);
        }
        ((Sprite*)modeSprites->objectAtIndex(current))->runAction(EaseBackOut::create(scaleUpAction));
        ((Sprite*)maskSprites->objectAtIndex(current))->runAction(EaseBackOut::create(scaleUpAction1));
        ((Sprite*)maskSprites->objectAtIndex(current))->runAction(fadeOut);
        
        current--;
        unsigned int next=current;
        if(next==0) next=4;
        SpriteFrame *frame=NULL;
        switch (current) {
            case 0://bazaar
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_buffet.png");
                break;
            case 1:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_toyCastle.png");
                break;
            case 2:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_tvShow.png");
                break;
            case 3:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_bazaar.png");
                break;
            case 4:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_blank.png");
                break;
                
            default:
                break;
        }
        bg2->setDisplayFrame(frame);
        FadeOut *fadeOutBg = FadeOut::create(TIME);
        bg->runAction(fadeOutBg);
    }else if(pSender == prevItemSprite){//next mode
        if(inRotate) return;
        if(current==modeSprites->count()-1) return;
        inRotate = true;
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        //log("next");
        RotateBy *rotateAction = RotateBy::create(TIME, -ANG);
        rotateLayer->runAction(rotateAction);
        Sequence *seq = Sequence::create(EaseBackOut::create(rotateAction),
                                         CallFunc::create(CC_CALLBACK_0(StageSelectLayer::rotateOver, this)), NULL);
        rotateLayer->runAction(seq);
        RotateBy *rotateAction2 = RotateBy::create(TIME, -360/5);
        modeRotateLayer->runAction(EaseBackOut::create(rotateAction2));
        
        ScaleBy *scaleUpAction = ScaleBy::create(TIME, 0.5);
        ScaleBy *scaleDownAction = ScaleBy::create(TIME, 2);
        ScaleBy *scaleUpAction1 = ScaleBy::create(TIME, 0.5);
        ScaleBy *scaleDownAction1 = ScaleBy::create(TIME, 2);
        FadeTo *fadeIn = FadeTo::create(TIME, 0x0);
        FadeTo *fadeOut = FadeTo::create(TIME, 0x88);
        if(current+1 <= modeSprites->count()-1){
            ((Sprite*)modeSprites->objectAtIndex(current+1))->runAction(EaseBackOut::create(scaleDownAction));
            ((Sprite*)maskSprites->objectAtIndex(current+1))->runAction(EaseBackOut::create(scaleDownAction1));
            ((Sprite*)maskSprites->objectAtIndex(current+1))->runAction(fadeIn);
        }
        ((Sprite*)modeSprites->objectAtIndex(current))->runAction(EaseBackOut::create(scaleUpAction));
        ((Sprite*)maskSprites->objectAtIndex(current))->runAction(EaseBackOut::create(scaleUpAction1));
        ((Sprite*)maskSprites->objectAtIndex(current))->runAction(fadeOut);
        current++;
        
        unsigned int next=current;
        if(next==4) next=0;
        SpriteFrame *frame=NULL;
        switch (current) {
            case 0://bazaar
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_buffet.png");
                break;
            case 1:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_toyCastle.png");
                break;
            case 2:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_tvShow.png");
                break;
            case 3:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_bazaar.png");
                break;
            case 4:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_blank.png");
                break;
                
            default:
                break;
        }
        bg2->setDisplayFrame(frame);
        FadeOut *fadeOutBg = FadeOut::create(TIME);
        bg->runAction(fadeOutBg);
    }else{
        //log("back");
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        Scene *scene = StartLayer::scene();
        TransitionFade *fade = TransitionFade::create(0.3, scene, Color3B::BLACK);
        Director::getInstance()->replaceScene(fade);
    }
}

void StageSelectLayer::rotateOver()
{
    SpriteFrame *frame=NULL;
    switch (current) {
        case 0://bazaar
            frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_buffet.png");
            break;
        case 1:
            frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_toyCastle.png");
            break;
        case 2:
            frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_tvShow.png");
            break;
        case 3:
            frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_bazaar.png");
            break;
        case 4:
            frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_blank.png");
            break;
            
        default:
            break;
    }
    bg->setDisplayFrame(frame);
    bg->setOpacity(0xff);
    inRotate = false;
}

void StageSelectLayer::initModeRotateSprite()
{
    modeRotateSprites = Array::create(Sprite::createWithSpriteFrameName("stageBg_buffet_rotate.png"),
                                      Sprite::createWithSpriteFrameName("stageBg_toyCastle_rotate.png"),
                                      Sprite::createWithSpriteFrameName("stageBg_tvShow_rotate.png"),
                                      Sprite::createWithSpriteFrameName("stageBg_bazaar_rotate.png"),
                                      Sprite::createWithSpriteFrameName("stageBg_blank_rotate.png"),
                                      NULL);
    modeRotateSprites->retain();
    modeRotateLayer = Layer::create();
    modeRotateLayer->setRotation(-90-72*current);
    //modeRotateLayer->setScale(0.2);
    this->addChild(modeRotateLayer);
    
    Size size = Director::getInstance()->getVisibleSize();
    modeRotateLayer->setPosition(Point(0, -size.height-270));
    Point o = Point(size.width/2, size.height/2);
    float radius = 620;
    for(int i=0; i<5; i++){
        Sprite *sprite = (Sprite*)modeRotateSprites->objectAtIndex(i);
        sprite->setRotation(90+i*360.0/5);
        modeRotateLayer->addChild(sprite);
        sprite->setPosition(o + Point(radius*cos(-i*360.0*PI/180/5), radius*sin(-i*360.0*PI/180/5)));
    }
}

void StageSelectLayer::onEnterTransitionDidFinish()
{
    //log("StageSelectLayer on Enter");
    Director::getInstance()->getTouchDispatcher()->addTargetedDelegate(this, 1, true);
    Layer::onEnterTransitionDidFinish();
}

void StageSelectLayer::onExit()
{
    //log("StageSelectLayer on Exit");
    Director::getInstance()->getTouchDispatcher()->removeDelegate(this);
    Layer::onExit();
}

bool StageSelectLayer::ccTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    beforePoint = Director::getInstance()->convertToGL(touch->getLocationInView());
//    log("start start location[%f,%f]", location.x, location.y);
    return true;
}

void StageSelectLayer::ccTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
//    log("touch Moved");
}

void StageSelectLayer::ccTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
    Point location = Director::getInstance()->convertToGL(touch->getLocationInView());
    if(fabs(location.x-beforePoint.x)>30 || fabs(location.y-beforePoint.y)>30){
        if(location.x > beforePoint.x){
            buttonCallBack(nextItemSprite);
        }else
            buttonCallBack(prevItemSprite);
        return;
    }
    Size size = buffetSprite->getContentSize();
    Size winSize = Director::getInstance()->getVisibleSize();
    if(location.x < winSize.width/2-size.width/2 || location.x > winSize.width/2 + size.width/2
       || location.y < winSize.height/2-size.height/2 || location.y > winSize.height/2 + size.height/2){
        //log("out of range");
        return;
    }
    if(current>3 || GameData::getInstance()->isLargeStageLocked(current+1)){
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("wrong.mp3");
        return;
    }
    if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
	Scene *scene;
	if(current == 0){
        GameData::getInstance()->tmpData->gameLayerType = TempData::kGameTypeBuffet;
        GameData::getInstance()->setCurrentGame(1, 0);
	}else if(current == 1){
        GameData::getInstance()->tmpData->gameLayerType = TempData::kGameTypeToyCastle;
        GameData::getInstance()->setCurrentGame(2, 0);
	}else if(current == 2){
        GameData::getInstance()->tmpData->gameLayerType = TempData::kGameTypeTvShow;
        GameData::getInstance()->setCurrentGame(3, 0);
	}else if(current == 3){
        GameData::getInstance()->tmpData->gameLayerType = TempData::kGameTypeBazaar;
        GameData::getInstance()->setCurrentGame(4, 0);
    }else if(current == 4){//blank
        return;
    }
    scene = StageMenuSub::scene();
	TransitionFade *fade = TransitionFade::create(0.3, scene, Color3B::BLACK);
	Director::getInstance()->replaceScene(fade);
}

StageSelectLayer::~StageSelectLayer()
{
    modeSprites->release();
    maskSprites->release();
    //SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("beding-hd.plist");
    //SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("stageBack-hd.plist");
    //SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("stageBg-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("stageLogo-hd.plist");
    //if(loadPlistFile) SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("being-hd.plist");
}
