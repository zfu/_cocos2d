#include "AppDelegate.h"
#include "StartLayer.h"
#include "ShopLayer.h"
#include "GameData.h"
#include "GameLayer.h"
#include "StageSelectLayer.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("being-hd.plist");
    GameData::getInstance()->release();
    removeAllResource();
}

void AppDelegate::removeAllResource()
{
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("bg_logo-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("mode-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("modeRotate-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("modebg-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("ingame-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("animate_time-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("bg_pause-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("effect-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("bad_effect-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("bg_ingame_0-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("food-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("bg_ingame_1-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("toy-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("bg_ingame_2-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("role-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("bg_ingame_3-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("tool-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("bg_ingame_sp-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("food-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("toy-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("role-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("tool-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("guid-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("handbook-hd.plist");//for mode and rotateSprite
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("stageBack-hd.plist");//background
	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("setup-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("shop-hd.plist");//for background
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("shop_mode-hd.plist");//for mode
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("stageBack-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("stageBg-hd.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("stageLogo-hd.plist");
    
    //music
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("button.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("change.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("cleaner.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("clock.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("coin.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("correct.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("lost.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("paper.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("pause.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("plane.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("star.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("steel.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("timeup.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("tips.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("tool.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("victory.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("water1.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("water2.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("wood.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("wrong.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect("zoom.mp3");
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    Director* director = Director::getInstance();
    EGLView* eglView = EGLView::getInstance();

    director->setOpenGLView(eglView);
	
    // turn on display FPS
    director->setDisplayStats(false);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);
    Size size = eglView->getFrameSize();
    //eglView->setDesignResolutionSize(2048, 1536, ResolutionPolicy::SHOW_ALL);
    float width = 768/size.height * size.width;
    log("window size[%f,%f] - designsize[%f,%d]", size.width, size.height, width, 768);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    eglView->setDesignResolutionSize(width, 768, ResolutionPolicy::SHOW_ALL);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    eglView->setDesignResolutionSize(width, 768, ResolutionPolicy::SHOW_ALL);
#endif
    //director->setContentScaleFactor(1.0);

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("being-hd.plist");
    // create a scene. it's an autorelease object
    //Scene *scene = HelloWorld::scene();
    GameData *gameData = GameData::getInstance();
    gameData->retain();
    
    Scene *scene = LogoLayer::scene();
    //Scene *scene = StageSelectLayer::scene();

    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    log("App Enter background");
    Director::getInstance()->stopAnimation();
    
    CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    log("App Enter foreground");
    Director::getInstance()->startAnimation();

    CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}
