//
//  GameData.cpp
//  notfound
//
//  Created by 付卓 on 13-11-12.
//
//

#include "GameData.h"

static GameData *_sharedGameData=NULL;

GameData *GameData::getInstance()
{
    if (!_sharedGameData) {
        _sharedGameData = GameData::create();
    }
    return _sharedGameData;
}

bool GameData::init()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    initAndroidData();
#endif
    //coinNums = 0;
    loadGameData();
    
    tmpData = new TempData();
    
    initHandBookData();
    
    //TODO read from ini file
    backGroundMusicOn = true;
    effectOn = true;
    
    //init tools
    initTools();
    
    //init satge Info
    initStageInfo();
    
    loadMusicEffect();
    
    initSkillInfo();
    return true;
}

GameData::~GameData()
{
    delete tmpData;
    //for music
    delete musicEffectIni;
    
    //for handbook
    delete buffetINI;
    delete toyCastleINI;
    delete tvShowINI;
    delete bazaarINI;
    
    //for stage
    delete stageINI;
    
    //for gameData
    delete gameDataINI;
    
    //for skill
    delete skillINI;
}

void GameData::resetAllData()
{
    log("WANING____________ reste all data");
    char index[1024];
    
    //data.ini
    coinNums = 0;
    gameDataINI->setIntValue("coin", "num", 0);
    gameDataINI->setStringValue("clock", "name", "clock");
    gameDataINI->setIntValue("clock", "num", 0);
    gameDataINI->setStringValue("glass", "name", "glass");
    gameDataINI->setIntValue("glass", "num", 0);
    gameDataINI->setStringValue("rotate", "name", "rotate");
    gameDataINI->setIntValue("rotate", "num", 0);
    gameDataINI->setStringValue("clean", "name", "clean");
    gameDataINI->setIntValue("clean", "num", 0);
    gameDataINI->setBoolValue("game", "firstTime", true);
    gameDataINI->setBoolValue("gameLayer", "firstTime", true);
    gameDataINI->setBoolValue("handbookLayer", "firstTime", true);
    
    for(int i=1; i<=6; i++){
        sprintf(index, "skill_%d", i);
        gameDataINI->setIntValue(index, "level", 0);
    }
    gameDataINI->writeConfigFile();
    updateTools();
    
    //stage.ini
    for(int i=1; i<=5; i++){
        sprintf(index, "self_%d", i);
        stageINI->setBoolValue(index, "lock", true);
    }
    for(int i=1; i<=4; i++){
        for(int j=1; j<=15; j++){
            sprintf(index, "level_%d_%d", i,j);
            stageINI->setBoolValue(index, "lock", true);
            stageINI->setIntValue(index, "starNum", 0);
        }
    }
    stageINI->setBoolValue("self_1", "lock", false);
    stageINI->setBoolValue("level_1_1", "lock", false);
    stageINI->writeConfigFile();
    resetStageInfo();
    
    //bazaar.ini/buffet.ini/toyCastle.ini/tvShow.ini
    for(int i=1; i<=20; i++){
        sprintf(index, "tools_%d", i);
        bazaarINI->setIntValue(index, "TimeCur", 0);
        buffetINI->setIntValue(index, "TimeCur", 0);
        toyCastleINI->setIntValue(index, "TimeCur", 0);
        tvShowINI->setIntValue(index, "TimeCur", 0);
    }
    bazaarINI->writeConfigFile();
    buffetINI->writeConfigFile();
    toyCastleINI->writeConfigFile();
    tvShowINI->writeConfigFile();
}

void GameData::loadGameData()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    std::string path = FileUtils::getInstance()->getWritablePath() + "data.ini";
#else
    std::string path = FileUtils::getInstance()->fullPathForFilename("data.ini");
#endif
    gameDataINI = new ConfigINI(path.c_str());
    coinNums = gameDataINI->getIntValue("coin", "num");
    updateTools();
}

bool GameData::isFirstTime(std::string str)
{
    bool firstTime = gameDataINI->getBoolValue(str.c_str(), "firstTime");
    if(firstTime) gameDataINI->setBoolValue(str.c_str(), "firstTime", false);//only one time true
    return firstTime;
}

void GameData::updateTools()
{
    toolData[0].num = gameDataINI->getIntValue("clock", "num");
    toolData[1].num = gameDataINI->getIntValue("glass", "num");
    toolData[2].num = gameDataINI->getIntValue("rotate", "num");
    toolData[3].num = gameDataINI->getIntValue("clean", "num");
    toolData[0].level = gameDataINI->getIntValue("clock", "level");
    toolData[1].level = gameDataINI->getIntValue("glass", "level");
    toolData[2].level = gameDataINI->getIntValue("rotate", "level");
    toolData[3].level = gameDataINI->getIntValue("clean", "level");
}
void GameData::initAndroidData()
{
    std::string path = FileUtils::getInstance()->getWritablePath();
    //setup.ini
    if(!FileUtils::getInstance()->isFileExist(path+"setup.ini")){
        log("Android- file [%s] not exist, copy from assets", (path+"setup.ini").c_str());
        copyData("setup.ini");
    }else log("file already exist[%s]", path.c_str());
    //bazaar.ini
    if(!FileUtils::getInstance()->isFileExist(path+"bazaar.ini")){
        log("Android- file [%s] not exist, copy from assets", (path+"bazaar.ini").c_str());
        copyData("bazaar.ini");
    }else log("file already exist[%s]", path.c_str());
    //buffet.ini
    if(!FileUtils::getInstance()->isFileExist(path+"buffet.ini")){
        log("Android- file [%s] not exist, copy from assets", (path+"buffet.ini").c_str());
        copyData("buffet.ini");
    }else log("file already exist[%s]", path.c_str());
    //tvShow.ini
    if(!FileUtils::getInstance()->isFileExist(path+"tvShow.ini")){
        log("Android- file [%s] not exist, copy from assets", (path+"tvShow.ini").c_str());
        copyData("tvShow.ini");
    }else log("file already exist[%s]", path.c_str());
    //toyCastle.ini
    if(!FileUtils::getInstance()->isFileExist(path+"toyCastle.ini")){
        log("Android- file [%s] not exist, copy from assets", (path+"toyCastle.ini").c_str());
        copyData("toyCastle.ini");
    }else log("file already exist[%s]", path.c_str());
    //stage.ini
    if(!FileUtils::getInstance()->isFileExist(path+"stage.ini")){
        log("Android- file [%s] not exist, copy from assets", (path+"stage.ini").c_str());
        copyData("stage.ini");
    }else log("file already exist[%s]", path.c_str());
    //data.ini
    if(!FileUtils::getInstance()->isFileExist(path+"data.ini")){
        log("Android- file [%s] not exist, copy from assets", (path+"data.ini").c_str());
        copyData("data.ini");
    }else log("file already exist[%s]", path.c_str());
    //skill.ini
    if(!FileUtils::getInstance()->isFileExist(path+"skill.ini")){
        log("Android- file [%s] not exist, copy from assets", (path+"skill.ini").c_str());
        copyData("skill.ini");
    }else log("file already exist[%s]", path.c_str());
}

void GameData::loadMusicEffect()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    std::string path = FileUtils::getInstance()->getWritablePath() + "setup.ini";
#else
    std::string path = FileUtils::getInstance()->fullPathForFilename("setup.ini");
#endif
    //test ConfigINI
    //now must exist
    //log("-----> game config use file:[%s]", path.c_str());
    musicEffectIni = new ConfigINI(path.c_str());
    
    backGroundMusicOn = musicEffectIni->getBoolValue("game", "musicOn");
    log("music on = %d", backGroundMusicOn);
    effectOn = musicEffectIni->getBoolValue("game", "effectOn");
    log("effect on = %d", effectOn);
}

void GameData::saveMusicEffect()
{
    musicEffectIni->writeConfigFile();
}

void GameData::setBackGroundMusicOn(bool value)
{
    backGroundMusicOn = value;
    musicEffectIni->setBoolValue("game", "musicOn", backGroundMusicOn);
    saveMusicEffect();
}

void GameData::setEffectOn(bool value)
{
    effectOn = value;
    musicEffectIni->setBoolValue("game", "effectOn", effectOn);
    saveMusicEffect();
}

void GameData::initHandBookData()
{
    /*
    std::string fullPath = FileUtils::getInstance()->fullPathFromRelativeFile("bazaar.ini", "");
    ConfigINI *ini = new ConfigINI(fullPath.c_str());
    for (int i=0; i<ini->datas.size(); i++) {
        log("index[%s]:", ini->datas[i].index.c_str());
    }
     */
    log("init handbook data");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    std::string path_buffet = FileUtils::getInstance()->getWritablePath() + "buffet.ini";
    std::string path_toyCastle = FileUtils::getInstance()->getWritablePath() + "toyCastle.ini";
    std::string path_tvShow = FileUtils::getInstance()->getWritablePath() + "tvShow.ini";
    std::string path_bazaar = FileUtils::getInstance()->getWritablePath() + "bazaar.ini";
#else
    std::string path_buffet = FileUtils::getInstance()->fullPathForFilename("buffet.ini");
    std::string path_toyCastle = FileUtils::getInstance()->fullPathForFilename("toyCastle.ini");
    std::string path_tvShow = FileUtils::getInstance()->fullPathForFilename("tvShow.ini");
    std::string path_bazaar = FileUtils::getInstance()->fullPathForFilename("bazaar.ini");
#endif
    buffetINI = new ConfigINI(path_buffet.c_str());
    toyCastleINI = new ConfigINI(path_toyCastle.c_str());
    tvShowINI = new ConfigINI(path_tvShow.c_str());
    bazaarINI = new ConfigINI(path_bazaar.c_str());
    
    //load data from INI
    /*
    string fName = getHandBookDataFileName(1, 1);
    log("getFIle name[%s]", fName.c_str());
    log("is 1-1 locked[%d]", isHandBookLocked(1, 1));
     */
}

string GameData::getHandBookDataFileName(unsigned int stage, unsigned int index)
{
    ConfigINI *ini;
    if(stage==1) ini=buffetINI;
    else if(stage==2) ini=toyCastleINI;
    else if(stage==3) ini=tvShowINI;
    else if(stage==4) ini=bazaarINI;
    else CCASSERT(true, "show not go here");
    char index_name[1024];
    sprintf(index_name, "tools_%d", index);
    //log("index_name[%s]", index_name);
    const char *fileName = ini->getStringValue(index_name, "fileName");
    //log("%s - fileName[%s]", index_name, fileName);
    return string(fileName);
}

string GameData::getHandBookDataDesc(unsigned int stage, unsigned int index)
{
    ConfigINI *ini;
    if(stage==1) ini=buffetINI;
    else if(stage==2) ini=toyCastleINI;
    else if(stage==3) ini=tvShowINI;
    else if(stage==4) ini=bazaarINI;
    else CCASSERT(true, "show not go here");
    char index_name[1024];
    sprintf(index_name, "tools_%d", index);
    //log("index_name[%s]", index_name);
    const char *fileName = ini->getStringValue(index_name, "desc");
    //log("%s - fileName[%s]", index_name, fileName);
    return string(fileName);
}

string GameData::getHandBookDataName(unsigned int stage, unsigned int index)
{
    ConfigINI *ini;
    if(stage==1) ini=buffetINI;
    else if(stage==2) ini=toyCastleINI;
    else if(stage==3) ini=tvShowINI;
    else if(stage==4) ini=bazaarINI;
    else CCASSERT(true, "show not go here");
    char index_name[1024];
    sprintf(index_name, "tools_%d", index);
    //log("index_name[%s]", index_name);
    const char *fileName = ini->getStringValue(index_name, "name");
    //log("%s - fileName[%s]", index_name, fileName);
    return string(fileName);
}

bool GameData::isHandBookLocked(unsigned int stage, unsigned int index)
{
    ConfigINI *ini;
    if(stage==1) ini=buffetINI;
    else if(stage==2) ini=toyCastleINI;
    else if(stage==3) ini=tvShowINI;
    else if(stage==4) ini=bazaarINI;
    else CCASSERT(false, "show not go here");
    char index_name[1024];
    sprintf(index_name, "tools_%d", index);
    int Timecurr = ini->getIntValue(index_name, "TimeCur");
    int TimeNeed = ini->getIntValue(index_name, "timeNeed");
    //log("Timecurr/TimeNeed - [%d/%d]", Timecurr, TimeNeed);
    return Timecurr<TimeNeed;
}

void GameData::updateHandBookItem(std::string fileName)//call in game
{
    unsigned int stage = currentStage;
    
    if(currentStage == 5){
        string num = fileName.substr(0, fileName.length()-4);
        log("num= %s", num.c_str());
        if(atoi(num.c_str())>4074) return;//not a normal target
        stage = atoi(num.c_str())/1000;
    }
    
    ConfigINI *ini;
    if(stage==1) ini=buffetINI;
    else if(stage==2) ini=toyCastleINI;
    else if(stage==3) ini=tvShowINI;
    else if(stage==4) ini=bazaarINI;
    else CCASSERT(false, "show not go here");
    log("update Item[%s]", fileName.c_str());
    
    for(unsigned int i=0; i<13; i++){
        char index_name[1024];
        sprintf(index_name, "tools_%d", i+1);
        const char *fName = ini->getStringValue(index_name, "fileName");
        if(strcmp(fileName.c_str(), fName)==0){
            int timecurr = ini->getIntValue(index_name, "TimeCur");
            if(timecurr<5) timecurr++;
            log("update [%s-%s]:to [%d]", index_name, "TimeCur", timecurr);
            ini->setIntValue(index_name, "TimeCur", timecurr);
            ini->writeConfigFile();//write back
            return;
        }
    }
    log("not a handbook item");
    //CCASSERT(false, "must find one");
}

unsigned int GameData::getTotalUnlockedNum()//all unlocked num
{
    unsigned int ans = 0;
    for(int i=0; i<4; i++){
        for(int j=0; j<13; j++){
            if(!isHandBookLocked(i+1, j+1)) ans++;
        }
    }
    return ans;
}

unsigned int GameData::getStageUnlockedNum(unsigned int stage)//unlocked item num for current stage
{
    unsigned int ans = 0;
    for(int j=0; j<13; j++){
        if(!isHandBookLocked(stage+1, j+1)) ans++;
    }
    return ans;
}

unsigned int GameData::getStageItemUnlockNum(unsigned int stage, unsigned int index)
{
    ConfigINI *ini;
    if(stage==1) ini=buffetINI;
    else if(stage==2) ini=toyCastleINI;
    else if(stage==3) ini=tvShowINI;
    else if(stage==4) ini=bazaarINI;
    else CCASSERT(true, "show not go here");
    char index_name[1024];
    sprintf(index_name, "tools_%d", index);
    int Timecurr = ini->getIntValue(index_name, "TimeCur");
    return Timecurr;
}

void GameData::initTools()
{
    //TODO read from save Data
//    sprintf(toolData[0].name, "clock");
//    sprintf(toolData[1].name, "glass");
//    sprintf(toolData[2].name, "rotate");
//    sprintf(toolData[3].name, "clean");
    sprintf(toolData[0].name, "%s", gameDataINI->getStringValue("clock", "name"));
    sprintf(toolData[1].name, "%s", gameDataINI->getStringValue("glass", "name"));
    sprintf(toolData[2].name, "%s", gameDataINI->getStringValue("rotate", "name"));
    sprintf(toolData[3].name, "%s", gameDataINI->getStringValue("clean", "name"));
}

void GameData::getEternalTarget(int *num, int **targets)
{
    int _stage=-1, _substage=-1;
    bool ok = false;
    for(int i=1;i<=4;i++){
        if(GameData::getInstance()->isStageLocked(i, 1)){
            _stage = i-1;
            ok = true;
            break;
        }
    }
    if(!ok) _stage=4;
    log("current unlocked[%d-%d]", _stage, _substage);
    ok = false;
    for(int i=1;i<=15;i++){
        if(GameData::getInstance()->isStageLocked(_stage, i)){
            _substage = i-1;
            ok = true;
            break;
        }
    }
    if(!ok) _substage = 15;
    log("current unlocked[%d-%d]", _stage, _substage);
    int itemTotalNum = (_stage-1)*75 + ((((int)(_substage+2)/3)-1)*5 + 55);
    log("itemTotalNum=%d", itemTotalNum);
    
    int count0 = rand()%4+1;
    int count1 = rand()%4+1;
    int count2 = rand()%4+1;
    
    *num = count0+count1+count2;
    int *tar = new int[*num];//total items
    
    int t0,t1,t2;
    //type0
    int ii = 0;
    int index = rand()%itemTotalNum;
    int line=index/75+1;
    int subIndex = index%75;
    t0 = line*1000 + subIndex;
    for(int i=0;i<count0; i++) tar[ii++]=t0;
    
    //type1
    while (true) {
        index = rand()%itemTotalNum;
        line=index/75+1;
        subIndex = index%75;
        t1 = line*1000 + subIndex;
        if(t1==t0) continue;
        for(int i=0;i<count1; i++) tar[ii++]=t1;
        break;
    }
    
    //type2
    while (true) {
        index = rand()%itemTotalNum;
        line=index/75+1;
        subIndex = index%75;
        t2 = line*1000 + subIndex;
        if(t2==t0 || t2==t1) continue;
        for(int i=0;i<count2; i++) tar[ii++]=t2;
        break;
    }
    *targets = tar;
    log("----debug targets---------");
    for(int i=0; i<*num; i++){
        log("-> %d.png", tar[i]);
    }
    log("----debug targets--over------");
//    CCASSERT(false, "test");
}

void GameData::getTarget(int *num, int **targets)
{
	if((currentStage==1) && (currentSubStage==1)){
		*num=1;
	}else if((currentStage==1) && (currentSubStage==2)){
		*num=2;
	}else if((currentStage==1) && (currentSubStage==3)){
		*num=2;
	}else if((currentStage==1) && ((currentSubStage==4) || (currentSubStage==5))) {
		*num=3;
	}else if(currentStage<=4){//for forever mode
		*num=((currentStage-1)*15 + currentSubStage)/5+3;
    }else{//for external mode
        *num=99;//TODO set foever mode
    }

	Array *arr = Array::create();
	for (int i=0; i<55; i++) arr->addObject(Integer::create(i));
	int count = 55;
	int k = 0;
    int stage = currentStage;
    int *t = new int[*num];
    for(int i=0; i<*num; i++){
        t[i] = 0;
		if(stage==5){
            t[i] = rand()%75 + currentStage*1000;
        }else{
			k = rand() % count;
			count--;
			t[i]=((Integer *)arr->objectAtIndex(k))->getValue()  + ((currentSubStage + 2)/3 -1)*5 + stage*1000;
			arr->removeObjectAtIndex(k);
        }
	}
    // *num=2;
	int *array_num = new int[*num];
	int total_num = 0;
	
	for(int i=0; i<*num; i++) {
		array_num[i] = rand()%5;
		if (array_num[i] ==0)
			array_num[i] = 2;
		if (array_num[i] == 1)
			array_num[i] = 3;
		total_num += array_num[i];
//		log("*num is %d\n", *num);
//		log("array_num is %d\n", array_num[i]);
	}
//	log("total num is %d\n", total_num);
	int *target_return = new int[total_num];
	int base=0;
	for(int i=0; i<*num; i++) {
		if(i>0)
		base+=array_num[i-1];
		for(int j=0; j<array_num[i]; j++) {
			target_return[base+j]=t[i];
		}
	}
    *num = total_num;
    *targets = target_return;
	delete[] t;
	delete[] array_num;
	//log("+++++++++++++++++++++++++++xxxxxx num is %d\n", *num);
}

unsigned int GameData::getEternalItem()
{
    int _stage=-1, _substage=-1;
    for(int i=1;i<=4;i++){
        if(GameData::getInstance()->isStageLocked(i, 1)){
            _stage = i-1;
            break;
        }
    }
    log("current unlocked[%d-%d]", _stage, _substage);
    for(int i=1;i<=15;i++){
        if(GameData::getInstance()->isStageLocked(_stage, i)){
            _substage = i-1;
            break;
        }
    }
    log("current unlocked[%d-%d]", _stage, _substage);
    int itemTotalNum = (_stage-1)*75 + ((((int)(_substage+2)/3)-1)*5 + 55);
    log("itemTotalNum=%d", itemTotalNum);
    
    unsigned int index = rand()%itemTotalNum;
    int ss = index/75+1;
    int subs = index%75;
    return ss*1000+subs;
}

void GameData::setCurrentGame(int stage, int subStage)
{
    log("---------> set current Game [%d-%d]", stage, subStage);
    currentStage = stage;
    currentSubStage=subStage;
    if(stage==1) tmpData->gameLayerType = TempData::kGameTypeBuffet;
    else if(stage == 2) tmpData->gameLayerType = TempData::kGameTypeToyCastle;
    else if(stage == 3) tmpData->gameLayerType = TempData::kGameTypeTvShow;
    else if(stage == 4) tmpData->gameLayerType = TempData::kGameTypeBazaar;
    else if(stage == 5) tmpData->gameLayerType = TempData::kGameEternal;//infinite mode
    else log("ERROR game stage");
}

void GameData::initStageInfo()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    std::string path = FileUtils::getInstance()->getWritablePath() + "stage.ini";
#else
    std::string path = FileUtils::getInstance()->fullPathForFilename("stage.ini");
#endif
    stageINI = new ConfigINI(path.c_str());
    char index[1024];
    for(int i=0; i<5; i++){
        sprintf(index, "self_%d", i+1);
        bool lock = stageINI->getBoolValue(index, "lock");
        stageData[i].selfLocked = lock;
        log("stage [%d].lock = %d", i+1, lock);
    }
    for(int i=0; i<4; i++){
        for(int j=0; j<15; j++){
            sprintf(index, "level_%d_%d", i+1, j+1);
            bool lock = stageINI->getBoolValue(index, "lock");
            int starNum = stageINI->getIntValue(index, "starNum");
            stageData[i].starNum[j] = starNum;
            stageData[i].locked[j] = lock;
        }
    }
}

void GameData::resetStageInfo()
{
    char index[1024];
    for(int i=0; i<4; i++){
        sprintf(index, "self_%d", i+1);
        bool lock = stageINI->getBoolValue(index, "lock");
        stageData[i].selfLocked = lock;
        log("stage [%d].lock = %d", i+1, lock);
    }
    for(int i=0; i<4; i++){
        for(int j=0; j<15; j++){
            sprintf(index, "level_%d_%d", i+1, j+1);
            bool lock = stageINI->getBoolValue(index, "lock");
            int starNum = stageINI->getIntValue(index, "starNum");
            stageData[i].starNum[j] = starNum;
            stageData[i].locked[j] = lock;
        }
    }
}

bool GameData::isNextStageLocked()
{
    int mstage = currentStage;
    int subStage = currentSubStage;
    if(subStage==15 && mstage<=3){//sub stage on next big stage
        mstage++;
        subStage=0;
    }else if(subStage<15){//sub stage on current next sub stage
        subStage++;
    }else{
        log("no next stage and substage");
        return true;
    }
    log("is stage locked %d-%d", mstage, subStage);
    //return stageData[mstage].locked[subStage];
    return isStageLocked(mstage, subStage);
}

bool GameData::isStageLocked(int stage, int subStage)
{
    return stageData[stage-1].locked[subStage-1];
}

bool GameData::isLargeStageLocked(int stage)
{
    return stageData[stage-1].selfLocked;
}

bool GameData::updateStar(int stage, int subStage, int starNum)
{
    log("update starnum ||||||||||||||||||||||||||||||||||||||||||||||");
    int oriStartNum = getStarNum(stage, subStage);
    if(oriStartNum>=starNum) return false;
    int stageIndex = stage-1;
    int subStageIndex = subStage-1;
    log("update star %d-%d", stageIndex, subStageIndex);
    
    if(subStageIndex<14){//unlock little stage
        log("unlock %d-%d", stageIndex, subStageIndex+1);
        stageData[stageIndex].locked[subStageIndex+1] = false;//open next sub stage
        char index[1024];
        sprintf(index, "level_%d_%d", stage, subStage+1);
        log("write config --> level %d-%d", stage, subStage+1);
        stageINI->setBoolValue(index, "lock", false);
        stageINI->writeConfigFile();
    }
    
    //update starNum
    stageData[stageIndex].starNum[subStageIndex] = starNum;
    char index[1024];
    sprintf(index, "level_%d_%d", stage, subStage);
    log("write config starNUm --> level %d-%d", stage, subStage);
    stageINI->setIntValue(index, "starNum", starNum);
    stageINI->writeConfigFile();
    
    {//try unlock large stage
        //first find a need unlock stage
        int targetStage = -1;
        for(int i=0; i<4; i++){
            if(stageData[i].selfLocked == true){
                targetStage=i;
                break;
            }
        }
        log("=======> targetStage[%d]", targetStage);
        if(targetStage==-1){return true;}//all stage are unlocked
        //check all star num is OK
        unsigned int starTotalNum = 0;
        for(int i=0; i<targetStage; i++){
            //log("LOOP1, i=%d, targetStage=%d, locked[%d]", i, targetStage, stageData[i].selfLocked);
            if(stageData[i].selfLocked == false){
                for(int j=0; j<15; j++){
                    //log("cout %d-%d:%d", i+1, j+1, stageData[i].starNum[j]);
                    starTotalNum+=stageData[i].starNum[j];
                }
            }
        }
        log("total Star num[%d], targetStage = [%d]", starTotalNum, targetStage);
        
        //here unlock eternal mode
        if(starTotalNum >= 90 && stageData[4].selfLocked==true){
            stageData[4].selfLocked = false;
            log("unlock eternal MODE ***************");
            stageINI->setBoolValue("self_5", "lock", false);
            stageINI->writeConfigFile();
        }
        
        //check all sub stage are unlocked
        bool allSubStageUnlock = true;
        for(int i=0; i<15; i++){
            if(stageData[targetStage-1].locked[i] == true ||
               stageData[targetStage-1].starNum[i] == 0){
                allSubStageUnlock = false;
                log("not all subsage finished");
                return true;//can't unlock a large stage
            }
        }
        //here unlock a large stage
        if(starTotalNum < targetStage*35) return true;
        
        stageData[targetStage].locked[0] = false;//open next big stage
        stageData[targetStage].selfLocked = false;//open a big stage
        log("unlock %d-%d", targetStage+1, 1);
        char index[1024];
        sprintf(index, "level_%d_%d", targetStage+1, 1);
        log("write config open a large stage --> level %d-%d", targetStage+1, 1);
        
        stageINI->setBoolValue(index, "lock", false);
        
        sprintf(index, "self_%d", targetStage+1);
        stageINI->setBoolValue(index, "lock", false);
        stageINI->writeConfigFile();
        
    }
    return true;
}

bool GameData::updateCurrentStageStar(int startNum)
{
    return updateStar(currentStage, currentSubStage, startNum);
}

int GameData::getStarNum(int stage, int subStage)
{
    int stageIndex = stage-1;
    int subStageIndex = subStage-1;
    return stageData[stageIndex].starNum[subStageIndex];
}

void GameData::copyData(const char* pFileName)
{
    std::string strPath = FileUtils::getInstance()->fullPathFromRelativeFile(pFileName, "");
    unsigned long len = 0;
    unsigned char *data = NULL;
    
    data = FileUtils::getInstance()->getFileData(strPath.c_str(),"r",&len);
    std::string destPath = FileUtils::getInstance()->getWritablePath();
    destPath += pFileName;
    FILE *fp = fopen(destPath.c_str(),"w");
    fwrite(data,sizeof(char),len,fp);
    fclose(fp);
    delete []data;
    data = NULL;
}

unsigned int GameData::getSkillLevel(unsigned int index)
{
    char indexStr[1024];
    sprintf(indexStr, "skill_%d", index);
    return gameDataINI->getIntValue(indexStr, "level");
}

void GameData::addSkillLevel(unsigned int index)
{
    char indexStr[1024];
    sprintf(indexStr, "skill_%d", index);
    unsigned int level = gameDataINI->getIntValue(indexStr, "level");
    gameDataINI->setIntValue(indexStr, "level", level+1);
}

void GameData::initSkillInfo()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    std::string path = FileUtils::getInstance()->getWritablePath() + "skill.ini";
#else
    std::string path = FileUtils::getInstance()->fullPathForFilename("skill.ini");
#endif
    skillINI = new ConfigINI(path.c_str());
}

string GameData::getToolName(unsigned int index)
{
    char indexStr[1024];
    sprintf(indexStr, "tool_%d", index);
    return skillINI->getStringValue(indexStr, "name");
}
string GameData::getToolComment(unsigned int index)
{
    char indexStr[1024];
    sprintf(indexStr, "tool_%d", index);
    return skillINI->getStringValue(indexStr, "comment");
}
unsigned int GameData::getToolPrice(unsigned int index)
{
    char indexStr[1024];
    sprintf(indexStr, "tool_%d", index);
    return skillINI->getIntValue(indexStr, "price");
}

string GameData::getCoinName(unsigned int index)
{
    char indexStr[1024];
    sprintf(indexStr, "coin_%d", index);
    return skillINI->getStringValue(indexStr, "name");
}
string GameData::getCoinComment(unsigned int index)
{
    char indexStr[1024];
    sprintf(indexStr, "coin_%d", index);
    return skillINI->getStringValue(indexStr, "comment");
}

unsigned int GameData::getCoinPrice(unsigned int index)
{
    char indexStr[1024];
    sprintf(indexStr, "coin_%d", index);
    return skillINI->getIntValue(indexStr, "price");
}
string GameData::getSkillName(unsigned int index)
{
    char indexStr[1024];
    sprintf(indexStr, "skill_%d", index);
    return skillINI->getStringValue(indexStr, "name");
}

string GameData::getSkillComment(unsigned int index)
{
    char indexStr[1024];
    sprintf(indexStr, "skill_%d", index);
    return skillINI->getStringValue(indexStr, "comment");
}

string GameData::getSkillComment(unsigned int index, unsigned int level)
{
    char indexStr[1024];
    sprintf(indexStr, "skill_%d_%d", index, level);
    return skillINI->getStringValue(indexStr, "comment");
}

unsigned int GameData::getSkillminValue(unsigned int index, unsigned int level)
{
    char indexStr[1024];
    sprintf(indexStr, "skill_%d_%d", index, level);
    return skillINI->getIntValue(indexStr, "minValue");
}
unsigned int GameData::getSkillmaxValue(unsigned int index, unsigned int level)
{
    char indexStr[1024];
    sprintf(indexStr, "skill_%d_%d", index, level);
    return skillINI->getIntValue(indexStr, "maxValue");
}
unsigned int GameData::getSkillValue(unsigned int index, unsigned int level)
{
    char indexStr[1024];
    sprintf(indexStr, "skill_%d_%d", index, level);
    return skillINI->getIntValue(indexStr, "value");
}
unsigned int GameData::getSkillPrice(unsigned int index, unsigned int level)
{
    char indexStr[1024];
    sprintf(indexStr, "skill_%d_%d", index, level);
    return skillINI->getIntValue(indexStr, "price");
}
