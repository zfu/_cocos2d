//
//  handBookLayer.cpp
//  notfound
//
//  Created by 付卓 on 13-11-10.
//
//

#include "handBookLayer.h"

#include "StartLayer.h"
#include "SimpleAudioEngine.h"
#include "GuidLayer.h"
#include "StageSelectLayer.h"
#include "GameData.h"

#define PI 3.14159265
#define ANG 45
#define TIME 0.3
#define itemSpriteTag 0xff

typedef struct pos{
    float x;
    float y;
}pos;

static bool inited = false;
static pos itemPosition[13] = {
    {128-512,580},
    {325-512,670},
    {505-512,685},
    {705-512,670},
    {890-512,590},
    {130-512,400},
    {325-512,470},
    {510-512,480},
    {705-512,470},
    {900-512,400},
    {320-512,290},
    {515-512,300},
    {708-512,285}
};

Scene* handBookLayer::scene()
{
    Scene *scene = Scene::create();
    handBookLayer *layer = handBookLayer::create();
    if(layer){
        scene->addChild(layer);
        return scene;
    }
    return NULL;
}

bool handBookLayer::init()
{
    //SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bg_logo-hd.plist");
    //SpriteFrameCache::getInstance()->addSpriteFramesWithFile("modebg-hd.plist");
    //SpriteFrameCache::getInstance()->addSpriteFramesWithFile("mode-hd.plist");
    //SpriteFrameCache::getInstance()->addSpriteFramesWithFile("modeRotate-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("handbook-hd.plist");//for mode and rotateSprite
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("stageBack-hd.plist");//background
    
    bg = Sprite::createWithSpriteFrameName("stageBack_buffet.png");
    bg2 = Sprite::createWithSpriteFrameName("stageBack_buffet.png");
    this->addChild(bg2);
    this->addChild(bg);
    Size size = Director::getInstance()->getVisibleSize();

    if(!inited){
        inited = true;
        for(int i=0; i<13; i++) itemPosition[i].x+=size.width/2;
    }

    bg->setPosition(Point(size.width/2, size.height/2));
    bg2->setPosition(Point(size.width/2, size.height/2));
    Size bgSize = bg->getContentSize();
    float scale = (size.width/bgSize.width)>(size.height/bgSize.height)?(size.width/bgSize.width):(size.height/bgSize.height);
    log("scale = %f", scale);
    bg->setScale(scale);
    bg2->setScale(scale);
    
    modeLogo = Sprite::createWithSpriteFrameName("handbook_title.png");
    Size logoSize = modeLogo->getContentSize();
    //modeLogo->setScale(0.5);
    modeLogo->setPosition(Point(size.width/2, size.height-logoSize.height/2));
    this->addChild(modeLogo);
    
    modeRotateSprite = Sprite::createWithSpriteFrameName("handbook_bk.png");
    Size itemSize = modeRotateSprite->getContentSize();
    //modeRotateSprite->setScale(0.5);
    modeRotateSprite->setPosition(Point(size.width/2, itemSize.height/2));
    this->addChild(modeRotateSprite);
    
    stage1UnlockNum = GameData::getInstance()->getStageUnlockedNum(0);
    stage2UnlockNum = GameData::getInstance()->getStageUnlockedNum(1);
    stage3UnlockNum = GameData::getInstance()->getStageUnlockedNum(2);
    stage4UnlockNum = GameData::getInstance()->getStageUnlockedNum(3);
    totoalUnlockNum = stage1UnlockNum + stage2UnlockNum + stage3UnlockNum + stage4UnlockNum;
    
    sprintf(percentString, "%d/%d", totoalUnlockNum, 13*4);
    percentLabel = LabelTTF::create(percentString, "STHeitiTC-Bold", 30);
    percentLabel->setColor(Color3B::BLACK);
    percentLabel->setPosition(Point(itemSize.width/2, itemSize.height/8 - 5));
    modeRotateSprite->addChild(percentLabel);
    initModeSprite();
    
//    Size visibleSize = Director::getInstance()->getVisibleSize();
//    log("visible Size[%f,%f]", visibleSize.width, visibleSize.height);
    
    //for next and prev
    nextItemSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("being_all.png"),
                                            Sprite::createWithSpriteFrameName("being_all_selected.png"),
                                            CC_CALLBACK_1(handBookLayer::buttonCallback, this));
    //nextItemSprite->setScale(0.5);
	nextItemSprite->setPosition(size.width*5/6, size.height/6);
    nextItemSprite->setVisible(false);
    
    prevItemSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("being_all.png"),
                                            Sprite::createWithSpriteFrameName("being_all_selected.png"),
                                            CC_CALLBACK_1(handBookLayer::buttonCallback, this));
    //prevItemSprite->setScale(0.5);
    prevItemSprite->setRotation(-180.0);
	prevItemSprite->setPosition(size.width*1/6, size.height/6);
    prevItemSprite->setVisible(false);
    
    guideItemSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("being_return2.png"),
                                             Sprite::createWithSpriteFrameName("being_return2_selected.png"),
                                             CC_CALLBACK_1(handBookLayer::buttonCallback, this));
    //guideItemSprite->setScale(0.5);
    itemSize = guideItemSprite->getContentSize();
	guideItemSprite->setPosition(size.width-itemSize.width/2, size.height-itemSize.height/2);
    
    menu = Menu::create(nextItemSprite, prevItemSprite, guideItemSprite, NULL);
    menu->setTag(123);
    menu->setPosition(Point::ZERO);
    this->addChild(menu);
    
    initItemSprite();
    initItems();
    this->changeToStage(kSelectStage);
    //play the music
        //CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
        //CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("mainMenu.mp3", true);
    isGuideShow = false;
    if(GameData::getInstance()->isFirstTime("handbookLayer")){
        log("yes first time");
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("zoom.mp3");
        isGuideShow = true;
        guidSprite = Sprite::createWithSpriteFrameName("guid_1.png");
        guidSprite->setPosition(Point(size.width/2, size.height/2));
        Size guidSize = guidSprite->getContentSize();
        float scaleX = size.width/guidSize.width;
        float scaleY = size.height/guidSize.height;
        ScaleTo *scaleTo = ScaleTo::create(1, scaleX, scaleY);
        guidSprite->runAction(EaseBackOut::create(scaleTo));
        this->addChild(guidSprite);
    }
    return true;
}

void handBookLayer::initModeSprite()
{
    current = 0;//story mode
    inRotate = false;
    Size size = Director::getInstance()->getVisibleSize();
    //log("size[%f,%f]", size.width, size.height);
    
    rotateLayer = LayerRGBA::create();
//    rotateLayer->setScale(0.2);
    
    Point o = Point(size.width/2,size.height/2);
    float rlen = size.height*2/3 + 70;
    
    achieveModeSprite = Sprite::createWithSpriteFrameName("handbook_menu_1.png");
    achieveModeSprite->setPosition(o + Point(rlen*cos(180*PI/180), rlen*sin(180*PI/180)));
    achieveModeSprite->setRotation(-ANG*2);
    achieveModeSprite->setScale(2);
    rotateLayer->addChild(achieveModeSprite);
    
    achieveMaskSprite = Sprite::createWithSpriteFrameName("handbook_menu_1.png");
    achieveMaskSprite->setPosition(o + Point(rlen*cos(180*PI/180), rlen*sin(180*PI/180)));
    achieveMaskSprite->setRotation(-ANG*2);
    achieveMaskSprite->setColor(Color3B::BLACK);
    achieveMaskSprite->setOpacity(0x0);
    achieveMaskSprite->setScale(2);
    rotateLayer->addChild(achieveMaskSprite);
    
    setupModeSprite = Sprite::createWithSpriteFrameName("handbook_menu_3.png");
    setupModeSprite->setPosition(o + Point(rlen*cos(135*PI/180), rlen*sin(135*PI/180)));
    setupModeSprite->setRotation(-ANG);
    rotateLayer->addChild(setupModeSprite);
    
    setupMaskSprite = Sprite::createWithSpriteFrameName("handbook_menu_3.png");
    setupMaskSprite->setPosition(o + Point(rlen*cos(135*PI/180), rlen*sin(135*PI/180)));
    setupMaskSprite->setRotation(-ANG);
    setupMaskSprite->setColor(Color3B::BLACK);
    setupMaskSprite->setOpacity(0xaa);
    rotateLayer->addChild(setupMaskSprite);
    
    storyModeSprite = Sprite::createWithSpriteFrameName("handbook_menu_2.png");
    storyModeSprite->setPosition(o + Point(rlen*cos(90*PI/180), rlen*sin(90*PI/180)));
    rotateLayer->addChild(storyModeSprite);
    
    storyMaskSprite = Sprite::createWithSpriteFrameName("handbook_menu_2.png");
    storyMaskSprite->setPosition(o + Point(rlen*cos(90*PI/180), rlen*sin(90*PI/180)));
    storyMaskSprite->setColor(Color3B::BLACK);
    storyMaskSprite->setOpacity(0xaa);
    rotateLayer->addChild(storyMaskSprite);
    
    eternalModeSprite = Sprite::createWithSpriteFrameName("handbook_menu_4.png");
    eternalModeSprite->setPosition(o + Point(rlen*cos(45*PI/180), rlen*sin(45*PI/180)));
    eternalModeSprite->setRotation(ANG);
    rotateLayer->addChild(eternalModeSprite);
    
    eternalMaskSprite = Sprite::createWithSpriteFrameName("handbook_menu_4.png");
    eternalMaskSprite->setPosition(o + Point(rlen*cos(45*PI/180), rlen*sin(45*PI/180)));
    eternalMaskSprite->setRotation(ANG);
    eternalMaskSprite->setColor(Color3B::BLACK);
    eternalMaskSprite->setOpacity(0xaa);
    rotateLayer->addChild(eternalMaskSprite);
    
    rotateLayer->setPosition(Point(0,-size.height*2/3));
    rotateLayer->setRotation(90.0);//for index 0
    this->addChild(rotateLayer);
    modeSprites = Array::create(achieveModeSprite,
                                setupModeSprite,
                                storyModeSprite,
                                eternalModeSprite,
                                NULL);
    modeSprites->retain();
    maskSprites = Array::create(achieveMaskSprite,
                                setupMaskSprite,
                                storyMaskSprite,
                                eternalMaskSprite,
                                NULL);
    maskSprites->retain();
}

void handBookLayer::initItems()
{
    itemLayer = Layer::create();
    itemLayer->setPosition(Point::ZERO);
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("food-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("toy-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("role-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tool-hd.plist");
    /*
    for(unsigned int i=0; i<13; i++){
        string fileName = GameData::getInstance()->getHandBookDataFileName(current+1, i+1);
        log("add sprite[%s]", fileName.c_str());
        Sprite *sprite = Sprite::createWithSpriteFrameName(fileName.c_str());
        sprite->setPosition(Point(itemPosition[i].x, itemPosition[i].y));
        itemLayer->addChild(sprite, i+1);
        //sprite->setColor(Color3B::BLACK);
    }
     */
    itemLayer->setVisible(false);
    this->addChild(itemLayer);
}

void handBookLayer::updateItems()
{
    //log("update Items");
    itemLayer->removeAllChildrenWithCleanup(true);
    for(unsigned int i=0; i<13; i++){
        string fileName = GameData::getInstance()->getHandBookDataFileName(current+1, i+1);
        //log("add sprite[%s]", fileName.c_str());
        Sprite *sprite = Sprite::createWithSpriteFrameName(fileName.c_str());
        sprite->setPosition(Point(itemPosition[i].x, itemPosition[i].y));
        itemLayer->addChild(sprite, i+1);
        if(GameData::getInstance()->isHandBookLocked(current+1, i+1))
           sprite->setColor(Color3B::BLACK);
    }
}

void handBookLayer::buttonCallback(Object* pSender)
{
    if(isGuideShow) return;
    if(pSender == nextItemSprite){//prev mode
        if(inRotate) return;
        //log("prev");
        if(current==0) return;
        inRotate = true;
        
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        RotateBy *rotateAction = RotateBy::create(TIME, ANG);
        //rotateLayer->runAction(rotateAction);
        Sequence *seq = Sequence::create(EaseBackOut::create(rotateAction),
                                         CallFunc::create(CC_CALLBACK_0(handBookLayer::rotateOver, this)), NULL);
        rotateLayer->runAction(seq);
        
        ScaleBy *scaleUpAction = ScaleBy::create(TIME, 0.5);
        ScaleBy *scaleDownAction = ScaleBy::create(TIME, 2);
        ScaleBy *scaleUpAction1 = ScaleBy::create(TIME, 0.5);
        ScaleBy *scaleDownAction1 = ScaleBy::create(TIME, 2);
        FadeTo *fadeIn = FadeTo::create(TIME, 0x0);
        FadeTo *fadeOut = FadeTo::create(TIME, 0x88);
        if(current-1 >=0){
            ((Sprite*)modeSprites->objectAtIndex(current-1))->runAction(EaseBackOut::create(scaleDownAction));
            ((Sprite*)maskSprites->objectAtIndex(current-1))->runAction(EaseBackOut::create(scaleDownAction1));
            ((Sprite*)maskSprites->objectAtIndex(current-1))->runAction(fadeIn);
        }
        ((Sprite*)modeSprites->objectAtIndex(current))->runAction(EaseBackOut::create(scaleUpAction));
        ((Sprite*)maskSprites->objectAtIndex(current))->runAction(EaseBackOut::create(scaleUpAction1));
        ((Sprite*)maskSprites->objectAtIndex(current))->runAction(fadeOut);
        
        current--;
        
        SpriteFrame *frame=NULL;
        unsigned int next=current;
        if(next==0) next=3;
        switch (next) {
            case 0://bazaar
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_buffet.png");
                break;
            case 1:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_toyCastle.png");
                break;
            case 2:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_tvShow.png");
                break;
            case 3:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_bazaar.png");
                break;
            default:
                break;
        }
        bg2->setDisplayFrame(frame);
        FadeOut *fadeOutBg = FadeOut::create(TIME);
        bg->runAction(fadeOutBg);
    }else if(pSender == prevItemSprite){//next mode
        if(inRotate) return;
        if(current==modeSprites->count()-1) return;
        inRotate = true;
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        //log("next");
        RotateBy *rotateAction = RotateBy::create(TIME, -ANG);
        Sequence *seq = Sequence::create(EaseBackOut::create(rotateAction),
                                         CallFunc::create(CC_CALLBACK_0(handBookLayer::rotateOver, this)), NULL);
        rotateLayer->runAction(seq);
        
        ScaleBy *scaleUpAction = ScaleBy::create(TIME, 0.5);
        ScaleBy *scaleDownAction = ScaleBy::create(TIME, 2);
        ScaleBy *scaleUpAction1 = ScaleBy::create(TIME, 0.5);
        ScaleBy *scaleDownAction1 = ScaleBy::create(TIME, 2);
        FadeTo *fadeIn = FadeTo::create(TIME, 0x0);
        FadeTo *fadeOut = FadeTo::create(TIME, 0x88);
        if(current+1 <= modeSprites->count()-1){
            ((Sprite*)modeSprites->objectAtIndex(current+1))->runAction(EaseBackOut::create(scaleDownAction));
            ((Sprite*)maskSprites->objectAtIndex(current+1))->runAction(EaseBackOut::create(scaleDownAction1));
            ((Sprite*)maskSprites->objectAtIndex(current+1))->runAction(fadeIn);
        }
        ((Sprite*)modeSprites->objectAtIndex(current))->runAction(EaseBackOut::create(scaleUpAction));
        ((Sprite*)maskSprites->objectAtIndex(current))->runAction(EaseBackOut::create(scaleUpAction1));
        ((Sprite*)maskSprites->objectAtIndex(current))->runAction(fadeOut);
        current++;
        
        SpriteFrame *frame=NULL;
        unsigned int next=current;
        if(next==4) next=0;
        switch (next) {
            case 0://bazaar
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_buffet.png");
                break;
            case 1:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_toyCastle.png");
                break;
            case 2:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_tvShow.png");
                break;
            case 3:
                frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_bazaar.png");
                break;
            default:
                break;
        }
        bg2->setDisplayFrame(frame);
        FadeOut *fadeOutBg = FadeOut::create(TIME);
        bg->runAction(fadeOutBg);
    }else{
        log("return");
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        if(mState==kSelectStage){
            Scene *scene = StartLayer::scene();
            TransitionFade *fade = TransitionFade::create(0.3, scene, Color3B::BLACK);
            Director::getInstance()->replaceScene(fade);
        }else if(mState==kSelectItem){
            this->changeToStage(kSelectStage);
            itemLayer->setVisible(false);
            sprintf(percentString, "%d/%d", totoalUnlockNum, 13*4);
            percentLabel->setString(percentString);
        }else if(mState==kItemDetal){
            this->changeToStage(kSelectItem);
        }
    }
}

void handBookLayer::rotateOver()
{
    SpriteFrame *frame=NULL;
    switch (current) {
        case 0://bazaar
            frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_buffet.png");
            break;
        case 1:
            frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_toyCastle.png");
            break;
        case 2:
            frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_tvShow.png");
            break;
        case 3:
            frame = SpriteFrameCache::getInstance()->getSpriteFrameByName("stageBack_bazaar.png");
            break;
        default:
            break;
    }
    bg->setDisplayFrame(frame);
    bg->setOpacity(0xff);
    inRotate = false;
}

void handBookLayer::onEnterTransitionDidFinish()
{
    log("handBookLayer on Enter");
    Director::getInstance()->getTouchDispatcher()->addTargetedDelegate(this, 1, true);
    Layer::onEnterTransitionDidFinish();
}

void handBookLayer::onExit()
{
    log("handBookLayer on Exit");
    Director::getInstance()->getTouchDispatcher()->removeDelegate(this);
    Layer::onExit();
}

bool handBookLayer::ccTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    if(isGuideShow) return true;
    beforePoint = Director::getInstance()->convertToGL(touch->getLocationInView());
//    log("start start location[%f,%f]", location.x, location.y);
    return true;
}

void handBookLayer::ccTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
//    log("touch Moved");
}

void handBookLayer::ccTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
    if(isGuideShow){
        isGuideShow=false;
        FadeOut *fadeOut = FadeOut::create(0.5);
        ScaleTo *scale = ScaleTo::create(0.5, 0.01);
        Sequence *seq = Sequence::create(scale,
                                         CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, guidSprite, true)),
                                         NULL);
        guidSprite->runAction(fadeOut);
        guidSprite->runAction(seq);
        return;
    }
    Point location = Director::getInstance()->convertToGL(touch->getLocationInView());
    if(fabs(location.x-beforePoint.x)>30 || fabs(location.y-beforePoint.y)>30){
        if(location.x > beforePoint.x){
            if(mState==kSelectStage)
                buttonCallback(nextItemSprite);
        }else{
            if(mState==kSelectStage)
                buttonCallback(prevItemSprite);
        }
        return;
    }
    
    if(mState==kSelectStage){
        Size size = achieveModeSprite->getContentSize();
        Size winSize = Director::getInstance()->getVisibleSize();
        if(location.x < winSize.width/2-size.width/2 || location.x > winSize.width/2 + size.width/2
           || location.y < winSize.height/2-size.height/2 || location.y > winSize.height/2 + size.height/2){
            //log("out of range");
            return;
        }
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        //log("select index[%d]", current);
        this->changeToStage(kSelectItem);
        
        //show items
        updateItems();
        itemLayer->setVisible(true);
        if(current==0) sprintf(percentString, "%d/%d", stage1UnlockNum, 13);
        else if(current==1) sprintf(percentString, "%d/%d", stage2UnlockNum, 13);
        else if(current==2) sprintf(percentString, "%d/%d", stage3UnlockNum, 13);
        else if(current==3) sprintf(percentString, "%d/%d", stage4UnlockNum, 13);
        else CCASSERT(true, "should not go here");
        percentLabel->setString(percentString);
    }else if(mState==kSelectItem){
        //log("location[%f,%f]", location.x, location.y);
        int i;
        for(i=0; i<13; i++){
            //log("offset-[%d]-[%f,%f]", i, fabs(location.x-itemPosition[i].x), fabs(location.y-itemPosition[i].y));
            if(fabs(location.x-itemPosition[i].x)<=80 && fabs(location.y-itemPosition[i].y)<=80) break;
        }
        if(i!=13){
            //log("item index [%d]", i);
            Point pos = itemSprite->getPosition();
            itemSprite->removeFromParentAndCleanup(true);
            itemSprite = Sprite::create();
            itemSprite->setPosition(pos);
            itemDetalSprite->addChild(itemSprite);
            //update detail
            unsigned int timecur = GameData::getInstance()->getStageItemUnlockNum(current+1, i+1);
            std::string name = GameData::getInstance()->getHandBookDataName(current+1, i+1);
            std::string fileName = GameData::getInstance()->getHandBookDataFileName(current+1, i+1);
            std::string desc = GameData::getInstance()->getHandBookDataDesc(current+1, i+1);
            bool lock = GameData::getInstance()->isHandBookLocked(current+1, i+1);
            sprintf(itemPercentString, "%d/%d", timecur, 5);
            itemPercentLabel->setString(itemPercentString);
            if(lock){
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
                lockLabel->setString("locked");
#else
                lockLabel->setString("未解锁");
#endif
                nameLabel->setString("???");
                descLabel->setString("?????????????????");
            }else{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
                lockLabel->setString("unlocked");
#else
                lockLabel->setString("已解锁");
#endif
                nameLabel->setString(name.c_str());
                descLabel->setString(desc.c_str());
            }
            SpriteFrame *frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(fileName.c_str());
            itemSprite->setDisplayFrame(frame);
            if(lock) itemSprite->setColor(Color3B::BLACK);
            
            this->changeToStage(kItemDetal);
            if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        }else{
            log("not found");
        }
    }else if(mState==kItemDetal){
        this->changeToStage(kSelectItem);
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
    }
}

handBookLayer::~handBookLayer()
{
    //SpriteFrameCache::getInstance()->removeSpriteFramesFromFile();
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("handbook-hd.plist");//for mode and rotateSprite
    //SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("stageBack-hd.plist");//background
    modeSprites->release();
    maskSprites->release();
    
//    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("food-hd.plist");
//    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("toy-hd.plist");
//    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("role-hd.plist");
//    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("tool-hd.plist");
}

void handBookLayer::changeToStage(State state)
{
    mState = state;
    int i=0;
    switch (mState) {
        case kSelectStage:
//            log("kSelectState");
            //nextItemSprite->setVisible(true);
            //prevItemSprite->setVisible(true);
            for(i=0; i<modeSprites->count(); i++){
                ((Sprite*)modeSprites->objectAtIndex(i))->setVisible(true);
                ((Sprite*)maskSprites->objectAtIndex(i))->setVisible(true);
            }
            itemSelectSprite->setVisible(false);
            itemDetalSprite->setVisible(false);
            modeLogo->setVisible(true);
            break;
            
        case kSelectItem:
//            log("kSelectItem");
            nextItemSprite->setVisible(false);
            prevItemSprite->setVisible(false);
            for(i=0; i<modeSprites->count(); i++){
                ((Sprite*)modeSprites->objectAtIndex(i))->setVisible(false);
                ((Sprite*)maskSprites->objectAtIndex(i))->setVisible(false);
            }
            itemSelectSprite->setVisible(true);
            itemDetalSprite->setVisible(false);
            modeLogo->setVisible(false);
            break;
            
        case kItemDetal:
//            log("kItemDetal");
            nextItemSprite->setVisible(false);
            prevItemSprite->setVisible(false);
            for(i=0; i<modeSprites->count(); i++){
                ((Sprite*)modeSprites->objectAtIndex(i))->setVisible(false);
                ((Sprite*)maskSprites->objectAtIndex(i))->setVisible(false);
            }
            itemSelectSprite->setVisible(true);
            itemDetalSprite->setVisible(true);
            modeLogo->setVisible(false);
            break;
            
        default:
            break;
    }
}

void handBookLayer::initItemSprite()
{
    Size winSize = Director::getInstance()->getVisibleSize();
    itemSelectSprite = Sprite::createWithSpriteFrameName("handbook_btn_bk.png");
    itemSelectSprite->setPosition(Point(winSize.width/2, winSize.height*5/8));
    //itemSelectSprite->setScale(0.5);
    this->addChild(itemSelectSprite);
    
    itemDetalSprite = Sprite::createWithSpriteFrameName("handbook_msg.png");
    itemDetalSprite->setPosition(Point(winSize.width/2, winSize.height/2));
    //itemDetalSprite->setScale(0.5);
    this->addChild(itemDetalSprite, 20);
    
    Size size = itemDetalSprite->getContentSize();
    itemSprite = Sprite::create();
    nameLabel = LabelTTF::create("", "STHeitiTC-Bold", 25);
    descLabel = LabelTTF::create("", "STHeitiTC-Bold", 25);
    lockLabel = LabelTTF::create("", "STHeitiTC-Bold", 25);
    itemPercentLabel = LabelTTF::create("", "STHeitiTC-Bold", 25);
    
    itemSprite->setPosition(Point(size.width/2, size.height-200));
    itemDetalSprite->addChild(itemSprite);
    
    nameLabel->setPosition(Point(size.width/2, size.height-270));
    itemDetalSprite->addChild(nameLabel);
    
    descLabel->setPosition(Point(size.width/2, size.height-330));
    itemDetalSprite->addChild(descLabel);
    
    lockLabel->setPosition(Point(size.width/2, size.height-370));
    itemDetalSprite->addChild(lockLabel);
    
    itemPercentLabel->setPosition(Point(size.width/2, size.height-430));
    itemDetalSprite->addChild(itemPercentLabel);
}
