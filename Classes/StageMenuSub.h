//
//  StageSlectLayer.h
//  notfound
//
//  Created by 付卓 on 13-11-5.
//
//

#ifndef __notfound__StageMenuSub__
#define __notfound__StageMenuSub__

#include <cocos2d.h>
USING_NS_CC;

class StageMenuSub : public Layer{
public:
    static Scene *scene();
    CREATE_FUNC(StageMenuSub);
    bool init();
    
    virtual void onEnterTransitionDidFinish();
    virtual void onExit();
    virtual bool ccTouchBegan(Touch *touch, Event *event);
    virtual void ccTouchMoved(Touch *touch, Event *event);
    virtual void ccTouchEnded(Touch *touch, Event *event);
    
    void buttonReturn(Object* pSender);
    
    void buttonChoose(Object* pSender);
    ~StageMenuSub();
private:
    Sprite *bg;
	Sprite *headSprite;
  
    void initModeSprite();

	Sprite *starsSprite;
    Array *customsPassSprites;
    Layer *chooseLayer;
 
    MenuItemSprite *returnSprite;

  //game information
	//int pass[15];//value 1 indicate this game is pass, for display.
	MenuItemSprite *customsPassSprite;


};

#endif /* defined(__notfound__StageSelectLayer__) */
