//
//  GuidLayer.h
//  notfound
//
//  Created by 付卓 on 13-11-4.
//
//

#ifndef __notfound__GuidLayer__
#define __notfound__GuidLayer__

#include <iostream>

#include <cocos2d.h>
USING_NS_CC;

class GuidLayer : public Layer
{
public:
    static Scene *scene();
    CREATE_FUNC(GuidLayer);
    bool init();
    ~GuidLayer();
    void moveActionOver();
    void buttonCallBack(Object* pSender);
    
    virtual void onEnterTransitionDidFinish();
    virtual void onExit();
    virtual bool ccTouchBegan(Touch *touch, Event *event);
    virtual void ccTouchMoved(Touch *touch, Event *event);
    virtual void ccTouchEnded(Touch *touch, Event *event);
private:
    int currentIndex;
    Array *guidSprites;
    Point startPoint;
    Point endPoint;
    Point oriPosition;
    Point beforePosition;
    bool inAutoMove;
    bool loadPlistFile;
    long millisecondNow();
    long startTime;
    
    MenuItemSprite *returnSprite;
};

#endif /* defined(__notfound__GuidLayer__) */
