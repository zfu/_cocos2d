//
//  StartLayer.h
//  notfound
//
//  Created by 付卓 on 13-11-1.
//
//

#ifndef __notfound__StartLayer__
#define __notfound__StartLayer__

#include <cocos2d.h>
USING_NS_CC;

class StartLayer : public Layer{
public:
    static Scene *scene();
    CREATE_FUNC(StartLayer);
    bool init();
    
    virtual void onEnterTransitionDidFinish();
    virtual void onExit();
    virtual bool ccTouchBegan(Touch *touch, Event *event);
    virtual void ccTouchMoved(Touch *touch, Event *event);
    virtual void ccTouchEnded(Touch *touch, Event *event);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    virtual void keyBackClicked();
#endif

    void buttonCallback(Object* pSender);
    ~StartLayer();
private:
    Sprite *bg;
    Sprite *bg2;
    Sprite *setupModeSprite;
    Sprite *storyModeSprite;
    Sprite *eternalModeSprite;
    Sprite *achieveModeSprite;
    Sprite *shopModeSprite;
    
    Sprite *setupMaskSprite;
    Sprite *storyMaskSprite;
    Sprite *eternalMaskSprite;
    Sprite *achieveMaskSprite;
    Sprite *shopMaskSprite;
    void initModeSprite();
    int current;
    Array *modeSprites;
    Array *maskSprites;
    Layer *rotateLayer;
    bool inRotate;
    void rotateOver();
    
    void initModeRotateSprite();
    Array *modeRotateSprites;
    Layer *modeRotateLayer;
    
    MenuItemSprite *prevItemSprite;
    MenuItemSprite *nextItemSprite;
    MenuItemSprite *guideItemSprite;
    
    Point beforePoint;
    
    //async load all texture and music file
    void asyncLoadAllResources();
    void updateLoadState();
    
    //for guide
    bool isGuideShow;
    Sprite *guidSprite;
};

class LogoLayer : public Layer{
public:
    static Scene *scene(){
        Scene *scene = Scene::create();
        LogoLayer *layer = LogoLayer::create();
        scene->addChild(layer);
        return scene;
    }
    CREATE_FUNC(LogoLayer);
    bool init(){
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bg_logo-hd.plist");
        Size size = Director::getInstance()->getVisibleSize();
        Sprite *sprite = Sprite::createWithSpriteFrameName("bg_logo-hd.png");
        this->addChild(sprite);
        sprite->setPosition(Point(size.width/2, size.height/2));
        
        Sequence *seq = Sequence::create(DelayTime::create(1),
                                         CallFunc::create(CC_CALLBACK_0(LogoLayer::startGame, this)),
                                         NULL);
        this->runAction(seq);
        return true;
    }
    void startGame(){
        TransitionFade *fade = TransitionFade::create(1, StartLayer::scene(), Color3B::BLACK);
        Director::getInstance()->replaceScene(fade);
    }
};

#endif /* defined(__notfound__StartLayer__) */
