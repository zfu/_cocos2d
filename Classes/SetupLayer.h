//
//  StageSlectLayer.h
//  notfound
//
//  Created by 付卓 on 13-11-5.
//
//

#ifndef __notfound__SetupLayer__
#define __notfound__SetupLayer__

#include <cocos2d.h>
USING_NS_CC;

class SetupLayer : public Layer{
	typedef enum {kNormal, kInformation, kReset} State_Setup;
public:
    static Scene *scene();
    CREATE_FUNC(SetupLayer);
    bool init();
    
    virtual void onEnterTransitionDidFinish();
    virtual void onExit();
    virtual bool ccTouchBegan(Touch *touch, Event *event);
    virtual void ccTouchMoved(Touch *touch, Event *event);
    virtual void ccTouchEnded(Touch *touch, Event *event);
    
    void buttonReturn(Object* pSender);
    void buttonMusic(Object* pSender);
	 void buttonEffect(Object* pSender);
	State_Setup flags;
	void buttonShowInformation(Object* pSender);
	void buttonResetData(Object* pSender);
	void buttonYes(Object* pSender);
	void buttonNo(Object *pSender);

    ~SetupLayer();
private:
    Sprite *bg;
	Sprite *resetbg;
  
    void initModeSprite();

	MenuItemSprite *musicSprite;
	Sprite *musicOnSprite;
	Sprite *musicOffSprite;
	bool musicOn;

	MenuItemSprite *effectSprite;
	Sprite *effectOnSprite;
	Sprite *effectOffSprite;
	bool effectOn;

	Sprite *modeSetupEffectSprite;
	Sprite *modeSetupMusicSprite;

	MenuItemSprite *trashSprite;
	MenuItemSprite *producerSprite;
	Menu* menu;
   
	Sprite *informationSprite;
 
    MenuItemSprite *returnSprite;

	/* for reset game date */
	Layer *resetDateLayer;
	MenuItemSprite *yesButtonSprite;
	MenuItemSprite *noButtonSprite;
};

#endif /* defined(__notfound__StageSelectLayer__) */
