//
//  ShopSelectLayer.cpp
//  notfound
//
//  Created by 付卓 on 13-11-11.
//
//

#include "ShopSelectLayer.h"
#include "StartLayer.h"
#include "SimpleAudioEngine.h"
#include "GameLayer.h"
#include "ShopLayer.h"
#include "GameData.h"

#define PI 3.14159265
#define ANG 45
#define TIME 0.4

Scene* ShopSelectLayer::scene()
{
    Scene *scene = Scene::create();
    ShopSelectLayer *layer = ShopSelectLayer::create();
    scene->addChild(layer);
    return scene;
}

bool ShopSelectLayer::init()
{
    //SpriteFrameCache::getInstance()->addSpriteFramesWithFile("beding-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("modebg-hd.plist");//for background
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("modeRotate-hd.plist");//for rotate sprite
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("shop-hd.plist");//for shop title
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("shop_mode-hd.plist");//for mode
    
    bg = Sprite::createWithSpriteFrameName("bg_shop.png");
    this->addChild(bg);
    Size size = Director::getInstance()->getVisibleSize();
    bg->setPosition(Point(size.width/2, size.height/2));
    Size bgSize = bg->getContentSize();
    float scale = (size.width/bgSize.width)>(size.height/bgSize.height)?(size.width/bgSize.width):(size.height/bgSize.height);
    log("scale = %f", scale);
    bg->setScale(scale);
    
    initModeSprite();
    
    //modeRotateSprite = Sprite::createWithSpriteFrameName("rotate_shop.png");
    //modeRotateSprite->setPosition(Point(size.width/2, -size.height/20));
    //modeRotateSprite->setScale(0.5);
    //this->addChild(modeRotateSprite);
    
    //    Size visibleSize = Director::getInstance()->getVisibleSize();
    //    log("visible Size[%f,%f]", visibleSize.width, visibleSize.height);
    Sprite *modeLogo = Sprite::createWithSpriteFrameName("shop_title.png");
    Size logoSize = modeLogo->getContentSize();
    //modeLogo->setScale(0.5);
    modeLogo->setPosition(Point(size.width/2, size.height-logoSize.height/2));
    this->addChild(modeLogo);
    
    //for next and prev
    nextItemSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("being_all.png"), Sprite::createWithSpriteFrameName("being_all_selected.png"), CC_CALLBACK_1(ShopSelectLayer::buttonCallBack, this));
    //nextItemSprite->setScale(0.5);
	nextItemSprite->setPosition(size.width*5/6, size.height/6);
    nextItemSprite->setVisible(false);
    
    prevItemSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("being_all.png"), Sprite::createWithSpriteFrameName("being_all_selected.png"), CC_CALLBACK_1(ShopSelectLayer::buttonCallBack, this));
    //prevItemSprite->setScale(0.5);
    prevItemSprite->setRotation(-180.0);
	prevItemSprite->setPosition(size.width*1/6, size.height/6);
    prevItemSprite->setVisible(false);
    
    returnSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("being_return2.png"), Sprite::createWithSpriteFrameName("being_return2_selected.png"), CC_CALLBACK_1(ShopSelectLayer::buttonCallBack, this));
    Size spriteSize = returnSprite->getContentSize();
    //returnSprite->setScale(0.5);
    returnSprite->setPosition(size.width - spriteSize.width/2 - 5, size.height - spriteSize.height/2 - 5);
    
    Menu* menu = Menu::create(nextItemSprite, prevItemSprite, returnSprite/*, guideItemSprite*/, NULL);
    menu->setPosition(Point::ZERO);
    this->addChild(menu);
    
    //play the music
    //CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("mainMenu.mp3", true);
    return true;
}

void ShopSelectLayer::initModeSprite()
{
    TempData *tmpData = GameData::getInstance()->tmpData;
    if(tmpData->shopTypeMode == TempData::kShopTypeInvalid) tmpData->shopTypeMode = TempData::kModeBuyTool;
    //current = 0;//story mode
    current = (int)tmpData->shopTypeMode;

    //current = 1;//story mode
    inRotate = false;
    Size size = Director::getInstance()->getVisibleSize();
    log("size[%f,%f]", size.width, size.height);
    
    rotateLayer = LayerRGBA::create();
    //    rotateLayer->setScale(0.2);
    
    Point o = Point(size.width/2,size.height/2);
    float rlen = size.height*2/3;
    
    //update
    shopUpdateSprite = Sprite::createWithSpriteFrameName("shop_goods_level.png");
    shopUpdateSprite->setPosition(o + Point(rlen*cos(180*PI/180), rlen*sin(180*PI/180)));
    if(tmpData->shopTypeMode == TempData::kModeUpdateTool) shopUpdateSprite->setScale(2);
    shopUpdateSprite->setRotation(-ANG*2);
    rotateLayer->addChild(shopUpdateSprite);
    
    maskSprite0 = Sprite::createWithSpriteFrameName("shop_goods_level.png");
    maskSprite0->setPosition(o + Point(rlen*cos(180*PI/180), rlen*sin(180*PI/180)));
    maskSprite0->setRotation(shopUpdateSprite->getRotation());
    maskSprite0->setColor(Color3B::BLACK);
    maskSprite0->setOpacity(0xaa);
    if(tmpData->shopTypeMode == TempData::kModeUpdateTool){
        maskSprite0->setScale(2);
        maskSprite0->setOpacity(0x0);
    }
    maskSprite0->setRotation(-ANG*2);
    rotateLayer->addChild(maskSprite0);
    
    //buy tools
    shopBuyToolSprite = Sprite::createWithSpriteFrameName("shop_buy_goods.png");
    shopBuyToolSprite->setPosition(o + Point(rlen*cos(135*PI/180), rlen*sin(135*PI/180)));
    if(tmpData->shopTypeMode == TempData::kModeBuyTool) shopBuyToolSprite->setScale(2);
    shopBuyToolSprite->setRotation(-ANG);
    rotateLayer->addChild(shopBuyToolSprite);
    
    maskSprite1 = Sprite::createWithSpriteFrameName("shop_buy_goods.png");
    maskSprite1->setPosition(o + Point(rlen*cos(135*PI/180), rlen*sin(135*PI/180)));
    maskSprite1->setRotation(shopBuyToolSprite->getRotation());
    maskSprite1->setColor(Color3B::BLACK);
    maskSprite1->setOpacity(0xaa);
    if(tmpData->shopTypeMode == TempData::kModeBuyTool){
        maskSprite1->setScale(2);
        maskSprite1->setOpacity(0x0);
    }
    rotateLayer->addChild(maskSprite1);
    
    //buy coin
    shopBuyCoinSprite = Sprite::createWithSpriteFrameName("shop_recharge.png");
    shopBuyCoinSprite->setPosition(o + Point(rlen*cos(90*PI/180), rlen*sin(90*PI/180)));
    if(tmpData->shopTypeMode == TempData::kModeBuyCoin) shopBuyCoinSprite->setScale(2);
    rotateLayer->addChild(shopBuyCoinSprite);
    
    rotateLayer->setPosition(Point(0,-size.height*2/3));
    rotateLayer->setRotation((2-current)*ANG);
    this->addChild(rotateLayer);
    
    maskSprite2 = Sprite::createWithSpriteFrameName("shop_recharge.png");
    maskSprite2->setPosition(o + Point(rlen*cos(90*PI/180), rlen*sin(90*PI/180)));
    maskSprite2->setRotation(shopBuyCoinSprite->getRotation());
    maskSprite2->setColor(Color3B::BLACK);
    maskSprite2->setOpacity(0xaa);
    if(tmpData->shopTypeMode == TempData::kModeBuyCoin){
        maskSprite2->setScale(2);
        maskSprite2->setOpacity(0x0);
    }
    rotateLayer->addChild(maskSprite2);
    
    modeSprites = Array::create(shopUpdateSprite,
                                shopBuyToolSprite,
                                shopBuyCoinSprite,
                                NULL);
    modeSprites->retain();
    maskSprites = Array::create(maskSprite0, maskSprite1, maskSprite2, NULL);
    maskSprites->retain();
    initModeRotateSprite();
}

void ShopSelectLayer::initModeRotateSprite()
{
    modeRotateSprites = Array::create(Sprite::createWithSpriteFrameName("rotate_shop.png"),
                                      Sprite::createWithSpriteFrameName("rotate_shop.png"),
                                      Sprite::createWithSpriteFrameName("rotate_shop.png"),
                                      Sprite::createWithSpriteFrameName("rotate_shop.png"),
                                      Sprite::createWithSpriteFrameName("rotate_shop.png"),
                                      NULL);
    modeRotateSprites->retain();
    modeRotateLayer = Layer::create();
    modeRotateLayer->setRotation(-90-72*current);
    //modeRotateLayer->setScale(0.2);
    this->addChild(modeRotateLayer);
    
    Size size = Director::getInstance()->getVisibleSize();
    modeRotateLayer->setPosition(Point(0, -size.height-270));
    Point o = Point(size.width/2, size.height/2);
    float radius = 620;
    for(int i=0; i<5; i++){
        Sprite *sprite = (Sprite*)modeRotateSprites->objectAtIndex(i);
        sprite->setRotation(90+i*360.0/5);
        modeRotateLayer->addChild(sprite);
        sprite->setPosition(o + Point(radius*cos(-i*360.0*PI/180/5), radius*sin(-i*360.0*PI/180/5)));
    }
}

void ShopSelectLayer::buttonCallBack(Object* pSender)
{
    if(pSender == nextItemSprite){//prev mode
        if(inRotate) return;
        //log("prev");
        if(current==0) return;
        inRotate = true;
        
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        RotateBy *rotateAction = RotateBy::create(TIME, ANG);
        rotateLayer->runAction(rotateAction);
        Sequence *seq = Sequence::create(EaseBackOut::create(rotateAction),
                                         CallFunc::create(CC_CALLBACK_0(ShopSelectLayer::rotateOver, this)), NULL);
        rotateLayer->runAction(seq);
        RotateBy *rotateAction2 = RotateBy::create(TIME, 360/5);
        modeRotateLayer->runAction(EaseBackOut::create(rotateAction2));
        
        ScaleBy *scaleUpAction = ScaleBy::create(TIME, 0.5);
        ScaleBy *scaleDownAction = ScaleBy::create(TIME, 2);
        ScaleBy *scaleUpAction1 = ScaleBy::create(TIME, 0.5);
        ScaleBy *scaleDownAction1 = ScaleBy::create(TIME, 2);
        FadeTo *fadeIn = FadeTo::create(TIME, 0x0);
        FadeTo *fadeOut = FadeTo::create(TIME, 0x88);
        if(current-1 >=0){
            ((Sprite*)modeSprites->objectAtIndex(current-1))->runAction(EaseBackOut::create(scaleDownAction));
            ((Sprite*)maskSprites->objectAtIndex(current-1))->runAction(EaseBackOut::create(scaleDownAction1));
            ((Sprite*)maskSprites->objectAtIndex(current-1))->runAction(fadeIn);
        }
        ((Sprite*)modeSprites->objectAtIndex(current))->runAction(EaseBackOut::create(scaleUpAction));
        ((Sprite*)maskSprites->objectAtIndex(current))->runAction(EaseBackOut::create(scaleUpAction1));
        ((Sprite*)maskSprites->objectAtIndex(current))->runAction(fadeOut);
        
        current--;
    }else if(pSender == prevItemSprite){//next mode
        if(inRotate) return;
        if(current==modeSprites->count()-1) return;
        inRotate = true;
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        //log("next");
        RotateBy *rotateAction = RotateBy::create(TIME, -ANG);
        rotateLayer->runAction(rotateAction);
        Sequence *seq = Sequence::create(EaseBackOut::create(rotateAction),
                                         CallFunc::create(CC_CALLBACK_0(ShopSelectLayer::rotateOver, this)), NULL);
        rotateLayer->runAction(seq);
        RotateBy *rotateAction2 = RotateBy::create(TIME, -360/5);
        modeRotateLayer->runAction(EaseBackOut::create(rotateAction2));
        
        ScaleBy *scaleUpAction = ScaleBy::create(TIME, 0.5);
        ScaleBy *scaleDownAction = ScaleBy::create(TIME, 2);
        ScaleBy *scaleUpAction1 = ScaleBy::create(TIME, 0.5);
        ScaleBy *scaleDownAction1 = ScaleBy::create(TIME, 2);
        FadeTo *fadeIn = FadeTo::create(TIME, 0x0);
        FadeTo *fadeOut = FadeTo::create(TIME, 0x88);
        if(current+1 <= modeSprites->count()-1){
            ((Sprite*)modeSprites->objectAtIndex(current+1))->runAction(EaseBackOut::create(scaleDownAction));
            ((Sprite*)maskSprites->objectAtIndex(current+1))->runAction(EaseBackOut::create(scaleDownAction1));
            ((Sprite*)maskSprites->objectAtIndex(current+1))->runAction(fadeIn);
        }
        ((Sprite*)modeSprites->objectAtIndex(current))->runAction(EaseBackOut::create(scaleUpAction));
        ((Sprite*)maskSprites->objectAtIndex(current))->runAction(EaseBackOut::create(scaleUpAction1));
        ((Sprite*)maskSprites->objectAtIndex(current))->runAction(fadeOut);
        current++;
    }else{
        //log("back");
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        Scene *scene = StartLayer::scene();
        TransitionFade *fade = TransitionFade::create(0.3, scene, Color3B::BLACK);
        Director::getInstance()->replaceScene(fade);
    }
}

void ShopSelectLayer::rotateOver()
{
    inRotate = false;
}

void ShopSelectLayer::onEnterTransitionDidFinish()
{
    //log("ShopSelectLayer on Enter");
    Director::getInstance()->getTouchDispatcher()->addTargetedDelegate(this, 1, true);
    Layer::onEnterTransitionDidFinish();
}

void ShopSelectLayer::onExit()
{
    //log("ShopSelectLayer on Exit");
    Director::getInstance()->getTouchDispatcher()->removeDelegate(this);
    Layer::onExit();
}

bool ShopSelectLayer::ccTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    beforePoint = Director::getInstance()->convertToGL(touch->getLocationInView());
    //    log("start start location[%f,%f]", location.x, location.y);
    return true;
}

void ShopSelectLayer::ccTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
    //    log("touch Moved");
}

void ShopSelectLayer::ccTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
    Point location = Director::getInstance()->convertToGL(touch->getLocationInView());
    if(fabs(location.x-beforePoint.x)>30 || fabs(location.y-beforePoint.y)>30){
        if(location.x > beforePoint.x){
            buttonCallBack(nextItemSprite);
        }else
            buttonCallBack(prevItemSprite);
        return;
    }
    Size size = shopUpdateSprite->getContentSize();
    Size winSize = Director::getInstance()->getVisibleSize();
    if(location.x < winSize.width/2-size.width/2 || location.x > winSize.width/2 + size.width/2
       || location.y < winSize.height/2-size.height/2 || location.y > winSize.height/2 + size.height/2){
        //log("out of range");
        return;
    }
    if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
    
	Scene *scene;
	if(current == 0){
        GameData::getInstance()->tmpData->shopTypeMode = TempData::kModeUpdateTool;
	}else if(current == 1){
        GameData::getInstance()->tmpData->shopTypeMode = TempData::kModeBuyTool;
	}else if(current == 2){
        GameData::getInstance()->tmpData->shopTypeMode = TempData::kModeBuyCoin;
    }
    log("current = %d", current);
    scene = ShopLayer::scene((ShopLayer::State)current);
	TransitionFade *fade = TransitionFade::create(0.3, scene, Color3B::BLACK);
	Director::getInstance()->replaceScene(fade);
}

ShopSelectLayer::~ShopSelectLayer()
{
    modeSprites->release();
    maskSprites->release();
    //SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("beding-hd.plist");
    //SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("modebg-hd.plist");//for background
    //SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("modeRotate-hd.plist");//for rotate sprite
    //SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("shop-hd.plist");//for shop title
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("shop_mode-hd.plist");//for mode
    //if(loadPlistFile) SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("being-hd.plist");
}
