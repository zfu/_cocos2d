//
//  GuidLayer.cpp
//  notfound
//
//  Created by 付卓 on 13-11-4.
//
//

#include "GuidLayer.h"
#include "SimpleAudioEngine.h"
#include "GameData.h"

Scene *GuidLayer::scene()
{
    Scene *scene = Scene::create();
    GuidLayer *layer = GuidLayer::create();
    if(layer){
        scene->addChild(layer);
        return scene;
    }
    return NULL;
}

bool GuidLayer::init()
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("guid-hd.plist");
    Size winSize = Director::getInstance()->getVisibleSize();
    
    Sprite *sprite0 = Sprite::createWithSpriteFrameName("guid_0.png");
    Sprite *sprite1 = Sprite::createWithSpriteFrameName("guid_1.png");
    Sprite *sprite2 = Sprite::createWithSpriteFrameName("guid_2.png");
    Sprite *sprite3 = Sprite::createWithSpriteFrameName("guid_3.png");
    Sprite *sprite4 = Sprite::createWithSpriteFrameName("guid_4.png");
    guidSprites = Array::create(sprite0, sprite1, sprite2, sprite3, sprite4, NULL);
    for (int i=0; i<guidSprites->count(); i++) {
        Sprite *sprite = (Sprite*)guidSprites->objectAtIndex(i);
        Point position;
        position.x = winSize.width/2 + i*winSize.width;
        position.y = winSize.height/2;
        sprite->setPosition(position);
        Size spriteSize = sprite->getContentSize();
        sprite->setScaleX(winSize.width/spriteSize.width);
        sprite->setScaleY(winSize.height/spriteSize.height);
        this->addChild(sprite);
    }
    guidSprites->retain();
    beforePosition = getPosition();
    inAutoMove = false;
    returnSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("being_return2.png"), Sprite::createWithSpriteFrameName("being_return2_selected.png"), CC_CALLBACK_1(GuidLayer::buttonCallBack, this));
    Size spriteSize = returnSprite->getContentSize();
    //returnSprite->setScale(0.5);
    returnSprite->setPosition(-beforePosition.x + winSize.width - spriteSize.width/2 - 5,  spriteSize.height/2 - 5);
    Menu *menu = Menu::create(returnSprite, NULL);
    menu->setPosition(Point::ZERO);
    this->addChild(menu);
    return true;
}

GuidLayer::~GuidLayer()
{
    log("GuiLayer destructor");
    guidSprites->release();
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("guid-hd.plist");
}

void GuidLayer::onEnterTransitionDidFinish()
{
    Director::getInstance()->getTouchDispatcher()->addTargetedDelegate(this, 1, true);
    Layer::onEnterTransitionDidFinish();
}

void GuidLayer::onExit()
{
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("guid-hd.plist");
    Director::getInstance()->getTouchDispatcher()->removeDelegate(this);
    Layer::onExit();
}

bool GuidLayer::ccTouchBegan(Touch *touch, Event *event)
{
    if(inAutoMove) return true;
    Point location = Director::getInstance()->convertToGL(touch->getLocationInView());
    startPoint = location;
    oriPosition = this->getPosition();
    startTime = millisecondNow();
    return true;
}

void GuidLayer::ccTouchMoved(Touch *touch, Event *event)
{
    if(inAutoMove) return;
    Point location = Director::getInstance()->convertToGL(touch->getLocationInView());
    Point offset = location - startPoint;
    offset.y = 0;//only move horizon
    this->setPosition(oriPosition + offset);
}

void GuidLayer::ccTouchEnded(Touch *touch, Event *event)
{
    if(inAutoMove) return;
    Size winSize = Director::getInstance()->getWinSize();
    Point target,position;
    position = getPosition();
    long timeElapsed = millisecondNow() - startTime;
    float xoffset = position.x - oriPosition.x;
    if((timeElapsed < 200 && fabs(xoffset) > 30) || fabs(xoffset)>winSize.width/3 ){
        if(xoffset < 0 && -beforePosition.x < winSize.width*(guidSprites->count()-1))//left
        {
            target = beforePosition - Point(winSize.width,0);
        }
        else if(xoffset > 0 && -beforePosition.x >= winSize.width)//right
        {
            target = beforePosition + Point(winSize.width, 0);
        }
        else target = beforePosition;//give up
    }else target = beforePosition;
    inAutoMove = true;
    MoveTo *actionMove = MoveTo::create(0.2, target);
    Sequence *seq = Sequence::create(actionMove, CallFunc::create(CC_CALLBACK_0(GuidLayer::moveActionOver, this)), NULL);
    this->runAction(seq);
}

void GuidLayer::moveActionOver()
{
    beforePosition = getPosition();
    inAutoMove = false;
    Size winSize = Director::getInstance()->getWinSize();
    Size spriteSize = returnSprite->getContentSize();
    returnSprite->setPosition(-beforePosition.x + winSize.width - spriteSize.width/2 - 5, spriteSize.height/2 + 5);
}

void GuidLayer::buttonCallBack(Object* pSender)
{
    if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
    Director::getInstance()->popScene();//return to game
}

long GuidLayer::millisecondNow()
{
    struct timeval now;
    gettimeofday(&now, NULL);
    return (now.tv_sec * 1000 + now.tv_usec / 1000);
}
