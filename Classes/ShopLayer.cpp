//
//  ShopLayer.cpp
//  notfound
//
//  Created by 付卓 on 13-11-11.
//
//

#include "ShopLayer.h"
#include "ShopSelectLayer.h"
#include "SimpleAudioEngine.h"
#include "GameData.h"

Scene *ShopLayer::scene(State gameState)
{
    Scene *scene = Scene::create();
    if(scene){
        ShopLayer *layer = ShopLayer::create();
        layer->mState = gameState;
        //layer->setState(gameState);//init gameSate
        layer->init1();
        scene->addChild(layer);
        return scene;
    }
    return NULL;
}

bool ShopLayer::init1()
{
    Size winSize = Director::getInstance()->getVisibleSize();
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("shop-hd.plist");//for background
    
    Sprite *bg = Sprite::createWithSpriteFrameName("shop_bk.png");
    bg->setPosition(Point(winSize.width/2, winSize.height/2));
    this->addChild(bg);
    bg->setScale(2);
    
    Sprite *coinNumSprite = Sprite::createWithSpriteFrameName("shop_coin_num.png");
    Size itemSize = coinNumSprite->getContentSize();
    //coinNumSprite->setScale(0.5);
    coinNumSprite->setPosition(Point(itemSize.width/2, winSize.height-itemSize.height/2));
    this->addChild(coinNumSprite);
    
    Sprite *befourSprite = Sprite::createWithSpriteFrameName("shop_befour.png");
    //befourSprite->setScale(0.5);
    befourSprite->setPosition(Point(winSize.width/2+3,420));
    this->addChild(befourSprite);
    
    //add labels
    nameLabel = LabelTTF::create("name", "STHeitiTC-Bold", 40);
    nameLabel->setPosition(Point(winSize.width/2, winSize.height-130));
    this->addChild(nameLabel);
    
    commentLabel = LabelTTF::create("123", "STHeitiTC-Bold", 20);
    commentLabel->setPosition(Point(winSize.width/2, winSize.height-290));
    commentLabel->setHorizontalAlignment(Label::HAlignment::CENTER);
    commentLabel->setColor(Color3B::YELLOW);
    this->addChild(commentLabel);
    
    currentCommentLabel = LabelTTF::create("123", "STHeitiTC-Bold", 20);
    currentCommentLabel->setPosition(commentLabel->getPosition() + Point(0, 30));
    currentCommentLabel->setHorizontalAlignment(Label::HAlignment::CENTER);
    this->addChild(currentCommentLabel);
    
    coinLabel = LabelBMFont::create("0", "number_gold.fnt");
    coinLabel->setPosition(Point(160, winSize.height-20));
    this->addChild(coinLabel);
    
    if(mState != kBuyCoin) priceSprite = Sprite::createWithSpriteFrameName("shop_goods_coin.png");
    else priceSprite = Sprite::createWithSpriteFrameName("shop_goods_Y.png");
    priceSprite->setPosition(Point(winSize.width/2+60, winSize.height-625));
    this->addChild(priceSprite);
    priceLabel = LabelBMFont::create("0", "number_gold.fnt");
    priceLabel->setPosition(priceSprite->getPosition() + Point(65,35));
    this->addChild(priceLabel);
    
    //add buttons
    returnSprite = MenuItemSprite::create(Sprite::createWithSpriteFrameName("being_return2.png"), Sprite::createWithSpriteFrameName("being_return2_selected.png"), CC_CALLBACK_1(ShopLayer::buttonCallBack, this));
    if(mState == kBuyTool){
        buyButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("shop_button_buy.png"),
                                           Sprite::createWithSpriteFrameName("shop_button_buy_selected.png"),
                                           CC_CALLBACK_1(ShopLayer::buttonCallBack,this)
                                           );
        currentCommentLabel->setVisible(false);
        stateSprite = Sprite::createWithSpriteFrameName("shop_goods_have.png");
        stateSprite->setPosition(Point(winSize.width/2-150,winSize.height-420));
        //stateLabel = LabelTTF::create("123", "STHeitiTC-Bold", 20);
        stateLabel = LabelBMFont::create("123", "number_dollar.fnt");
        stateLabel->setPosition(stateSprite->getPosition() + Point(48, 3));
        this->addChild(stateSprite);
        this->addChild(stateLabel);
    }else if(mState == kUpdateTool){
        buyButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("shop_button_level.png"),
                                           Sprite::createWithSpriteFrameName("shop_button_level.png"),
                                           CC_CALLBACK_1(ShopLayer::buttonCallBack,this)
                                           );
        stateSprite = Sprite::createWithSpriteFrameName("shop_goods_lv.png");
        stateSprite->setPosition(Point(winSize.width/2-150,winSize.height-420));
        //stateLabel = LabelTTF::create("123", "STHeitiTC-Bold", 20);
        stateLabel = LabelBMFont::create("123", "number_dollar.fnt");
        stateLabel->setPosition(stateSprite->getPosition() + Point(48, 3));
        this->addChild(stateSprite);
        this->addChild(stateLabel);
    }else{
        buyButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("shop_button_recharge.png"),
                                           Sprite::createWithSpriteFrameName("shop_button_recharge.png"),
                                           CC_CALLBACK_1(ShopLayer::buttonCallBack,this)
                                           );
        currentCommentLabel->setVisible(false);
        stateSprite=NULL;
        stateSprite = Sprite::createWithSpriteFrameName("shop_goods_have.png");
        stateSprite->setPosition(Point(winSize.width/2-150,winSize.height-420));
        //stateLabel = LabelTTF::create("123", "STHeitiTC-Bold", 20);
        stateLabel = LabelBMFont::create("123", "number_dollar.fnt");
        stateLabel->setPosition(stateSprite->getPosition() + Point(48, 3));
        this->addChild(stateSprite);
        this->addChild(stateLabel);
    }
    
    buyButton->setPosition(Point(winSize.width/2, winSize.height-700));
    Size spriteSize = returnSprite->getContentSize();
    //returnSprite->setScale(0.5);
    returnSprite->setPosition(winSize.width - spriteSize.width/2 - 5,  winSize.height - spriteSize.height/2 - 5);
    Menu *menu = Menu::create(returnSprite, buyButton, NULL);
    menu->setPosition(Point::ZERO);
    this->addChild(menu);

    //init tool
    index=1;
    
    //initSprite
    leftSprite = Sprite::create();
    midSprite = Sprite::create();
    rightSprite = Sprite::create();
    
    leftSprite->setPosition(Point(winSize.width/2-362,170));
    //s0->setScale(0.5);
    midSprite->setPosition(Point(winSize.width/2-2,310));
    //s1->setScale(0.5);
    rightSprite->setPosition(Point(winSize.width/2+368, 170));
    //s2->setScale(0.5);
    this->addChild(leftSprite);
    this->addChild(midSprite);
    this->addChild(rightSprite);
    
    setState(mState);
    updateState();
    dialogSprite = Sprite::createWithSpriteFrameName("shop_coin_alert.png");
    dialogSprite->setPosition(Point(winSize.width/2, winSize.height/2));
    this->addChild(dialogSprite);
    dialogSprite->setVisible(false);
    itemSize = dialogSprite->getContentSize();
    /*
    buyCoinSprite = Sprite::createWithSpriteFrameName("shop_buyCoin.png");
    Size itemSize2 = buyCoinSprite->getContentSize();
    buyCoinSprite->setPosition(dialogSprite->getPosition() - Point(itemSize.width/2+itemSize2.width/2,0));
    this->addChild(buyCoinSprite);
    buyCoinSprite->setVisible(false);
     */
    return true;
}

void ShopLayer::setState(State state)
{
    mState = state;
    switch (mState) {
        case kBuyCoin:
            frame0 = SpriteFrameCache::getInstance()->getSpriteFrameByName("shop_coin1.png");
            frame1 = SpriteFrameCache::getInstance()->getSpriteFrameByName("shop_coin2.png");
            frame2 = SpriteFrameCache::getInstance()->getSpriteFrameByName("shop_coin3.png");
            frame3 = SpriteFrameCache::getInstance()->getSpriteFrameByName("shop_coin_double.png");
            shopItems = Array::create(frame0,frame1,frame2,frame3,NULL);
            log("buy Coin");
            break;
        case kBuyTool:
            frame0 = SpriteFrameCache::getInstance()->getSpriteFrameByName("shop_goods1.png");
            frame1 = SpriteFrameCache::getInstance()->getSpriteFrameByName("shop_goods2.png");
            frame2 = SpriteFrameCache::getInstance()->getSpriteFrameByName("shop_goods3.png");
            frame3 = SpriteFrameCache::getInstance()->getSpriteFrameByName("shop_goods4.png");
            shopItems = Array::create(frame0,frame1,frame2,frame3,NULL);
            log("buy tool");
            break;
        case kUpdateTool:
            frame0 = SpriteFrameCache::getInstance()->getSpriteFrameByName("shop_level1.png");
            frame1 = SpriteFrameCache::getInstance()->getSpriteFrameByName("shop_level2.png");
            frame2 = SpriteFrameCache::getInstance()->getSpriteFrameByName("shop_level3.png");
            frame3 = SpriteFrameCache::getInstance()->getSpriteFrameByName("shop_level4.png");
            frame4 = SpriteFrameCache::getInstance()->getSpriteFrameByName("shop_level5.png");
            frame5 = SpriteFrameCache::getInstance()->getSpriteFrameByName("shop_level6.png");
            shopItems = Array::create(frame0,frame1,frame2,frame3,frame4,frame5,NULL);
            log("buy update tool");
        default:
            break;
    }
    shopItems->retain();
    leftSprite->setDisplayFrame(frame0);
    midSprite->setDisplayFrame(frame1);
    rightSprite->setDisplayFrame(frame2);
}

void ShopLayer::updateState()
{
    //update coin num
    unsigned int coinNum = GameData::getInstance()->getCoinNums();
    char str[64];
    sprintf(str, "%d", coinNum);
    coinLabel->setString(str);
    //update comment
    if(mState == kBuyTool){
        string name = GameData::getInstance()->getToolName(index+1);
        string comment = GameData::getInstance()->getToolComment(index+1);
        nameLabel->setString(name.c_str());
        commentLabel->setString(comment.c_str());
        
        //have tool num
        unsigned int toolNum = 0;
        if(index==0) toolNum = GameData::getInstance()->getToolNum(GameData::kToolGlass);
        else if(index==1) toolNum = GameData::getInstance()->getToolNum(GameData::kToolClock);
        else if(index==2) toolNum = GameData::getInstance()->getToolNum(GameData::kToolClean);
        else if(index==3) toolNum = GameData::getInstance()->getToolNum(GameData::kToolRotate);
        sprintf(str, "%d", toolNum);
        stateLabel->setString(str);
        
        //price
        unsigned int toolPrice = GameData::getInstance()->getToolPrice(index+1);
        sprintf(str, "%d", toolPrice);
        priceLabel->setString(str);
        currentPrice = toolPrice;
    }else if(mState==kUpdateTool){
        string name = GameData::getInstance()->getSkillName(index+1);
        nameLabel->setString(name.c_str());
        
        //have tool num
        unsigned int skillLevel= GameData::getInstance()->getSkillLevel(index+1);
        sprintf(str, "%d", skillLevel);
        stateLabel->setString(str);
        
        string curr_comment="";
        string next_comment="";
        if(skillLevel<15){
            curr_comment = GameData::getInstance()->getSkillComment(index+1, skillLevel);
            next_comment = GameData::getInstance()->getSkillComment(index+1, skillLevel+1);
        }else{
            curr_comment = GameData::getInstance()->getSkillComment(index+1, skillLevel);
        }
        
        currentCommentLabel->setString(curr_comment.c_str());
        commentLabel->setString(next_comment.c_str());
        //price
        unsigned int toolPrice = 0;
        if(skillLevel<15) toolPrice = GameData::getInstance()->getSkillPrice(index+1, skillLevel+1);
        currentPrice = toolPrice;
        sprintf(str, "%d", toolPrice);
        priceLabel->setString(str);
        if(skillLevel==15) buyButton->setVisible(false);
        else buyButton->setVisible(true);
    }else{
        string name = GameData::getInstance()->getCoinName(index+1);
        nameLabel->setString(name.c_str());
        
        string curr_comment="";
        curr_comment = GameData::getInstance()->getCoinComment(index+1);
        
        commentLabel->setString(curr_comment.c_str());
        
        //price
        unsigned int toolPrice = 0;
        toolPrice = GameData::getInstance()->getCoinPrice(index+1);
        currentPrice = toolPrice;
        sprintf(str, "%d", toolPrice);
        priceLabel->setString(str);
        //buyButton->setVisible(true);
        
        unsigned int coinNum = GameData::getInstance()->getCoinNums();
        sprintf(str, "%d", coinNum);
        stateLabel->setString(str);
    }
    
    int count = shopItems->count();//total
    if(index==0){
        leftIndex=count-1;
        rightIndex=1;
    }else if(index==count-1){
        leftIndex=count-2;
        rightIndex=0;
    }else{
        leftIndex=index-1;
        rightIndex=index+1;
    }
    //log("index[%d,%d,%d]", leftIndex, index, rightIndex);
    leftSprite->setDisplayFrame((SpriteFrame*)shopItems->objectAtIndex(leftIndex));
    midSprite->setDisplayFrame((SpriteFrame*)shopItems->objectAtIndex(index));
    rightSprite->setDisplayFrame((SpriteFrame*)shopItems->objectAtIndex(rightIndex));
}

void ShopLayer::buttonCallBack(Object* pSender)
{
    if(mState==kDialog){
        dialogSprite->setVisible(false);
        //buyCoinSprite->setVisible(false);
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        mState = preState;
        return;
    }
    //log("back");
    if(pSender == returnSprite){
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        Scene *scene = ShopSelectLayer::scene();
        TransitionFade *fade = TransitionFade::create(0.3, scene, Color3B::BLACK);
        Director::getInstance()->replaceScene(fade);
    }else if(pSender == buyButton){
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        if(mState == kBuyTool){
            log("buy tool");
            unsigned int coinNum = GameData::getInstance()->getCoinNums();
            if(coinNum < currentPrice){
                log("coin not enough");
                dialogSprite->setVisible(true);
                //buyCoinSprite->setVisible(true);
                preState = mState;
                mState = kDialog;
                return;
            }
            GameData::getInstance()->useCoinNum(currentPrice);//use coin
            if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("coin.mp3");
            if(index==0) GameData::getInstance()->addToolNum(GameData::ToolType::kToolGlass, 1);
            else if(index==1) GameData::getInstance()->addToolNum(GameData::ToolType::kToolClock, 1);
            else if(index==2) GameData::getInstance()->addToolNum(GameData::ToolType::kToolClean, 1);
            else if(index==3) GameData::getInstance()->addToolNum(GameData::ToolType::kToolRotate, 1);
            updateState();
            return;
        }else if(mState == kUpdateTool){
            log("update tool");
            unsigned int coinNum = GameData::getInstance()->getCoinNums();
            if(coinNum < currentPrice){
                log("coin not enough");
                dialogSprite->setVisible(true);
                //buyCoinSprite->setVisible(true);
                preState = mState;
                mState = kDialog;
                return;
            }
            if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("coin.mp3");
            GameData::getInstance()->useCoinNum(currentPrice);//use coin
            GameData::getInstance()->addSkillLevel(index+1);
            updateState();
        }
    }
}

void ShopLayer::onEnterTransitionDidFinish()
{
    //log("StageSelectLayer on Enter");
    Director::getInstance()->getTouchDispatcher()->addTargetedDelegate(this, 1, true);
    Layer::onEnterTransitionDidFinish();
}

void ShopLayer::onExit()
{
    //log("StageSelectLayer on Exit");
    Director::getInstance()->getTouchDispatcher()->removeDelegate(this);
    Layer::onExit();
    shopItems->release();
}

bool ShopLayer::ccTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    return true;
}

void ShopLayer::ccTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
}

void ShopLayer::ccTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
    if(mState==kDialog){
        dialogSprite->setVisible(false);
        //buyCoinSprite->setVisible(false);
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        mState = preState;
        return;
    }
    Point location = Director::getInstance()->convertToGL(touch->getLocationInView());
    //log("location[%f,%f]", location.x, location.y);
    Point posleft = leftSprite->getPosition();
    Point posright = rightSprite->getPosition();
    if(fabs(location.x-posleft.x)<=40 && fabs(location.y-posleft.y)<=40){
        //log("left");
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        if(index>0) index--;
        else index=shopItems->count()-1;
        updateState();
        midSprite->setScale(0.1);
        ScaleTo *scale = ScaleTo::create(0.5, 1);
        midSprite->runAction(scale);
    }
    if(fabs(location.x-posright.x)<=40 && fabs(location.y-posright.y)<=40){
        //log("right");
        if(GameData::getInstance()->isEffectOn()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("button.mp3");
        if(index<shopItems->count()-1) index++;
        else index=0;
        updateState();
        midSprite->setScale(0.1);
        ScaleTo *scale = ScaleTo::create(0.5, 1);
        midSprite->runAction(scale);
    }
    //log("index=%d", index);
}
